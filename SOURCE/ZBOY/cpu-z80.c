/*
  Emulation core of the Zilog 80 CPU clone used in GameBoy consoles
  Part of the zBoy project
  Copyright (C) Mateusz Viste 2010,2011,2012,2013
*/


struct CpuRegisters { /* Note: IX and IY have been removed from the GB Z80 clone */
  /* 8 bit registers */
  uint8_t A;   /* Accumulator */
  uint8_t B;   /* Commonly used as a 8bit counter */
  uint8_t C;
  uint8_t D;
  uint8_t E;
  uint8_t F;   /* Flags */
  uint8_t H;
  uint8_t L;
  /*I AS UBYTE */  /* Interrupt vector register */
  /*R AS UBYTE */  /* Refresh register / might be used to generate random numbers */
  /* 16 bit register - Note: to write/read composite register, use dedicated functions! (eg. ReadRegAF) */
  int PC;        /* Program counter (holds memory adress currently executed code). No function can change PC */
  int SP;        /* The stack pointer. Holds current address of the top of the stack */
};

struct CpuRegisters Register;

int InterruptsState = 0;    /*  1 = ON  0 = OFF   (this is the IME register) */
int HaltBug = 0;            /* used to emulate the "HALT bug" (using HLT when IME is 0) -> The Smurfs won't work without that! */
int HaltState = 0;          /* 0 = CPU is running   1 = CPU is HALTed (wait until next interrupt) */

#include "cpu-code.c"  /* cpu microinstruction set */

#ifdef DEBUGMODE
  uint8_t DebugCpuPause = 0;   /* If set, pause at each CPU step */
#endif


/* tables below contain the number of machine cycles for every instruction */
const int cyctimes[256] = {
  1,3,2,2,1,1,2,1,5,2,2,2,1,1,2,1,
  0,3,2,2,1,1,2,1,3,2,2,2,1,1,2,1,
  2,3,2,2,1,1,2,1,2,2,2,2,1,1,2,1,
  2,3,2,2,3,3,3,1,2,2,2,2,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  2,2,2,2,2,2,1,2,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  1,1,1,1,1,1,2,1,1,1,1,1,1,1,2,1,
  2,3,3,4,3,4,2,4,2,4,3,0,3,6,2,4,
  2,3,3,0,3,4,2,4,2,4,3,0,3,0,2,4,
  3,3,2,0,0,4,2,4,4,1,4,0,0,0,2,4,
  3,3,2,1,0,4,2,4,3,2,4,1,0,0,2,4
};

const int cyctimescb[256] = {
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,3,2,2,2,2,2,2,2,3,2,
  2,2,2,2,2,2,3,2,2,2,2,2,2,2,3,2,
  2,2,2,2,2,2,3,2,2,2,2,2,2,2,3,2,
  2,2,2,2,2,2,3,2,2,2,2,2,2,2,3,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2,
  2,2,2,2,2,2,4,2,2,2,2,2,2,2,4,2
};


inline static void PopPCfromStack(void) {
  uint8_t tUbyteBuff1, tUbyteBuff2;
  PopFromStack(&tUbyteBuff1, &tUbyteBuff2);
  Register.PC = ((tUbyteBuff1 << 8) | tUbyteBuff2);
}

inline static void PushToStack16(int x) { /* Push to stack a 16bit value */
  PushToStack((x >> 8), (x & bx11111111));
}


/* Routine to process interrupts */
inline static void CheckInterrupts(void) {
  /* IME: Interrupt Master Enable (InterruptsState)
     IE: Interrupt Enable (0xFFFF)
     IF: Interrupt flag   (0xFF0F) */
  static uint8_t IrqTempState;
  if ((InterruptsState == 0) && (HaltState == 0)) return;
  IrqTempState = (MemoryInternalHiRAM[0xFFFF] & IoRegisters[0xFF0F] & bx00011111);    /* IE AND IF, but only on 5 bits (because there are 5 INTs to monitor) */
  if (IrqTempState == 0) return; /* No interrupt to handle */
  if (HaltState == 1) {
    HaltState = 0;   /* If the CPU was HALTed, resume it, but DON'T touch interrupts if the CPU is in DI state! */
    Register.PC++;
    if (InterruptsState == 0) return; /* If interrupts are disabled, quit here. We don't want to handle them if they are off, just resume the HALT */
  }
  PushToStack16(Register.PC);            /* Save current address */
  InterruptsState = 0;   /*  Reset the IME flag   * this is MANDATORY! * */
  if ((IrqTempState & INT_VBLANK) != 0) {         /* V-Blank */
    PrintDebug("INT: VBLANK\n");
    Register.PC = 0x40;
    IoRegisters[0xFF0F] &= ~INT_VBLANK;  /* Reset the trigerred if flag */
  } else if ((IrqTempState & INT_LCDC) != 0) {  /* LCDC */
    PrintDebug("INT: LCDC\n");
    Register.PC = 0x48;
    IoRegisters[0xFF0F] &= ~INT_LCDC;  /* Reset the trigerred if flag */
  } else if ((IrqTempState & INT_TIMA) != 0) {    /* TIMER */
    PrintDebug("INT: TIMER\n");
    Register.PC = 0x50;
    IoRegisters[0xFF0F] &= ~INT_TIMA;  /* Reset the trigerred if flag */
  } else if ((IrqTempState & INT_SERIAL) != 0) {  /* SERIAL */
    PrintDebug("INT: SERIAL\n");
    Register.PC = 0x58;
    IoRegisters[0xFF0F] &= ~INT_SERIAL;  /* Reset the trigerred if flag */
  } else if ((IrqTempState & INT_JOYPAD) != 0) {  /* Joypad */
    PrintDebug("INT: JOYPAD\n");
    Register.PC = 0x60;
    IoRegisters[0xFF0F] &= ~INT_JOYPAD;  /* Reset the trigerred if flag */
  }
}


/* executes a single CPU 'tick', and returns the number of consumed CLOCK cycles */
inline static int CpuExec(void) {
  uint8_t UbyteBuff1, UbyteBuff2;  /* Temporary buffer for various UBYTE values */
  int8_t ByteBuff;                 /* Temporary buffer for various BYTE values */
  unsigned int UintBuff;           /* Temporary buffer for various UINTEGER values */
  int cpucycles;
  uint8_t CpuInstruction;          /* holds current CPU instruction (bytecode) */

  /*IF TotalCycles > 1610000 THEN DebugCpuPause = 1  */  /* For debug purpose only! Enables stepbystep after a fixed time. */
  /*IF Register.PC = 0x38 THEN DebugCpuPause = 1  */     /* For debug purpose only! Enables stepbystep when reach a specific address */

  if (Register.PC > 0xFFFF) {
    PrintMsg("FATAL ERROR: PROGRAM COUNTER (PC) EXCEEDED FFFF!", 0); /* + CHR(10,10) + "EXECUTED " & TotalCycles & " CYCLES"); */
    #ifdef DEBUGMODE
      DebugOnScreen();
      DebugShowCpuRegisters();
    #endif
    PressAnyKey();
    exit(4);
  }

  PrintDebug("Registers -> AF=%02X%02X BC=%02X%02X DE=%02X%02X HL=%02X%02X SP=%04X PC=%04X\n", Register.A, Register.F, Register.B, Register.C, Register.D, Register.E, Register.H, Register.L, Register.SP, Register.PC);
  /*PressAnyKey();  */  /* For DEBUG purpose only */
  CpuInstruction = MemoryRead(Register.PC);   /* Fetch the next opcode */
  PrintDebug("fetched opcode %02Xh\n", CpuInstruction);

  if ((HaltBug != 0) && (HaltState == 0)) {   /* Emulates the "HALT bug", as described in the Gameboy CPU manual at page 20 */
    HaltBug = 0;        /* Some games are relying on this HALT bug, eg. "The Smurfs"                  */
    Register.PC--;
  }

  /* find out how much cycles the instruction will take (this might be
   * updated later, if the instruction happens to be CB-prefixed) */
  cpucycles = cyctimes[CpuInstruction];

  switch (CpuInstruction) {
    case 0x00:   /* NOP (No operation) */
      PrintDebug("NOP");  /* DEBUG */
      Register.PC++;
      break;
    case 0x01:   /* LD BC,nn  (load nn into BC) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD BC,nn"); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2))); */ /* DEBUG */
      WriteRegBC(UbyteBuff2, UbyteBuff1);
      Register.PC += 3;
      break;
    case 0x02:   /* LD (BC),A */   /* Load value of A into address at BC */
      PrintDebug("LD (BC),A");  /* DEBUG */
      MemoryWrite(ReadRegBC(), Register.A);
      Register.PC++;
      break;
    case 0x03:   /* INC BC (register BC+=1) */
      PrintDebug("INC BC");  /* DEBUG */
      IncReg16(&Register.B, &Register.C);
      Register.PC++;
      break;
    case 0x04:   /* INC B (register B+=1) */
      PrintDebug("INC B");  /* DEBUG */
      IncReg8(&Register.B);
      Register.PC++;
      break;
    case 0x05:   /* DEC B (register B-=1) */
      PrintDebug("DEC B");  /* DEBUG */
      DecReg8(&Register.B);
      Register.PC++;
      break;
    case 0x06:   /* LD B,n (load n into B) */
      PrintDebug("LD B,n");  /* DEBUG */
      Register.B = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x07:   /* RLCA (Rotate reg A left, old bit 7 written to Carry flag) */
      PrintDebug("RLCA");  /*  DEBUG */
      RLCA();
      Register.PC++;
      break;
    case 0x08:   /* LD (nn),SP  (puts SP value at nn) */
      /*MemoryWrite16(DwordVal(MemoryRead(Register.PC + 1),MemoryRead(Register.PC + 2)), Register.SP); */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD (nn),SP"); /* & HEX(DwordVal(UbyteBuff1,UbyteBuff2)) & ",SP"); */
      MemoryWrite(DwordVal(UbyteBuff1,UbyteBuff2), (Register.SP & bx11111111));
      MemoryWrite(DwordVal(UbyteBuff1,UbyteBuff2) + 1, (Register.SP >> 8) & bx11111111);
      Register.PC += 3;
      break;
    case 0x09:   /* ADD HL,BC */
      PrintDebug("ADD HL,BC");
      AddToHL(ReadRegBC());
      Register.PC++;
      break;
    case 0x0A:   /* LD A,(BC)  (put value from address BC to A) */
      PrintDebug("LD A,(BC)");  /* DEBUG */
      Register.A = MemoryRead(ReadRegBC());
      Register.PC++;
      break;
    case 0x0B:   /* DEC BC (register BC-=1) */
      PrintDebug("DEC BC");  /* DEBUG */
      DecReg16(&Register.B, &Register.C);
      Register.PC++;
      break;
    case 0x0C:   /* INC C (register C+=1) */
      PrintDebug("INC C");  /* DEBUG */
      IncReg8(&Register.C);
      Register.PC++;
      break;
    case 0x0D:   /* DEC C (register C-=1) */
      PrintDebug("DEC C");  /* DEBUG */
      DecReg8(&Register.C);
      Register.PC++;
      break;
    case 0x0E:   /* LD C,n (load n into C) */
      PrintDebug("LD C,n");  /* DEBUG */
      Register.C = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x0F:   /* RRCA (Rotate reg A right, old bit 0 written to Carry flag) */
      PrintDebug("RRCA");  /*  DEBUG */
      RRCA();
      Register.PC++;
      break;
    case 0x10:   /* STOP (opcode 10 00) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      if (UbyteBuff1 == 0x00) {
        #ifndef DEBUGMODE
          PrintMsg("GOT A STOP INSTRUCTION", 0);
          PressAnyKey();
        #endif
        #ifdef DEBUGMODE
          PrintDebug("Got a STOP at %4X (Opcode: 10 00)", Register.PC);
        #endif
        QuitEmulator = 1;
      } else {
        #ifndef DEBUGMODE
          /*UbyteBuff1 = MemoryRead(Register.PC + 1); */
          PrintMsg("GOT AN UNKNOWN STOP-LIKE INSTRUCTION!", 0); /* + CHR(10) + "ADDRESS: " + HEX(Register.PC, 4) + CHR(10) + "OPCODE: 10 " & HEX(UbyteBuff1, 2)); */
          printf("Got an unknown stop-like instruction (10%02Xh) at PC=0x%04Xh\n", UbyteBuff1, Register.PC);
          PressAnyKey();
        #endif
        #ifdef DEBUGMODE
          PrintDebug("Got an unknown stop-like instruction at %4X!", Register.PC);
        #endif
        QuitEmulator = 1;
      }
      break;
    case 0x11:   /* LD DE,nn  (load nn into DE) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD DE,nn"); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2))); */  /* DEBUG */
      WriteRegDE(UbyteBuff2, UbyteBuff1);
      Register.PC += 3;
      break;
    case 0x12:   /* LD (DE),A  */  /* Load value of A into address at DE */
      PrintDebug("LD (DE),A");  /* DEBUG */
      MemoryWrite(ReadRegDE(), Register.A);
      Register.PC++;
      break;
    case 0x13:   /* INC DE (register DE+=1) */
      PrintDebug("INC DE");  /* DEBUG */
      IncReg16(&Register.D, &Register.E);
      Register.PC++;
      break;
    case 0x14:   /* INC D (register D+=1) */
      PrintDebug("INC D");  /* DEBUG */
      IncReg8(&Register.D);
      Register.PC++;
      break;
    case 0x15:   /* DEC D (register D-=1) */
      PrintDebug("DEC D");  /* DEBUG */
      DecReg8(&Register.D);
      Register.PC++;
      break;
    case 0x16:   /* LD D,n (load n into D) */
      PrintDebug("LD D,n");  /* DEBUG */
      Register.D = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x17:  /* RLA */
      PrintDebug("RLA");  /* DEBUG */
      RotateLeftCarry(&Register.A);
      ResetFlagZ(); /* It looks like the only diff between 'RL A' and 'RLA' is that the latter always resets Z, while the former sets it accordingly to the result */
      Register.PC += 1;
      break;
    case 0x18:   /* JR n (jump to PC+n) */
      Register.PC += 2;  /* First increment the PC, and then jump */
      UbyteBuff1 = MemoryRead(Register.PC - 1);
      PrintDebug("JR n"); /* & UbyteToByte(UbyteBuff1)) */ /* DEBUG */
      Register.PC += UbyteToByte(UbyteBuff1);
      break;
    case 0x19:   /* ADD HL,DE */
      PrintDebug("ADD HL,DE");
      AddToHL(ReadRegDE());
      Register.PC++;
      break;
    case 0x1A:   /* LD A,(DE)  (put value from address DE to A) */
      PrintDebug("LD A,(DE)");
      Register.A = MemoryRead(ReadRegDE());
      Register.PC++;
      break;
    case 0x1B:   /* DEC DE (register DE-=1) */
      PrintDebug("DEC DE");  /* DEBUG */
      DecReg16(&Register.D, &Register.E);
      Register.PC++;
      break;
    case 0x1C:   /* INC E (register E+=1) */
      PrintDebug("INC E");  /* DEBUG */
      IncReg8(&Register.E);
      Register.PC++;
      break;
    case 0x1D:   /* DEC E (register E-=1) */
      PrintDebug("DEC E");  /* DEBUG */
      DecReg8(&Register.E);
      Register.PC++;
      break;
    case 0x1E:   /* LD E,n (load n into E) */
      PrintDebug("LD E,n");  /* DEBUG */
      Register.E = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x1F:  /* RRA */
      PrintDebug("RRA");  /* DEBUG */
      RotateRightCarry(&Register.A);
      ResetFlagZ(); /* It looks like the only diff between 'RR A' and 'RRA' is that the latter always resets Z, while the former sets it accordingly to the result */
      Register.PC++;
      break;
    case 0x20:   /* JR NZ,n (jump to PC+n if Z flag is false) */
      PrintDebug("JR NZ,n");  /* DEBUG */
      Register.PC += 2;  /* First increment the PC, and then we'll see */
      if (GetFlagZ() == 0) {
        cpucycles = 3; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC - 1);
        Register.PC += UbyteToByte(UbyteBuff1);
      }
      break;
    case 0x21:   /* LD HL,nn  (load nn into HL) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD HL,%04Xh", DwordVal(UbyteBuff1, UbyteBuff2)); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2))) */  /* DEBUG */
      WriteRegHL(UbyteBuff2, UbyteBuff1);
      Register.PC += 3;
      break;
    case 0x22:   /* LD (HL+),A (put A into memory addr HL, and increments HL) */
      PrintDebug("LD (HL+),A");   /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.A);
      IncReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x23:   /* INC HL (register HL+=1) */
      PrintDebug("INC HL");  /* DEBUG */
      IncReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x24:   /* INC H (register H+=1) */
      PrintDebug("INC H");  /* DEBUG */
      IncReg8(&Register.H);
      Register.PC++;
      break;
    case 0x25:   /* DEC H (register H-=1) */
      PrintDebug("DEC H");  /* DEBUG */
      DecReg8(&Register.H);
      Register.PC++;
      break;
    case 0x26:   /* LD H,n (load n into H) */
      PrintDebug("LD H,n");  /* DEBUG */
      Register.H = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x27:   /* DAA */
      PrintDebug("DAA");  /* DEBUG */
      AdjustDAA();
      Register.PC++;
      break;
    case 0x28:   /* JR Z,n (jump to PC+n if Z flag is true) */
      PrintDebug("JR Z,n");  /* DEBUG */
      Register.PC += 2;  /* First increment the PC, and then we'll see */
      if (GetFlagZ() == 1) {
        cpucycles = 3; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC - 1);
        Register.PC += UbyteToByte(UbyteBuff1);
      }
      break;
    case 0x29:   /* ADD HL,HL */
      PrintDebug("ADD HL,HL");
      AddToHL(ReadRegHL());
      Register.PC++;
      break;
    case 0x2A:   /* LD A,(HL+) (Put value @HL into A, and increment HL by 1) */
      PrintDebug("LD A,(HL+)");    /* DEBUG */
      Register.A = MemoryRead(ReadRegHL());
      IncReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x2B:   /* DEC HL (register HL-=1) */
      PrintDebug("DEC HL");  /* DEBUG */
      DecReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x2C:   /* INC L (register L+=1) */
      PrintDebug("INC L");  /* DEBUG */
      IncReg8(&Register.L);
      Register.PC++;
      break;
    case 0x2D:   /* DEC L (register L-=1) */
      PrintDebug("DEC L");  /* DEBUG */
      DecReg8(&Register.L);
      Register.PC++;
      break;
    case 0x2E:   /* LD L,n (load n into L) */
      PrintDebug("LD L,n");  /* DEBUG */
      Register.L = MemoryRead(Register.PC + 1);
      Register.PC += 2;
      break;
    case 0x2F:   /* CPL (complements the A register, that is flips all bits) */
      PrintDebug("CPL");
      SetFlagN();
      SetFlagH();
      Register.A = (Register.A ^ bx11111111);  /* Flip all bits */
      Register.PC++;
      break;
    case 0x30:   /* JR NC,n (jump to PC+n if C flag is false) */
      PrintDebug("JR NC,n");  /* DEBUG */
      Register.PC += 2;  /* First increment the PC, and then we'll see */
      if (GetFlagC() == 0) {
        cpucycles = 3; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC - 1);
        Register.PC += UbyteToByte(UbyteBuff1);
      }
      break;
    case 0x31:   /* LD SP,nn */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD SP,%04X", DwordVal(UbyteBuff1, UbyteBuff2)); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2))) */  /* DEBUG */
      Register.SP = DwordVal(UbyteBuff1, UbyteBuff2);
      Register.PC += 3;
      break;
    case 0x32:   /* LD (HL-),A   (put A into address at HL, then decrement HL) */
      PrintDebug("LD (HL-),A");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.A);
      DecReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x33:   /* INC SP (register SP+=1) */
      PrintDebug("INC SP");  /* DEBUG */
      if (Register.SP == 65535) {
        Register.SP = 0;
      } else {
        Register.SP += 1;
      }
      Register.PC++;
      break;
    case 0x34:   /* INC (HL) (value at (HL) is incremented) */
      PrintDebug("INC (HL)");   /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      IncReg8(&UbyteBuff1);
      MemoryWrite(ReadRegHL(), UbyteBuff1);
      Register.PC++;
      break;
    case 0x35:   /* DEC (HL) -> (HL) -= 1 */
      PrintDebug("DEC (HL)");   /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());  /* Save old value at (HL) */
      DecReg8(&UbyteBuff1);
      MemoryWrite(ReadRegHL(), UbyteBuff1);
      Register.PC++;
      break;
    case 0x36:   /* LD (HL),n */   /* Load value of n into address at HL */
      PrintDebug("LD (HL),n");  /* DEBUG */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      MemoryWrite(ReadRegHL(), UbyteBuff1);
      Register.PC += 2;
      break;
    case 0x37:   /* SCF (Set Carry Flag) */
      PrintDebug("SCF");  /* DEBUG */
      ResetFlagN();
      ResetFlagH();
      SetFlagC();
      Register.PC++;
      break;
    case 0x38:   /* JR C,n (jump to PC+n if C flag is true) */
      PrintDebug("JR C,n");  /* DEBUG */
      Register.PC += 2;  /* First increment the PC, and then we'll see */
      if (GetFlagC() == 1) {
        cpucycles = 3; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC - 1);
        Register.PC += UbyteToByte(UbyteBuff1);
      }
      break;
    case 0x39:   /* ADD HL,SP */
      PrintDebug("ADD HL,SP");
      AddToHL(Register.SP);
      Register.PC++;
      break;
    case 0x3A:   /* LD A,(HL-)   (put value at address HL into A, and decrement HL) */
      PrintDebug("LD A,(HL-)");
      Register.A = MemoryRead(ReadRegHL());
      DecReg16(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0x3B:   /* DEC SP (register SP-=1) */
      PrintDebug("DEC SP");  /* DEBUG */
      if (Register.SP == 0) {
        Register.SP = 65535;
      } else {
        Register.SP -= 1;
      }
      Register.PC++;
      break;
    case 0x3C:   /* INC A (register A+=1) */
      PrintDebug("INC A");  /* DEBUG */
      IncReg8(&Register.A);
      Register.PC++;
      break;
    case 0x3D:   /* DEC A (register A-=1) */
      PrintDebug("DEC A");  /* DEBUG */
      DecReg8(&Register.A);
      Register.PC++;
      break;
    case 0x3E:   /* LD A,n  (put n into A) */
      Register.A = MemoryRead(Register.PC + 1);
      PrintDebug("LD A,n"); /* & Register.A) */
      Register.PC += 2;
      break;
    case 0x3F:   /* CCF  (Complement Carry Flag) */
      PrintDebug("CCF");
      ResetFlagN();
      ResetFlagH();
      if (GetFlagC() == 0) {
        SetFlagC();
      } else {
        ResetFlagC();
      }
      Register.PC++;
      break;
    case 0x40:   /* LD B,B     Put value B into B */
      PrintDebug("LD B,B");  /* DEBUG */
      /*Register.B = Register.B; */   /* commented out - useless */
      Register.PC++;
      break;
    case 0x41:   /* LD B,C       Put value C into B */
      PrintDebug("LD B,C");  /* DEBUG */
      Register.B = Register.C;
      Register.PC++;
      break;
    case 0x42:   /* LD B,D     Put value D into B */
      PrintDebug("LD B,D");  /* DEBUG */
      Register.B = Register.D;
      Register.PC++;
      break;
    case 0x43:   /* LD B,E     Put value E into B */
      PrintDebug("LD B,E");  /* DEBUG */
      Register.B = Register.E;
      Register.PC++;
      break;
    case 0x44:   /* LD B,H      Put value H into B */
      PrintDebug("LD B,H");  /* DEBUG */
      Register.B = Register.H;
      Register.PC++;
      break;
    case 0x45:   /* LD B,L     Put value L into B */
      PrintDebug("LD B,L");  /* DEBUG */
      Register.B = Register.L;
      Register.PC++;
      break;
    case 0x46:   /* LD B,(HL)  (put value from address HL to B) */
      PrintDebug("LD B,(HL)");
      Register.B = MemoryRead(ReadRegHL());
      Register.PC++;
      break;
    case 0x47:   /* LD B,A      Put value A into B */
      PrintDebug("LD B,A");  /* DEBUG */
      Register.B = Register.A;
      Register.PC++;
      break;
    case 0x48:   /* LD C,B     Put value B into C */
      PrintDebug("LD C,B");  /* DEBUG */
      Register.C = Register.B;
      Register.PC++;
      break;
    case 0x49:   /* LD C,C      Put value C into C */
      PrintDebug("LD C,C");  /* DEBUG */
      Register.C = Register.C;
      Register.PC++;
      break;
    case 0x4A:   /* LD C,D     Put value D into C */
      PrintDebug("LD C,D");  /* DEBUG */
      Register.C = Register.D;
      Register.PC++;
      break;
    case 0x4B:   /* LD C,E     Put value E into C */
      PrintDebug("LD C,E");  /* DEBUG */
      Register.C = Register.E;
      Register.PC++;
      break;
    case 0x4C:   /* LD C,H     Put value H into C */
      PrintDebug("LD C,H");  /* DEBUG */
      Register.C = Register.H;
      Register.PC++;
      break;
    case 0x4D:   /* LD C,L     Put value L into C */
      PrintDebug("LD C,L");  /* DEBUG */
      Register.C = Register.L;
      Register.PC++;
      break;
    case 0x4E:   /* LD C,(HL)  (put value from address HL to C) */
      PrintDebug("LD C,(HL)");
      Register.C = MemoryRead(ReadRegHL());
      Register.PC++;
      break;
    case 0x4F:   /* LD C,A     Put value A into C */
      PrintDebug("LD C,A");  /* DEBUG */
      Register.C = Register.A;
      Register.PC++;
      break;
    case 0x50:   /* LD D,B     Put value B into D */
      PrintDebug("LD D,B");  /* DEBUG */
      Register.D = Register.B;
      Register.PC++;
      break;
    case 0x51:   /* LD D,C      Put value C into D */
      PrintDebug("LD D,C");  /* DEBUG */
      Register.D = Register.C;
      Register.PC++;
      break;
    case 0x52:   /* LD D,D     Put value D into D */
      PrintDebug("LD D,D");  /* DEBUG */
      /*Register.D = Register.D; */ /* Commented out - useless */
      Register.PC++;
      break;
    case 0x53:   /* LD D,E      Put value E into D */
      PrintDebug("LD D,E");  /* DEBUG */
      Register.D = Register.E;
      Register.PC++;
      break;
    case 0x54:   /* LD D,H      Put value H into D */
      PrintDebug("LD D,H");  /* DEBUG */
      Register.D = Register.H;
      Register.PC++;
      break;
    case 0x55:   /* LD D,L     Put value L into D */
      PrintDebug("LD D,L");  /* DEBUG */
      Register.D = Register.L;
      Register.PC++;
      break;
    case 0x56:   /* LD D,(HL)  (put value from address HL to D) */
      PrintDebug("LD D,(HL)");
      Register.D = MemoryRead(ReadRegHL());
      Register.PC++;
      break;
    case 0x57:   /* LD D,A      Put value A into D */
      PrintDebug("LD D,A");  /* DEBUG */
      Register.D = Register.A;
      Register.PC++;
      break;
    case 0x58:   /* LD E,B     Put value B into E */
      PrintDebug("LD E,B");  /* DEBUG */
      Register.E = Register.B;
      Register.PC++;
      break;
    case 0x59:   /* LD E,C     Put value C into E */
      PrintDebug("LD E,C");  /* DEBUG */
      Register.E = Register.C;
      Register.PC++;
      break;
    case 0x5A:   /* LD E,D     Put value D into E */
      PrintDebug("LD E,D");  /* DEBUG */
      Register.E = Register.D;
      Register.PC++;
      break;
    case 0x5B:   /* LD E,E     Put value E into E */
      PrintDebug("LD E,E");  /* DEBUG */
      Register.E = Register.E;
      Register.PC++;
      break;
    case 0x5C:   /* LD E,H     Put value H into E */
      PrintDebug("LD E,H");  /* DEBUG */
      Register.E = Register.H;
      Register.PC++;
      break;
    case 0x5D:   /* LD E,L     Put value L into E */
      PrintDebug("LD E,L");  /* DEBUG */
      Register.E = Register.L;
      Register.PC++;
      break;
    case 0x5E:   /* LD E,(HL)  (put value from address HL to E) */
      PrintDebug("LD E,(HL)");
      Register.E = MemoryRead(ReadRegHL());
      Register.PC++;
      break;
    case 0x5F:   /* LD E,A     Put value A into E */
      PrintDebug("LD E,A");  /* DEBUG */
      Register.E = Register.A;
      Register.PC++;
      break;
    case 0x60:   /* LD H,B     Put value B into H */
      PrintDebug("LD H,B");  /* DEBUG */
      Register.H = Register.B;
      Register.PC++;
      break;
    case 0x61:   /* LD H,C     Put value C into H */
      PrintDebug("LD H,C");  /* DEBUG */
      Register.H = Register.C;
      Register.PC++;
      break;
    case 0x62:   /* LD H,D     Put value D into H */
      PrintDebug("LD H,D");  /* DEBUG */
      Register.H = Register.D;
      Register.PC++;
      break;
    case 0x63:   /* LD H,E     Put value E into H */
      PrintDebug("LD H,E");  /* DEBUG */
      Register.H = Register.E;
      Register.PC++;
      break;
    case 0x64:   /* LD H,H      Put value H into H */
      PrintDebug("LD H,H");  /* DEBUG */
      /*Register.H = Register.H */   /* Commented out, because there is no point in doing that */
      Register.PC++;
      break;
    case 0x65:   /* LD H,L     Put value L into H */
      PrintDebug("LD H,L");  /* DEBUG */
      Register.H = Register.L;
      Register.PC++;
      break;
    case 0x66:   /* LD H,(HL)  (put value from address HL to H) */
      PrintDebug("LD H,(HL)");
      UbyteBuff1 = MemoryRead(ReadRegHL());
      Register.H = UbyteBuff1;   /* Don't try to work on H directly! It would change also HL! */
      /*Register.H = MemoryRead(ReadRegHL()) */
      Register.PC++;
      break;
    case 0x67:   /* LD H,A      Put value A into H */
      PrintDebug("LD H,A");  /* DEBUG */
      Register.H = Register.A;
      Register.PC++;
      break;
    case 0x68:   /* LD L,B     Put value B into L */
      PrintDebug("LD L,B");  /* DEBUG */
      Register.L = Register.B;
      Register.PC++;
      break;
    case 0x69:   /* LD L,C     Put value C into L */
      PrintDebug("LD L,C");  /* DEBUG */
      Register.L = Register.C;
      Register.PC++;
      break;
    case 0x6A:   /* LD L,D     Put value D into L */
      PrintDebug("LD L,D");  /* DEBUG */
      Register.L = Register.D;
      Register.PC++;
      break;
    case 0x6B:   /* LD L,E     Put value E into L */
      PrintDebug("LD L,E");  /* DEBUG */
      Register.L = Register.E;
      Register.PC++;
      break;
    case 0x6C:   /* LD L,H     Put value H into L */
      PrintDebug("LD L,H");  /* DEBUG */
      Register.L = Register.H;
      Register.PC++;
      break;
    case 0x6D:   /* LD L,L      Put value L into L */
      PrintDebug("LD L,L");  /* DEBUG */
      /*Register.L = Register.L */   /* Commented out, because there is no point in doing such operation */
      Register.PC++;
      break;
    case 0x6E:   /* LD L,(HL)  (put value from address HL to L) */
      PrintDebug("LD L,(HL)");
      UbyteBuff1 = MemoryRead(ReadRegHL());
      Register.L = UbyteBuff1;    /* Don't try to work directly on L! It would also change HL! */
      /*Register.L = MemoryRead(ReadRegHL()) */
      Register.PC++;
      break;
    case 0x6F:   /* LD L,A      Put value A into L */
      PrintDebug("LD L,A");  /* DEBUG */
      Register.L = Register.A;
      Register.PC++;
      break;
    case 0x70:   /* LD (HL),B    Load value of B into address at HL */
      PrintDebug("LD (HL),B");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.B);
      Register.PC++;
      break;
    case 0x71:   /* LD (HL),C    Load value of C into address at HL */
      PrintDebug("LD (HL),C");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.C);
      Register.PC++;
      break;
    case 0x72:   /* LD (HL),D    Load value of D into address at HL */
      PrintDebug("LD (HL),D");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.D);
      Register.PC++;
      break;
    case 0x73:   /* LD (HL),E   Load value of E into address at HL */
      PrintDebug("LD (HL),E");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.E);
      Register.PC++;
      break;
    case 0x74:   /* LD (HL),H   Load value of H into address at HL */
      PrintDebug("LD (HL),H");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.H);
      Register.PC++;
      break;
    case 0x75:   /* LD (HL),L   Load value of L into address at HL */
      PrintDebug("LD (HL),L");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.L);
      Register.PC++;
      break;
    case 0x76:   /* HALT   HALTs the CPU until next interrupt */
      /*  Note: I don't increment PC here, to make the game loop on the HALT op.
                PC will be incremented if an interrupt occurs. */
      HaltState = 1;
      if (InterruptsState == 0) {
        PrintDebug("Halt [INTs are OFF]");
        HaltBug = 1;
      } else {
        PrintDebug("Halt [INTs are ON]");
      }
      break;
    case 0x77:   /* LD (HL),A   Load value of A into address at HL */
      PrintDebug("LD (HL),A");  /* DEBUG */
      MemoryWrite(ReadRegHL(), Register.A);
      Register.PC++;
      break;
    case 0x78:   /* LD A,B  (put B into A) */
      PrintDebug("LD A,B");   /* DEBUG */
      Register.A = Register.B;
      Register.PC++;
      break;
    case 0x79:   /* LD A,C  (put C into A) */
      PrintDebug("LD A,C");   /* DEBUG */
      Register.A = Register.C;
      Register.PC++;
      break;
    case 0x7A:   /* LD A,D  (put D into A) */
      PrintDebug("LD A,D");   /* DEBUG */
      Register.A = Register.D;
      Register.PC++;
      break;
    case 0x7B:   /* LD A,E  (put E into A) */
      PrintDebug("LD A,E");   /* DEBUG */
      Register.A = Register.E;
      Register.PC++;
      break;
    case 0x7C:   /* LD A,H  (put H into A) */
      PrintDebug("LD A,H");   /* DEBUG */
      Register.A = Register.H;
      Register.PC++;
      break;
    case 0x7D:   /* LD A,L  (put L into A) */
      PrintDebug("LD A,L");   /* DEBUG */
      Register.A = Register.L;
      Register.PC++;
      break;
    case 0x7E:   /* LD A,(HL)  (put value from address HL to A) */
      PrintDebug("LD A,(HL)");
      Register.A = MemoryRead(ReadRegHL());
      Register.PC++;
      break;
    case 0x7F:   /* LD A,A  (put A into A) */
      PrintDebug("LD A,A");   /* DEBUG */
      /*Register.A = Register.A */  /* Commented out, because there is no point in doing such operation */
      Register.PC++;
      break;
    case 0x80:  /* ADD A,B  (A+=B) */
      PrintDebug("ADD A,B");
      AddToA(Register.B);
      Register.PC++;
      break;
    case 0x81:  /* ADD A,C  (A+=C) */
      PrintDebug("ADD A,C");
      AddToA(Register.C);
      Register.PC++;
      break;
    case 0x82:  /* ADD A,D  (A+=D) */
      PrintDebug("ADD A,D");
      AddToA(Register.D);
      Register.PC++;
      break;
    case 0x83:  /* ADD A,E  (A+=E) */
      PrintDebug("ADD A,E");
      AddToA(Register.E);
      Register.PC++;
      break;
    case 0x84:  /* ADD A,H  (A+=H) */
      PrintDebug("ADD A,H");
      AddToA(Register.H);
      Register.PC++;
      break;
    case 0x85:  /* ADD A,L  (A+=L) */
      PrintDebug("ADD A,L");
      AddToA(Register.L);
      Register.PC++;
      break;
    case 0x86:  /* ADD A,(HL)  (A+=[HL]) */
      PrintDebug("ADD A,(HL)");
      UbyteBuff1 = MemoryRead(ReadRegHL());
      AddToA(UbyteBuff1);
      Register.PC++;
      break;
    case 0x87:  /* ADD A,A  (A+=A) */
      PrintDebug("ADD A,A");
      AddToA(Register.A);
      Register.PC++;
      break;
    case 0x88:   /* ADC A,B */
      PrintDebug("ADC A,B");   /* DEBUG */
      AdcA(Register.B);
      Register.PC++;
      break;
    case 0x89:   /* ADC A,C */
      PrintDebug("ADC A,C");   /* DEBUG */
      AdcA(Register.C);
      Register.PC++;
      break;
    case 0x8A:   /* ADC A,D */
      PrintDebug("ADC A,D");   /* DEBUG */
      AdcA(Register.D);
      Register.PC++;
      break;
    case 0x8B:   /* ADC A,E */
      PrintDebug("ADC A,E");   /* DEBUG */
      AdcA(Register.E);
      Register.PC++;
      break;
    case 0x8C:   /* ADC A,H */
      PrintDebug("ADC A,H");   /* DEBUG */
      AdcA(Register.H);
      Register.PC++;
      break;
    case 0x8D:   /* ADC A,L */
      PrintDebug("ADC A,L");   /* DEBUG */
      AdcA(Register.L);
      Register.PC++;
      break;
    case 0x8E:   /* ADC A,(HL) */
      PrintDebug("ADC A,(HL)");   /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      AdcA(UbyteBuff1);
      Register.PC++;
      break;
    case 0x8F:   /* ADC A,A */
      PrintDebug("ADC A,A");   /* DEBUG */
      AdcA(Register.A);
      Register.PC++;
      break;
    case 0x90:  /* SUB B */
      PrintDebug("SUB B");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.B);
      Register.PC++;
      break;
    case 0x91:  /* SUB C */
      PrintDebug("SUB C");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.C);
      Register.PC++;
      break;
    case 0x92:  /* SUB D */
      PrintDebug("SUB D");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.D);
      Register.PC++;
      break;
    case 0x93:  /* SUB E */
      PrintDebug("SUB E");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.E);
      Register.PC++;
      break;
    case 0x94:  /* SUB H */
      PrintDebug("SUB H");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.H);
      Register.PC++;
      break;
    case 0x95:  /* SUB L */
      PrintDebug("SUB L");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.L);
      Register.PC++;
      break;
    case 0x96:  /* SUB (HL)  (sub content at address HL from A) */
      PrintDebug("SUB (HL)");  /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      SubValFromReg8(&Register.A, &UbyteBuff1);
      Register.PC++;
      break;
    case 0x97:  /* SUB A */
      PrintDebug("SUB A");  /* DEBUG */
      SubValFromReg8(&Register.A, &Register.A);
      Register.PC++;
      break;
    case 0x98:  /* SBC A,B */
      PrintDebug("SBC A,B");  /* DEBUG */
      SbcA(Register.B);
      Register.PC++;
      break;
    case 0x99:  /* SBC A,C */
      PrintDebug("SBC A,C");  /* DEBUG */
      SbcA(Register.C);
      Register.PC++;
      break;
    case 0x9A:  /* SBC A,D */
      PrintDebug("SBC A,D");  /* DEBUG */
      SbcA(Register.D);
      Register.PC++;
      break;
    case 0x9B:  /* SBC A,E */
      PrintDebug("SBC A,E");  /* DEBUG */
      SbcA(Register.E);
      Register.PC++;
      break;
    case 0x9C:  /* SBC A,H */
      PrintDebug("SBC A,H");  /* DEBUG */
      SbcA(Register.H);
      Register.PC++;
      break;
    case 0x9D:  /* SBC A,L */
      PrintDebug("SBC A,L");  /* DEBUG */
      SbcA(Register.L);
      Register.PC++;
      break;
    case 0x9E:  /* SBC A,(HL) */
      PrintDebug("SBC A,(HL)");  /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      SbcA(UbyteBuff1);
      Register.PC++;
      break;
    case 0x9F:  /* SBC A,A */
      PrintDebug("SBC A,A");  /* DEBUG */
      SbcA(Register.A);
      Register.PC++;
      break;
    case 0xA0:   /* AND B (A = A AND B) */
      PrintDebug("AND B");  /* DEBUG */
      AndA(Register.B);
      Register.PC++;
      break;
    case 0xA1:   /* AND C (A = A AND C) */
      PrintDebug("AND C");  /* DEBUG */
      AndA(Register.C);
      Register.PC++;
      break;
    case 0xA2:   /* AND D (A = A AND D) */
      PrintDebug("AND D");  /* DEBUG */
      AndA(Register.D);
      Register.PC++;
      break;
    case 0xA3:   /* AND E (A = A AND E) */
      PrintDebug("AND E");  /* DEBUG */
      AndA(Register.E);
      Register.PC++;
      break;
    case 0xA4:   /* AND H (A = A AND H) */
      PrintDebug("AND H");  /* DEBUG */
      AndA(Register.H);
      Register.PC++;
      break;
    case 0xA5:   /* AND L (A = A AND L) */
      PrintDebug("AND L");  /* DEBUG */
      AndA(Register.L);
      Register.PC++;
      break;
    case 0xA6:   /* AND (HL) (A = A AND [HL]) */
      PrintDebug("AND (HL)");  /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      AndA(UbyteBuff1);
      Register.PC++;
      break;
    case 0xA7:   /* AND A (A = A AND A) */
      PrintDebug("AND A");  /* DEBUG */
      AndA(Register.A);
      Register.PC++;
      break;
    case 0xA8:   /* XOR B (A = A XOR B) */
      PrintDebug("XOR B");  /* DEBUG */
      XorA(Register.B); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xA9:   /* XOR C (A = A XOR C) */
      PrintDebug("XOR C");  /* DEBUG */
      XorA(Register.C); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAA:   /* XOR D (A = A XOR D) */
      PrintDebug("XOR D");  /* DEBUG */
      XorA(Register.D); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAB:   /* XOR E (A = A XOR E) */
      PrintDebug("XOR E");  /* DEBUG */
      XorA(Register.E); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAC:   /* XOR H (A = A XOR H) */
      PrintDebug("XOR H");  /* DEBUG */
      XorA(Register.H); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAD:   /* XOR L (A = A XOR L) */
      PrintDebug("XOR L");  /* DEBUG */
      XorA(Register.L); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAE:   /* XOR (HL)  (A = A XOR [HL]) */
      PrintDebug("XOR (HL)");  /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      XorA(UbyteBuff1); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xAF:   /* XOR A (A = A XOR A) */
      PrintDebug("XOR A");  /* DEBUG */
      XorA(Register.A); /* Xor with A, result in A */
      Register.PC++;
      break;
    case 0xB0:   /* OR B  (A = A OR B) */
      PrintDebug("OR B");   /* DEBUG */
      OrA(Register.B);
      Register.PC++;
      break;
    case 0xB1:   /* OR C  (A = A OR C) */
      PrintDebug("OR C");   /* DEBUG */
      OrA(Register.C);
      Register.PC++;
      break;
    case 0xB2:   /* OR D  (A = A OR D) */
      PrintDebug("OR D");   /* DEBUG */
      OrA(Register.D);
      Register.PC++;
      break;
    case 0xB3:   /* OR E  (A = A OR E) */
      PrintDebug("OR E");   /* DEBUG */
      OrA(Register.E);
      Register.PC++;
      break;
    case 0xB4:   /* OR H  (A = A OR H) */
      PrintDebug("OR H");   /* DEBUG */
      OrA(Register.H);
      Register.PC++;
      break;
    case 0xB5:   /* OR L  (A = A OR L) */
      PrintDebug("OR L");   /* DEBUG */
      OrA(Register.L);
      Register.PC++;
      break;
    case 0xB6:   /* OR (HL)  (A = A OR (HL)) */
      PrintDebug("OR (HL)");   /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      OrA(UbyteBuff1);
      Register.PC++;
      break;
    case 0xB7:   /* OR A  (A = A OR A) */
      PrintDebug("OR A");   /* DEBUG */
      OrA(Register.A);
      Register.PC++;
      break;
    case 0xB8:   /* CP B (compare A with B) */
      PrintDebug("CP B");  /* DEBUG */
      CmpA(Register.B);
      Register.PC++;
      break;
    case 0xB9:   /* CP C (compare A with C) */
      PrintDebug("CP C");   /* DEBUG */
      CmpA(Register.C);
      Register.PC++;
      break;
    case 0xBA:   /* CP D (compare A with D) */
      PrintDebug("CP D");   /* DEBUG */
      CmpA(Register.D);
      Register.PC++;
      break;
    case 0xBB:   /* CP E (compare A with E) */
      PrintDebug("CP E");   /* DEBUG */
      CmpA(Register.E);
      Register.PC++;
      break;
    case 0xBC:   /* CP H (compare A with H) */
      PrintDebug("CP H");   /* DEBUG */
      CmpA(Register.H);
      Register.PC++;
      break;
    case 0xBD:   /* CP L (compare A with L) */
      PrintDebug("CP L");   /* DEBUG */
      CmpA(Register.L);
      Register.PC++;
      break;
    case 0xBE:   /* CP (HL) (compare A with value at address [HL]) */
      PrintDebug("CP (HL)");   /* DEBUG */
      UbyteBuff1 = MemoryRead(ReadRegHL());
      CmpA(UbyteBuff1);
      Register.PC++;
      break;
    case 0xBF:   /* CP A (compare A with A) */
      PrintDebug("CP A");   /* DEBUG */
      CmpA(Register.A);
      Register.PC++;
      break;
    case 0xC0:   /* RET NZ    RET if Z = 0 */
      PrintDebug("RET NZ");
      if (GetFlagZ() == 0) {
        cpucycles = 5; /* correct the timing when an action is taken */
        PopPCfromStack();
      } else {
        Register.PC++;
      }
      break;
    case 0xC1:   /* POP BC (Write the value in stack into BC) */
      PrintDebug("POP BC");  /* DEBUG */
      PopFromStack(&Register.B, &Register.C);
      Register.PC++;
      break;
    case 0xC2:   /* JP NZ,nn (jump if Z=0) */
      PrintDebug("JP NZ,nn");   /* DEBUG */
      if (GetFlagZ() == 0) {  /* Jump */
        cpucycles = 4; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      } else {   /* Don't jump */
        Register.PC += 3;
      }
      break;
    case 0xC3:   /* JP nn (Unconditional jump) */
      PrintDebug("JP nn");   /* DEBUG */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      break;
    case 0xC4:   /* CALL NZ,nn (call if Z=0) */
      PrintDebug("CALL NZ,nn");   /* DEBUG */
      if (GetFlagZ() == 0) {  /* Call */
        cpucycles = 6; /* correct the timing when an action is taken */
        PushToStack16(Register.PC + 3);           /* First save the current value of the PC (+3 bytes) */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      } else {   /* Don't call */
        Register.PC += 3;
      }
      break;
    case 0xC5:   /* PUSH BC */
      PrintDebug("PUSH BC");  /* DEBUG */
      PushToStack(Register.B, Register.C);
      Register.PC++;
      break;
    case 0xC6:  /* ADD A,n  (A+=n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("ADD An"); /*," & HEX(UbyteBuff1, 2)) */
      AddToA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xC7:   /* RST 00h */
      PrintDebug("RST 00h");   /* DEBUG */
      PushToStack16(Register.PC + 1);            /* Save current address (+1) */
      Register.PC = 0x00;  /* Jump to 00h */
      break;
    case 0xC8:   /* RET Z   RET if Z = 1 */
      PrintDebug("RET Z");
      if (GetFlagZ() == 0) {
        Register.PC++;
      } else {
        cpucycles = 5; /* correct the timing when an action is taken */
        PopPCfromStack();
      }
      break;
    case 0xC9:   /* RET */
      PrintDebug("RET");
      PopPCfromStack();
      break;
    case 0xCA:   /* JP Z,nn (jump if Z=1) */
      PrintDebug("JP Z,nn");   /* DEBUG */
      if (GetFlagZ() == 0) {  /* Don't jump */
        Register.PC += 3;
      } else {   /* Jump */
        cpucycles = 4; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      }
      break;
    case 0xCB:  /* Here we have a CBxx opcode... */
      CpuInstruction = MemoryRead(Register.PC + 1);  /*  Let's check the second byte! */
      cpucycles = cyctimescb[CpuInstruction]; /* update the instruction's cycles time */
      switch (CpuInstruction) {
        case 0x00:  /* RLC B */
          PrintDebug("RLC B");  /* DEBUG */
          RotateRLC(&Register.B);
          Register.PC += 2;
          break;
        case 0x01:  /* RLC C */
          PrintDebug("RLC C");  /* DEBUG */
          RotateRLC(&Register.C);
          Register.PC += 2;
          break;
        case 0x02:  /* RLC D */
          PrintDebug("RLC D");  /* DEBUG */
          RotateRLC(&Register.D);
          Register.PC += 2;
          break;
        case 0x03:  /* RLC E */
          PrintDebug("RLC E");  /* DEBUG */
          RotateRLC(&Register.E);
          Register.PC += 2;
          break;
        case 0x04:  /* RLC H */
          PrintDebug("RLC H");  /* DEBUG */
          RotateRLC(&Register.H);
          Register.PC += 2;
          break;
        case 0x05:  /* RLC L */
          PrintDebug("RLC L");  /* DEBUG */
          RotateRLC(&Register.L);
          Register.PC += 2;
          break;
        case 0x06:  /* RLC (HL) */
          PrintDebug("RLC (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          RotateRLC(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x07:  /* RLC A */
          PrintDebug("RLC A");  /* DEBUG */
          RotateRLC(&Register.A);
          Register.PC += 2;
          break;
        case 0x08:  /* RRC B */
          PrintDebug("RRC B");  /* DEBUG */
          RotateRRC(&Register.B);
          Register.PC += 2;
          break;
        case 0x09:  /* RRC C */
          PrintDebug("RRC C");  /* DEBUG */
          RotateRRC(&Register.C);
          Register.PC += 2;
          break;
        case 0x0A:  /* RRC D */
          PrintDebug("RRC D");  /* DEBUG */
          RotateRRC(&Register.D);
          Register.PC += 2;
          break;
        case 0x0B:  /* RRC E */
          PrintDebug("RRC E");  /* DEBUG */
          RotateRRC(&Register.E);
          Register.PC += 2;
          break;
        case 0x0C:  /* RRC H */
          PrintDebug("RRC H");  /* DEBUG */
          RotateRRC(&Register.H);
          Register.PC += 2;
          break;
        case 0x0D:  /* RRC L */
          PrintDebug("RRC L");  /* DEBUG */
          RotateRRC(&Register.L);
          Register.PC += 2;
          break;
        case 0x0E:  /* RRC (HL) */
          PrintDebug("RRC (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          RotateRRC(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x0F:  /* RRC A */
          PrintDebug("RRC A");  /* DEBUG */
          RotateRRC(&Register.A);
          Register.PC += 2;
          break;
        case 0x10:  /* RL B */
          PrintDebug("RL B");  /* DEBUG */
          RotateLeftCarry(&Register.B);
          Register.PC += 2;
          break;
        case 0x11:  /* RL C */
          PrintDebug("RL C");  /* DEBUG */
          RotateLeftCarry(&Register.C);
          Register.PC += 2;
          break;
        case 0x12:  /* RL D */
          PrintDebug("RL D");  /* DEBUG */
          RotateLeftCarry(&Register.D);
          Register.PC += 2;
          break;
        case 0x13:  /* RL E */
          PrintDebug("RL E");  /* DEBUG */
          RotateLeftCarry(&Register.E);
          Register.PC += 2;
          break;
        case 0x14:  /* RL H */
          PrintDebug("RL H");  /* DEBUG */
          RotateLeftCarry(&Register.H);
          Register.PC += 2;
          break;
        case 0x15:  /* RL L */
          PrintDebug("RL L");  /* DEBUG */
          RotateLeftCarry(&Register.L);
          Register.PC += 2;
          break;
        case 0x16:  /* RL (HL) */
          PrintDebug("RL (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          RotateLeftCarry(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x17:  /* RL A */
          PrintDebug("RL A");  /* DEBUG */
          RotateLeftCarry(&Register.A);
          Register.PC += 2;
          break;
        case 0x18:  /* RR B */
          PrintDebug("RR B");  /* DEBUG */
          RotateRightCarry(&Register.B);
          Register.PC += 2;
          break;
        case 0x19:  /* RR C */
          PrintDebug("RR C");  /* DEBUG */
          RotateRightCarry(&Register.C);
          Register.PC += 2;
          break;
        case 0x1A:  /* RR D */
          PrintDebug("RR D");  /* DEBUG */
          RotateRightCarry(&Register.D);
          Register.PC += 2;
          break;
        case 0x1B:  /* RR E */
          PrintDebug("RR E");  /* DEBUG */
          RotateRightCarry(&Register.E);
          Register.PC += 2;
          break;
        case 0x1C:  /* RR H */
          PrintDebug("RR H");  /* DEBUG */
          RotateRightCarry(&Register.H);
          Register.PC += 2;
          break;
        case 0x1D:  /* RR L */
          PrintDebug("RR L");  /* DEBUG */
          RotateRightCarry(&Register.L);
          Register.PC += 2;
          break;
        case 0x1E:  /* RR (HL) */
          PrintDebug("RR (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          RotateRightCarry(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x1F:  /* RR A */
          PrintDebug("RR A");  /* DEBUG */
          RotateRightCarry(&Register.A);
          Register.PC += 2;
          break;
        case 0x20:  /* SLA B */
          PrintDebug("SLA B");  /* DEBUG */
          ShiftSLA(&Register.B);
          Register.PC += 2;
          break;
        case 0x21:  /* SLA C */
          PrintDebug("SLA C");  /* DEBUG */
          ShiftSLA(&Register.C);
          Register.PC += 2;
          break;
        case 0x22:  /* SLA D */
          PrintDebug("SLA D");  /* DEBUG */
          ShiftSLA(&Register.D);
          Register.PC += 2;
          break;
        case 0x23:  /* SLA E */
          PrintDebug("SLA E");  /* DEBUG */
          ShiftSLA(&Register.E);
          Register.PC += 2;
          break;
        case 0x24:  /* SLA H */
          PrintDebug("SLA H");  /* DEBUG */
          ShiftSLA(&Register.H);
          Register.PC += 2;
          break;
        case 0x25:  /* SLA L */
          PrintDebug("SLA L");  /* DEBUG */
          ShiftSLA(&Register.L);
          Register.PC += 2;
          break;
        case 0x26:  /* SLA (HL) */
          PrintDebug("SLA (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ShiftSLA(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x27:  /* SLA A */
          PrintDebug("SLA A");  /* DEBUG */
          ShiftSLA(&Register.A);
          Register.PC += 2;
          break;
        case 0x28:  /* SRA B */
          PrintDebug("SRA B");  /* DEBUG */
          ShiftSRA(&Register.B);
          Register.PC += 2;
          break;
        case 0x29:  /* SRA C */
          PrintDebug("SRA C");  /* DEBUG */
          ShiftSRA(&Register.C);
          Register.PC += 2;
          break;
        case 0x2A:  /* SRA D */
          PrintDebug("SRA D");  /* DEBUG */
          ShiftSRA(&Register.D);
          Register.PC += 2;
          break;
        case 0x2B:  /* SRA E */
          PrintDebug("SRA E");  /* DEBUG */
          ShiftSRA(&Register.E);
          Register.PC += 2;
          break;
        case 0x2C:  /* SRA H */
          PrintDebug("SRA H");  /* DEBUG */
          ShiftSRA(&Register.H);
          Register.PC += 2;
          break;
        case 0x2D:  /* SRA L */
          PrintDebug("SRA L");  /* DEBUG */
          ShiftSRA(&Register.L);
          Register.PC += 2;
          break;
        case 0x2E:  /* SRA (HL) */
          PrintDebug("SRA (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ShiftSRA(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x2F:  /* SRA A */
          PrintDebug("SRA A");  /* DEBUG */
          ShiftSRA(&Register.A);
          Register.PC += 2;
          break;
        case 0x30:  /* SWAP B (swaps high and low nibbles of B) */
          PrintDebug("SWAP B");  /* DEBUG */
          SwapUbyte(&Register.B);
          Register.PC += 2;
          break;
        case 0x31:  /* SWAP C (swaps high and low nibbles of C) */
          PrintDebug("SWAP C");  /* DEBUG */
          SwapUbyte(&Register.C);
          Register.PC += 2;
          break;
        case 0x32:  /* SWAP D (swaps high and low nibbles of D) */
          PrintDebug("SWAP D");  /* DEBUG */
          SwapUbyte(&Register.D);
          Register.PC += 2;
          break;
        case 0x33:  /* SWAP E (swaps high and low nibbles of E) */
          PrintDebug("SWAP E");  /* DEBUG */
          SwapUbyte(&Register.E);
          Register.PC += 2;
          break;
        case 0x34:  /* SWAP H (swaps high and low nibbles of H) */
          PrintDebug("SWAP H");  /* DEBUG */
          SwapUbyte(&Register.H);
          Register.PC += 2;
          break;
        case 0x35:  /* SWAP L (swaps high and low nibbles of L) */
          PrintDebug("SWAP L");  /* DEBUG */
          SwapUbyte(&Register.L);
          Register.PC += 2;
          break;
        case 0x36:  /* SWAP (HL) (swaps high and low nibbles of value at address HL) */
          PrintDebug("SWAP (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SwapUbyte(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x37:  /* SWAP A (swaps high and low nibbles of A) */
          PrintDebug("SWAP A");  /* DEBUG */
          SwapUbyte(&Register.A);
          Register.PC += 2;
          break;
        case 0x38:  /* SRL B  (Shift Right B) */
          PrintDebug("SRL B");  /* DEBUG */
          ShiftRightSRL(&Register.B);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x39:  /* SRL C  (Shift Right C) */
          PrintDebug("SRL C");  /* DEBUG */
          ShiftRightSRL(&Register.C);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3A:  /* SRL D  (Shift Right D) */
          PrintDebug("SRL D");  /* DEBUG */
          ShiftRightSRL(&Register.D);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3B:  /* SRL E  (Shift Right E) */
          PrintDebug("SRL E");  /* DEBUG */
          ShiftRightSRL(&Register.E);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3C:  /* SRL H  (Shift Right H) */
          PrintDebug("SRL H");  /* DEBUG */
          ShiftRightSRL(&Register.H);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3D:  /* SRL L  (Shift Right L) */
          PrintDebug("SRL L");  /* DEBUG */
          ShiftRightSRL(&Register.L);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3E:  /* SRL (HL)  (Shift Right value at address HL) */
          PrintDebug("SRL (HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ShiftRightSRL(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);   /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x3F:  /* SRL A  (Shift Right A) */
          PrintDebug("SRL A");  /* DEBUG */
          ShiftRightSRL(&Register.A);  /* Shift right and set some flags */
          Register.PC += 2;
          break;
        case 0x40: /* BIT 0,B (Test bit 0 of register B) */
          PrintDebug("BIT 0,B");  /* DEBUG */
          TestBit0(Register.B);
          Register.PC += 2;
          break;
        case 0x41: /* BIT 0,C (Test bit 0 of register C) */
          PrintDebug("BIT 0,C");  /* DEBUG */
          TestBit0(Register.C);
          Register.PC += 2;
          break;
        case 0x42: /* BIT 0,D (Test bit 0 of register D) */
          PrintDebug("BIT 0,D");  /* DEBUG */
          TestBit0(Register.D);
          Register.PC += 2;
          break;
        case 0x43: /* BIT 0,E (Test bit 0 of register E) */
          PrintDebug("BIT 0,E");  /* DEBUG */
          TestBit0(Register.E);
          Register.PC += 2;
          break;
        case 0x44: /* BIT 0,H (Test bit 0 of register H) */
          PrintDebug("BIT 0,H");  /* DEBUG */
          TestBit0(Register.H);
          Register.PC += 2;
          break;
        case 0x45: /* BIT 0,L (Test bit 0 of register L) */
          PrintDebug("BIT 0,L");  /* DEBUG */
          TestBit0(Register.L);
          Register.PC += 2;
          break;
        case 0x46: /* BIT 0,(HL) (Test bit 0 of value at address HL) */
          PrintDebug("BIT 0,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit0(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x47: /* BIT 0,A (Test bit 0 of register A) */
          PrintDebug("BIT 0,A");  /* DEBUG */
          TestBit0(Register.A);
          Register.PC += 2;
          break;
        case 0x48: /* BIT 1,B (Test bit 1 of register B) */
          PrintDebug("BIT 1,B");  /* DEBUG */
          TestBit1(Register.B);
          Register.PC += 2;
          break;
        case 0x49: /* BIT 1,C (Test bit 1 of register C) */
          PrintDebug("BIT 1,C");  /* DEBUG */
          TestBit1(Register.C);
          Register.PC += 2;
          break;
        case 0x4A: /* BIT 1,D (Test bit 1 of register D) */
          PrintDebug("BIT 1,D");  /* DEBUG */
          TestBit1(Register.D);
          Register.PC += 2;
          break;
        case 0x4B: /* BIT 1,E (Test bit 1 of register E) */
          PrintDebug("BIT 1,E");  /* DEBUG */
          TestBit1(Register.E);
          Register.PC += 2;
          break;
        case 0x4C: /* BIT 1,H (Test bit 1 of register H) */
          PrintDebug("BIT 1,H");  /* DEBUG */
          TestBit1(Register.H);
          Register.PC += 2;
          break;
        case 0x4D: /* BIT 1,L (Test bit 1 of register L) */
          PrintDebug("BIT 1,L");  /* DEBUG */
          TestBit1(Register.L);
          Register.PC += 2;
          break;
        case 0x4E: /* BIT 1,(HL) (Test bit 1 of value at address HL) */
          PrintDebug("BIT 1,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit1(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x4F: /* BIT 1,A (Test bit 1 of register A) */
          PrintDebug("BIT 1,A");  /* DEBUG */
          TestBit1(Register.A);
          Register.PC += 2;
          break;
        case 0x50: /* BIT 2,B (Test bit 2 of register B) */
          PrintDebug("BIT 2,B");  /* DEBUG */
          TestBit2(Register.B);
          Register.PC += 2;
          break;
        case 0x51: /* BIT 2,C (Test bit 2 of register C) */
          PrintDebug("BIT 2,C");  /* DEBUG */
          TestBit2(Register.C);
          Register.PC += 2;
          break;
        case 0x52: /* BIT 2,D (Test bit 2 of register D) */
          PrintDebug("BIT 2,D");  /* DEBUG */
          TestBit2(Register.D);
          Register.PC += 2;
          break;
        case 0x53: /* BIT 2,E (Test bit 2 of register E) */
          PrintDebug("BIT 2,E");  /* DEBUG */
          TestBit2(Register.E);
          Register.PC += 2;
          break;
        case 0x54: /* BIT 2,H (Test bit 2 of register H) */
          PrintDebug("BIT 2,H");  /* DEBUG */
          TestBit2(Register.H);
          Register.PC += 2;
          break;
        case 0x55: /* BIT 2,L (Test bit 2 of register L) */
          PrintDebug("BIT 2,L");  /* DEBUG */
          TestBit2(Register.L);
          Register.PC += 2;
          break;
        case 0x56: /* BIT 2,(HL) (Test bit 2 of value at [HL]) */
          PrintDebug("BIT 2,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit2(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x57: /* BIT 2,A (Test bit 2 of register A) */
          PrintDebug("BIT 2,A");  /* DEBUG */
          TestBit2(Register.A);
          Register.PC += 2;
          break;
        case 0x58: /* BIT 3,B (Test bit 3 of register B) */
          PrintDebug("BIT 3,B");  /* DEBUG */
          TestBit3(Register.B);
          Register.PC += 2;
          break;
        case 0x59: /* BIT 3,C (Test bit 3 of register C) */
          PrintDebug("BIT 3,C");  /* DEBUG */
          TestBit3(Register.C);
          Register.PC += 2;
          break;
        case 0x5A: /* BIT 3,D (Test bit 3 of register D) */
          PrintDebug("BIT 3,D");  /* DEBUG */
          TestBit3(Register.D);
          Register.PC += 2;
          break;
        case 0x5B: /* BIT 3,E (Test bit 3 of register E) */
          PrintDebug("BIT 3,E");  /* DEBUG */
          TestBit3(Register.E);
          Register.PC += 2;
          break;
        case 0x5C: /* BIT 3,H (Test bit 3 of register H) */
          PrintDebug("BIT 3,H");  /* DEBUG */
          TestBit3(Register.H);
          Register.PC += 2;
          break;
        case 0x5D: /* BIT 3,L (Test bit 3 of register L) */
          PrintDebug("BIT 3,L");  /* DEBUG */
          TestBit3(Register.L);
          Register.PC += 2;
          break;
        case 0x5E: /* BIT 3,(HL) (Test bit 3 of value at [HL]) */
          PrintDebug("BIT 3,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit3(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x5F: /* BIT 3,A (Test bit 3 of register A) */
          PrintDebug("BIT 3,A");  /* DEBUG */
          TestBit3(Register.A);
          Register.PC += 2;
          break;
        case 0x60: /* BIT 4,B (Test bit 4 of register B) */
          PrintDebug("BIT 4,B");  /* DEBUG */
          TestBit4(Register.B);
          Register.PC += 2;
          break;
        case 0x61: /* BIT 4,C (Test bit 4 of register C) */
          PrintDebug("BIT 4,C");  /* DEBUG */
          TestBit4(Register.C);
          Register.PC += 2;
          break;
        case 0x62: /* BIT 4,D (Test bit 4 of register D) */
          PrintDebug("BIT 4,D");  /* DEBUG */
          TestBit4(Register.D);
          Register.PC += 2;
          break;
        case 0x63: /* BIT 4,E (Test bit 4 of register E) */
          PrintDebug("BIT 4,E");  /* DEBUG */
          TestBit4(Register.E);
          Register.PC += 2;
          break;
        case 0x64: /* BIT 4,H (Test bit 4 of register H) */
          PrintDebug("BIT 4,H");  /* DEBUG */
          TestBit4(Register.H);
          Register.PC += 2;
          break;
        case 0x65: /* BIT 4,L (Test bit 4 of register L) */
          PrintDebug("BIT 4,L");  /* DEBUG */
          TestBit4(Register.L);
          Register.PC += 2;
          break;
        case 0x66: /* BIT 4,(HL) (Test bit 4 of value at [HL]) */
          PrintDebug("BIT 4,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit4(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x67: /* BIT 4,A (Test bit 4 of register A) */
          PrintDebug("BIT 4,A");  /* DEBUG */
          TestBit4(Register.A);
          Register.PC += 2;
          break;
        case 0x68: /* BIT 5,B (Test bit 5 of register B) */
          PrintDebug("BIT 5,B");  /* DEBUG */
          TestBit5(Register.B);
          Register.PC += 2;
          break;
        case 0x69: /* BIT 5,C (Test bit 5 of register C) */
          PrintDebug("BIT 5,C");  /* DEBUG */
          TestBit5(Register.C);
          Register.PC += 2;
          break;
        case 0x6A: /* BIT 5,D (Test bit 5 of register D) */
          PrintDebug("BIT 5,D");  /* DEBUG */
          TestBit5(Register.D);
          Register.PC += 2;
          break;
        case 0x6B: /* BIT 5,E (Test bit 5 of register E) */
          PrintDebug("BIT 5,E");  /* DEBUG */
          TestBit5(Register.E);
          Register.PC += 2;
          break;
        case 0x6C: /* BIT 5,H (Test bit 5 of register H) */
          PrintDebug("BIT 5,H");  /* DEBUG */
          TestBit5(Register.H);
          Register.PC += 2;
          break;
        case 0x6D: /* BIT 5,L (Test bit 5 of register L) */
          PrintDebug("BIT 5,L");  /* DEBUG */
          TestBit5(Register.L);
          Register.PC += 2;
          break;
        case 0x6E: /* BIT 5,(HL) (Test bit 5 of value at [HL]) */
          PrintDebug("BIT 5,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit5(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x6F: /* BIT 5,A (Test bit 5 of register A) */
          PrintDebug("BIT 5,A");  /* DEBUG */
          TestBit5(Register.A);
          Register.PC += 2;
          break;
        case 0x70: /* BIT 6,B (Test bit 6 of register B) */
          PrintDebug("BIT 6,B");  /* DEBUG */
          TestBit6(Register.B);
          Register.PC += 2;
          break;
        case 0x71: /* BIT 6,C (Test bit 6 of register C) */
          PrintDebug("BIT 6,C");  /* DEBUG */
          TestBit6(Register.C);
          Register.PC += 2;
          break;
        case 0x72: /* BIT 6,D (Test bit 6 of register D) */
          PrintDebug("BIT 6,D");  /* DEBUG */
          TestBit6(Register.D);
          Register.PC += 2;
          break;
        case 0x73: /* BIT 6,E (Test bit 6 of register E) */
          PrintDebug("BIT 6,E");  /* DEBUG */
          TestBit6(Register.E);
          Register.PC += 2;
          break;
        case 0x74: /* BIT 6,H (Test bit 6 of register H) */
          PrintDebug("BIT 6,H");  /* DEBUG */
          TestBit6(Register.H);
          Register.PC += 2;
          break;
        case 0x75: /* BIT 6,L (Test bit 6 of register L) */
          PrintDebug("BIT 6,L");  /* DEBUG */
          TestBit6(Register.L);
          Register.PC += 2;
          break;
        case 0x76: /* BIT 6,(HL) (Test bit 6 of value at [HL]) */
          PrintDebug("BIT 6,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit6(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x77: /* BIT 6,A (Test bit 6 of register A) */
          PrintDebug("BIT 6,A");  /* DEBUG */
          TestBit6(Register.A);
          Register.PC += 2;
          break;
        case 0x78: /* BIT 7,B (Test bit 7 of register B) */
          PrintDebug("BIT 7,B");  /* DEBUG */
          TestBit7(Register.B);
          Register.PC += 2;
          break;
        case 0x79: /* BIT 7,C (Test bit 7 of register C) */
          PrintDebug("BIT 7,C");  /* DEBUG */
          TestBit7(Register.C);
          Register.PC += 2;
          break;
        case 0x7A: /* BIT 7,D (Test bit 7 of register D) */
          PrintDebug("BIT 7,D");  /* DEBUG */
          TestBit7(Register.D);
          Register.PC += 2;
          break;
        case 0x7B: /* BIT 7,E (Test bit 7 of register E) */
          PrintDebug("BIT 7,E");  /* DEBUG */
          TestBit7(Register.E);
          Register.PC += 2;
          break;
        case 0x7C: /* BIT 7,H (Test bit 7 of register H) */
          PrintDebug("BIT 7,H");  /* DEBUG */
          TestBit7(Register.H);
          Register.PC += 2;
          break;
        case 0x7D: /* BIT 7,L (Test bit 7 of register L) */
          PrintDebug("BIT 7,L");  /* DEBUG */
          TestBit7(Register.L);
          Register.PC += 2;
          break;
        case 0x7E: /* BIT 7,(HL) (Test bit 7 of value at address HL) */
          PrintDebug("BIT 7,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          TestBit7(UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x7F: /* BIT 7,A (Test bit 7 of register A) */
          PrintDebug("BIT 7,A");  /* DEBUG */
          TestBit7(Register.A);
          Register.PC += 2;
          break;
        case 0x80: /* RES 0,B (Reset bit 0 of register B) */
          PrintDebug("RES 0,B");  /* DEBUG */
          ResetBit0(&Register.B);
          Register.PC += 2;
          break;
        case 0x81: /* RES 0,C (Reset bit 0 of register C) */
          PrintDebug("RES 0,C");  /* DEBUG */
          ResetBit0(&Register.C);
          Register.PC += 2;
          break;
        case 0x82: /* RES 0,D (Reset bit 0 of register D) */
          PrintDebug("RES 0,D");  /* DEBUG */
          ResetBit0(&Register.D);
          Register.PC += 2;
          break;
        case 0x83: /* RES 0,E (Reset bit 0 of register E) */
          PrintDebug("RES 0,E");  /* DEBUG */
          ResetBit0(&Register.E);
          Register.PC += 2;
          break;
        case 0x84: /* RES 0,H (Reset bit 0 of register H) */
          PrintDebug("RES 0,H");  /* DEBUG */
          ResetBit0(&Register.H);
          Register.PC += 2;
          break;
        case 0x85: /* RES 0,L (Reset bit 0 of register L) */
          PrintDebug("RES 0,L");  /* DEBUG */
          ResetBit0(&Register.L);
          Register.PC += 2;
          break;
        case 0x86: /* RES 0,(HL) (Reset bit 0 of value at [HL]) */
          PrintDebug("RES 0,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit0(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x87: /* RES 0,A (Reset bit 0 of register A) */
          PrintDebug("RES 0,A");  /* DEBUG */
          ResetBit0(&Register.A);
          Register.PC += 2;
          break;
        case 0x88: /* RES 1,B (Reset bit 1 of register B) */
          PrintDebug("RES 1,B");  /* DEBUG */
          ResetBit1(&Register.B);
          Register.PC += 2;
          break;
        case 0x89: /* RES 1,C (Reset bit 1 of register C) */
          PrintDebug("RES 1,C");  /* DEBUG */
          ResetBit1(&Register.C);
          Register.PC += 2;
          break;
        case 0x8A: /* RES 1,D (Reset bit 1 of register D) */
          PrintDebug("RES 1,D");  /* DEBUG */
          ResetBit1(&Register.D);
          Register.PC += 2;
          break;
        case 0x8B: /* RES 1,E (Reset bit 1 of register E) */
          PrintDebug("RES 1,E");  /* DEBUG */
          ResetBit1(&Register.E);
          Register.PC += 2;
          break;
        case 0x8C: /* RES 1,H (Reset bit 1 of register H) */
          PrintDebug("RES 1,H");  /* DEBUG */
          ResetBit1(&Register.H);
          Register.PC += 2;
          break;
        case 0x8D: /* RES 1,L (Reset bit 1 of register L) */
          PrintDebug("RES 1,L");  /* DEBUG */
          ResetBit1(&Register.L);
          Register.PC += 2;
          break;
        case 0x8E: /* RES 1,(HL) (Reset bit 1 of value at [HL]) */
          PrintDebug("RES 1,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit1(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x8F: /* RES 1,A (Reset bit 1 of register A) */
          PrintDebug("RES 1,A");  /* DEBUG */
          ResetBit1(&Register.A);
          Register.PC += 2;
          break;
        case 0x90: /* RES 2,B (Reset bit 2 of register B) */
          PrintDebug("RES 2,B");  /* DEBUG */
          ResetBit2(&Register.B);
          Register.PC += 2;
          break;
        case 0x91: /* RES 2,C (Reset bit 2 of register C) */
          PrintDebug("RES 2,C");  /* DEBUG */
          ResetBit2(&Register.C);
          Register.PC += 2;
          break;
        case 0x92: /* RES 2,D (Reset bit 2 of register D) */
          PrintDebug("RES 2,D");  /* DEBUG */
          ResetBit2(&Register.D);
          Register.PC += 2;
          break;
        case 0x93: /* RES 2,E (Reset bit 2 of register E) */
          PrintDebug("RES 2,E");  /* DEBUG */
          ResetBit2(&Register.E);
          Register.PC += 2;
          break;
        case 0x94: /* RES 2,H (Reset bit 2 of register H) */
          PrintDebug("RES 2,H");  /* DEBUG */
          ResetBit2(&Register.H);
          Register.PC += 2;
          break;
        case 0x95: /* RES 2,L (Reset bit 2 of register L) */
          PrintDebug("RES 2,L");  /* DEBUG */
          ResetBit2(&Register.L);
          Register.PC += 2;
          break;
        case 0x96: /* RES 2,(HL) (Reset bit 2 of value at [HL]) */
          PrintDebug("RES 2,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit2(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x97: /* RES 2,A (Reset bit 2 of register A) */
          PrintDebug("RES 2,A");  /* DEBUG */
          ResetBit2(&Register.A);
          Register.PC += 2;
          break;
        case 0x98: /* RES 3,B (Reset bit 3 of register B) */
          PrintDebug("RES 3,B");  /* DEBUG */
          ResetBit3(&Register.B);
          Register.PC += 2;
          break;
        case 0x99: /* RES 3,C (Reset bit 3 of register C) */
          PrintDebug("RES 3,C");  /* DEBUG */
          ResetBit3(&Register.C);
          Register.PC += 2;
          break;
        case 0x9A: /* RES 3,D (Reset bit 3 of register D) */
          PrintDebug("RES 3,D");  /* DEBUG */
          ResetBit3(&Register.D);
          Register.PC += 2;
          break;
        case 0x9B: /* RES 3,E (Reset bit 3 of register E) */
          PrintDebug("RES 3,E");  /* DEBUG */
          ResetBit3(&Register.E);
          Register.PC += 2;
          break;
        case 0x9C: /* RES 3,H (Reset bit 3 of register H) */
          PrintDebug("RES 3,H");  /* DEBUG */
          ResetBit3(&Register.H);
          Register.PC += 2;
          break;
        case 0x9D: /* RES 3,L (Reset bit 3 of register L) */
          PrintDebug("RES 3,L");  /* DEBUG */
          ResetBit3(&Register.L);
          Register.PC += 2;
          break;
        case 0x9E: /* RES 3,(HL) (Reset bit 3 of value at [HL]) */
          PrintDebug("RES 3,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit3(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0x9F: /* RES 3,A (Reset bit 3 of register A) */
          PrintDebug("RES 3,A");  /* DEBUG */
          ResetBit3(&Register.A);
          Register.PC += 2;
          break;
        case 0xA0: /* RES 4,B (Reset bit 4 of register B) */
          PrintDebug("RES 4,B");  /* DEBUG */
          ResetBit4(&Register.B);
          Register.PC += 2;
          break;
        case 0xA1: /* RES 4,C (Reset bit 4 of register C) */
          PrintDebug("RES 4,C");  /* DEBUG */
          ResetBit4(&Register.C);
          Register.PC += 2;
          break;
        case 0xA2: /* RES 4,D (Reset bit 4 of register D) */
          PrintDebug("RES 4,D");  /* DEBUG */
          ResetBit4(&Register.D);
          Register.PC += 2;
          break;
        case 0xA3: /* RES 4,E (Reset bit 4 of register E) */
          PrintDebug("RES 4,E");  /* DEBUG */
          ResetBit4(&Register.E);
          Register.PC += 2;
          break;
        case 0xA4: /* RES 4,H (Reset bit 4 of register H) */
          PrintDebug("RES 4,H");  /* DEBUG */
          ResetBit4(&Register.H);
          Register.PC += 2;
          break;
        case 0xA5: /* RES 4,L (Reset bit 4 of register L) */
          PrintDebug("RES 4,L");  /* DEBUG */
          ResetBit4(&Register.L);
          Register.PC += 2;
          break;
        case 0xA6: /* RES 4,(HL) (Reset bit 4 of value at [HL]) */
          PrintDebug("RES 4,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit4(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xA7: /* RES 4,A (Reset bit 4 of register A) */
          PrintDebug("RES 4,A");  /* DEBUG */
          ResetBit4(&Register.A);
          Register.PC += 2;
          break;
        case 0xA8: /* RES 5,B (Reset bit 5 of register B) */
          PrintDebug("RES 5,B");  /* DEBUG */
          ResetBit5(&Register.B);
          Register.PC += 2;
          break;
        case 0xA9: /* RES 5,C (Reset bit 5 of register C) */
          PrintDebug("RES 5,C");  /* DEBUG */
          ResetBit5(&Register.C);
          Register.PC += 2;
          break;
        case 0xAA: /* RES 5,D (Reset bit 5 of register D) */
          PrintDebug("RES 5,D");  /* DEBUG */
          ResetBit5(&Register.D);
          Register.PC += 2;
          break;
        case 0xAB: /* RES 5,E (Reset bit 5 of register E) */
          PrintDebug("RES 5,E");  /* DEBUG */
          ResetBit5(&Register.E);
          Register.PC += 2;
          break;
        case 0xAC: /* RES 5,H (Reset bit 5 of register H) */
          PrintDebug("RES 5,H");  /* DEBUG */
          ResetBit5(&Register.H);
          Register.PC += 2;
          break;
        case 0xAD: /* RES 5,L (Reset bit 5 of register L) */
          PrintDebug("RES 5,L");  /* DEBUG */
          ResetBit5(&Register.L);
          Register.PC += 2;
          break;
        case 0xAE: /* RES 5,(HL) (Reset bit 5 of value at [HL]) */
          PrintDebug("RES 5,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit5(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xAF: /* RES 5,A (Reset bit 5 of register A) */
          PrintDebug("RES 5,A");  /* DEBUG */
          ResetBit5(&Register.A);
          Register.PC += 2;
          break;
        case 0xB0: /* RES 6,B (Reset bit 6 of register B) */
          PrintDebug("RES 6,B");  /* DEBUG */
          ResetBit6(&Register.B);
          Register.PC += 2;
          break;
        case 0xB1: /* RES 6,C (Reset bit 6 of register C) */
          PrintDebug("RES 6,C");  /* DEBUG */
          ResetBit6(&Register.C);
          Register.PC += 2;
          break;
        case 0xB2: /* RES 6,D (Reset bit 6 of register D) */
          PrintDebug("RES 6,D");  /* DEBUG */
          ResetBit6(&Register.D);
          Register.PC += 2;
          break;
        case 0xB3: /* RES 6,E (Reset bit 6 of register E) */
          PrintDebug("RES 6,E");  /* DEBUG */
          ResetBit6(&Register.E);
          Register.PC += 2;
          break;
        case 0xB4: /* RES 6,H (Reset bit 6 of register H) */
          PrintDebug("RES 6,H");  /* DEBUG */
          ResetBit6(&Register.H);
          Register.PC += 2;
          break;
        case 0xB5: /* RES 6,L (Reset bit 6 of register L) */
          PrintDebug("RES 6,L");  /* DEBUG */
          ResetBit6(&Register.L);
          Register.PC += 2;
          break;
        case 0xB6: /* RES 6,(HL) (Reset bit 6 of value at [HL]) */
          PrintDebug("RES 6,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit6(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xB7: /* RES 6,A (Reset bit 6 of register A) */
          PrintDebug("RES 6,A");  /* DEBUG */
          ResetBit6(&Register.A);
          Register.PC += 2;
          break;
        case 0xB8: /* RES 7,B (Reset bit 7 of register B) */
          PrintDebug("RES 7,B");  /* DEBUG */
          ResetBit7(&Register.B);
          Register.PC += 2;
          break;
        case 0xB9: /* RES 7,C (Reset bit 7 of register C) */
          PrintDebug("RES 7,C");  /* DEBUG */
          ResetBit7(&Register.C);
          Register.PC += 2;
          break;
        case 0xBA: /* RES 7,D (Reset bit 7 of register D) */
          PrintDebug("RES 7,D");  /* DEBUG */
          ResetBit7(&Register.D);
          Register.PC += 2;
          break;
        case 0xBB: /* RES 7,E (Reset bit 7 of register E) */
          PrintDebug("RES 7,E");  /* DEBUG */
          ResetBit7(&Register.E);
          Register.PC += 2;
          break;
        case 0xBC: /* RES 7,H (Reset bit 7 of register H) */
          PrintDebug("RES 7,H");  /* DEBUG */
          ResetBit7(&Register.H);
          Register.PC += 2;
          break;
        case 0xBD: /* RES 7,L (Reset bit 7 of register L) */
          PrintDebug("RES 7,L");  /* DEBUG */
          ResetBit7(&Register.L);
          Register.PC += 2;
          break;
        case 0xBE: /* RES 7,(HL) (Reset bit 7 of value at [HL]) */
          PrintDebug("RES 7,(HL)");  /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          ResetBit7(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xBF: /* RES 7,A (Reset bit 7 of register A) */
          PrintDebug("RES 7,A");  /* DEBUG */
          ResetBit7(&Register.A);
          Register.PC += 2;
          break;
        case 0xC0:  /* SET 0,B  (Set bit 0 of B) */
          PrintDebug("SET 0,B");   /* DEBUG */
          SetBit0(&Register.B);
          Register.PC += 2;
          break;
        case 0xC1:  /* SET 0,C  (Set bit 0 of C) */
          PrintDebug("SET 0,C");   /* DEBUG */
          SetBit0(&Register.C);
          Register.PC += 2;
          break;
        case 0xC2:  /* SET 0,D  (Set bit 0 of D) */
          PrintDebug("SET 0,D");   /* DEBUG */
          SetBit0(&Register.D);
          Register.PC += 2;
          break;
        case 0xC3:  /* SET 0,E  (Set bit 0 of E) */
          PrintDebug("SET 0,E");   /* DEBUG */
          SetBit0(&Register.E);
          Register.PC += 2;
          break;
        case 0xC4:  /* SET 0,H  (Set bit 0 of H) */
          PrintDebug("SET 0,H");   /* DEBUG */
          SetBit0(&Register.H);
          Register.PC += 2;
          break;
        case 0xC5:  /* SET 0,L  (Set bit 0 of L) */
          PrintDebug("SET 0,L");   /* DEBUG */
          SetBit0(&Register.L);
          Register.PC += 2;
          break;
        case 0xC6:  /* SET 0,(HL)  (Set bit 0 of value at HL) */
          PrintDebug("SET 0,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit0(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xC7:  /* SET 0,A  (Set bit 0 of A) */
          PrintDebug("SET 0,A");   /* DEBUG */
          SetBit0(&Register.A);
          Register.PC += 2;
          break;
        case 0xC8:  /* SET 1,B  (Set bit 1 of B) */
          PrintDebug("SET 1,B");   /* DEBUG */
          SetBit1(&Register.B);
          Register.PC += 2;
          break;
        case 0xC9:  /* SET 1,C  (Set bit 1 of C) */
          PrintDebug("SET 1,C");   /* DEBUG */
          SetBit1(&Register.C);
          Register.PC += 2;
          break;
        case 0xCA:  /* SET 1,D  (Set bit 1 of D) */
          PrintDebug("SET 1,D");   /* DEBUG */
          SetBit1(&Register.D);
          Register.PC += 2;
          break;
        case 0xCB:  /* SET 1,E  (Set bit 1 of E) */
          PrintDebug("SET 1,E");   /* DEBUG */
          SetBit1(&Register.E);
          Register.PC += 2;
          break;
        case 0xCC:  /* SET 1,H  (Set bit 1 of H) */
          PrintDebug("SET 1,H");   /* DEBUG */
          SetBit1(&Register.H);
          Register.PC += 2;
          break;
        case 0xCD:  /* SET 1,L  (Set bit 1 of L) */
          PrintDebug("SET 1,L");   /* DEBUG */
          SetBit1(&Register.L);
          Register.PC += 2;
          break;
        case 0xCE:  /* SET 1,(HL)  (Set bit 1 of value at HL) */
          PrintDebug("SET 1,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit1(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xCF:  /* SET 1,A  (Set bit 1 of A) */
          PrintDebug("SET 1,A");   /* DEBUG */
          SetBit1(&Register.A);
          Register.PC += 2;
          break;
        case 0xD0:  /* SET 2,B  (Set bit 2 of B) */
          PrintDebug("SET 2,B");   /* DEBUG */
          SetBit2(&Register.B);
          Register.PC += 2;
          break;
        case 0xD1:  /* SET 2,C  (Set bit 2 of C) */
          PrintDebug("SET 2,C");   /* DEBUG */
          SetBit2(&Register.C);
          Register.PC += 2;
          break;
        case 0xD2:  /* SET 2,D  (Set bit 2 of D) */
          PrintDebug("SET 2,D");   /* DEBUG */
          SetBit2(&Register.D);
          Register.PC += 2;
          break;
        case 0xD3:  /* SET 2,E  (Set bit 2 of E) */
          PrintDebug("SET 2,E");   /* DEBUG */
          SetBit2(&Register.E);
          Register.PC += 2;
          break;
        case 0xD4:  /* SET 2,H  (Set bit 2 of H) */
          PrintDebug("SET 2,H");   /* DEBUG */
          SetBit2(&Register.H);
          Register.PC += 2;
          break;
        case 0xD5:  /* SET 2,L  (Set bit 2 of L) */
          PrintDebug("SET 2,L");   /* DEBUG */
          SetBit2(&Register.L);
          Register.PC += 2;
          break;
        case 0xD6:  /* SET 2,(HL)  (Set bit 2 of value at HL) */
          PrintDebug("SET 2,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit2(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xD7:  /* SET 2,A  (Set bit 2 of A) */
          PrintDebug("SET 2,A");   /* DEBUG */
          SetBit2(&Register.A);
          Register.PC += 2;
          break;
        case 0xD8:  /* SET 3,B  (Set bit 3 of B) */
          PrintDebug("SET 3,B");   /* DEBUG */
          SetBit3(&Register.B);
          Register.PC += 2;
          break;
        case 0xD9:  /* SET 3,C  (Set bit 3 of C) */
          PrintDebug("SET 3,C");   /* DEBUG */
          SetBit3(&Register.C);
          Register.PC += 2;
          break;
        case 0xDA:  /* SET 3,D  (Set bit 3 of D) */
          PrintDebug("SET 3,D");   /* DEBUG */
          SetBit3(&Register.D);
          Register.PC += 2;
          break;
        case 0xDB:  /* SET 3,E  (Set bit 3 of E) */
          PrintDebug("SET 3,E");   /* DEBUG */
          SetBit3(&Register.E);
          Register.PC += 2;
          break;
        case 0xDC:  /* SET 3,H  (Set bit 3 of H) */
          PrintDebug("SET 3,H");   /* DEBUG */
          SetBit3(&Register.H);
          Register.PC += 2;
          break;
        case 0xDD:  /* SET 3,L  (Set bit 3 of L) */
          PrintDebug("SET 3,L");   /* DEBUG */
          SetBit3(&Register.L);
          Register.PC += 2;
          break;
        case 0xDE:  /* SET 3,(HL)  (Set bit 3 of value at HL) */
          PrintDebug("SET 3,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit3(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xDF:  /* SET 3,A  (Set bit 3 of A) */
          PrintDebug("SET 3,A");   /* DEBUG */
          SetBit3(&Register.A);
          Register.PC += 2;
          break;
        case 0xE0:  /* SET 4,B  (Set bit 4 of B) */
          PrintDebug("SET 4,B");   /* DEBUG */
          SetBit4(&Register.B);
          Register.PC += 2;
          break;
        case 0xE1:  /* SET 4,C  (Set bit 4 of C) */
          PrintDebug("SET 4,C");   /* DEBUG */
          SetBit4(&Register.C);
          Register.PC += 2;
          break;
        case 0xE2:  /* SET 4,D  (Set bit 4 of D) */
          PrintDebug("SET 4,D");   /* DEBUG */
          SetBit4(&Register.D);
          Register.PC += 2;
          break;
        case 0xE3:  /* SET 4,E  (Set bit 4 of E) */
          PrintDebug("SET 4,E");   /* DEBUG */
          SetBit4(&Register.E);
          Register.PC += 2;
          break;
        case 0xE4:  /* SET 4,H  (Set bit 4 of H) */
          PrintDebug("SET 4,H");   /* DEBUG */
          SetBit4(&Register.H);
          Register.PC += 2;
          break;
        case 0xE5:  /* SET 4,L  (Set bit 4 of L) */
          PrintDebug("SET 4,L");   /* DEBUG */
          SetBit4(&Register.L);
          Register.PC += 2;
          break;
        case 0xE6:  /* SET 4,(HL)  (Set bit 4 of value at HL) */
          PrintDebug("SET 4,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit4(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xE7:  /* SET 4,A  (Set bit 4 of A) */
          PrintDebug("SET 4,A");   /* DEBUG */
          SetBit4(&Register.A);
          Register.PC += 2;
          break;
        case 0xE8:  /* SET 5,B  (Set bit 5 of B) */
          PrintDebug("SET 5,B");   /* DEBUG */
          SetBit5(&Register.B);
          Register.PC += 2;
          break;
        case 0xE9:  /* SET 5,C  (Set bit 5 of C) */
          PrintDebug("SET 5,C");   /* DEBUG */
          SetBit5(&Register.C);
          Register.PC += 2;
          break;
        case 0xEA:  /* SET 5,D  (Set bit 5 of D) */
          PrintDebug("SET 5,D");   /* DEBUG */
          SetBit5(&Register.D);
          Register.PC += 2;
          break;
        case 0xEB:  /* SET 5,E  (Set bit 5 of E) */
          PrintDebug("SET 5,E");   /* DEBUG */
          SetBit5(&Register.E);
          Register.PC += 2;
          break;
        case 0xEC:  /* SET 5,H  (Set bit 5 of H) */
          PrintDebug("SET 5,H");   /* DEBUG */
          SetBit5(&Register.H);
          Register.PC += 2;
          break;
        case 0xED:  /* SET 5,L  (Set bit 5 of L) */
          PrintDebug("SET 5,L");   /* DEBUG */
          SetBit5(&Register.L);
          Register.PC += 2;
          break;
        case 0xEE:  /* SET 5,(HL)  (Set bit 5 of value at HL) */
          PrintDebug("SET 5,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit5(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xEF:  /* SET 5,A  (Set bit 5 of A) */
          PrintDebug("SET 5,A");   /* DEBUG */
          SetBit5(&Register.A);
          Register.PC += 2;
          break;
        case 0xF0:  /* SET 6,B  (Set bit 6 of B) */
          PrintDebug("SET 6,B");   /* DEBUG */
          SetBit6(&Register.B);
          Register.PC += 2;
          break;
        case 0xF1:  /* SET 6,C  (Set bit 6 of C) */
          PrintDebug("SET 6,C");   /* DEBUG */
          SetBit6(&Register.C);
          Register.PC += 2;
          break;
        case 0xF2:  /* SET 6,D  (Set bit 6 of D) */
          PrintDebug("SET 6,D");   /* DEBUG */
          SetBit6(&Register.D);
          Register.PC += 2;
          break;
        case 0xF3:  /* SET 6,E  (Set bit 6 of E) */
          PrintDebug("SET 6,E");   /* DEBUG */
          SetBit6(&Register.E);
          Register.PC += 2;
          break;
        case 0xF4:  /* SET 6,H  (Set bit 6 of H) */
          PrintDebug("SET 6,H");   /* DEBUG */
          SetBit6(&Register.H);
          Register.PC += 2;
          break;
        case 0xF5:  /* SET 6,L  (Set bit 6 of L) */
          PrintDebug("SET 6,L");   /* DEBUG */
          SetBit6(&Register.L);
          Register.PC += 2;
          break;
        case 0xF6:  /* SET 6,(HL)  (Set bit 6 of value at HL) */
          PrintDebug("SET 6,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit6(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xF7:  /* SET 6,A  (Set bit 6 of A) */
          PrintDebug("SET 6,A");   /* DEBUG */
          SetBit6(&Register.A);
          Register.PC += 2;
          break;
        case 0xF8:  /* SET 7,B  (Set bit 7 of B) */
          PrintDebug("SET 7,B");   /* DEBUG */
          SetBit7(&Register.B);
          Register.PC += 2;
          break;
        case 0xF9:  /* SET 7,C  (Set bit 7 of C) */
          PrintDebug("SET 7,C");   /* DEBUG */
          SetBit7(&Register.C);
          Register.PC += 2;
          break;
        case 0xFA:  /* SET 7,D  (Set bit 7 of D) */
          PrintDebug("SET 7,D");   /* DEBUG */
          SetBit7(&Register.D);
          Register.PC += 2;
          break;
        case 0xFB:  /* SET 7,E  (Set bit 7 of E) */
          PrintDebug("SET 7,E");   /* DEBUG */
          SetBit7(&Register.E);
          Register.PC += 2;
          break;
        case 0xFC:  /* SET 7,H  (Set bit 7 of H) */
          PrintDebug("SET 7,H");   /* DEBUG */
          SetBit7(&Register.H);
          Register.PC += 2;
          break;
        case 0xFD:  /* SET 7,L  (Set bit 7 of L) */
          PrintDebug("SET 7,L");   /* DEBUG */
          SetBit7(&Register.L);
          Register.PC += 2;
          break;
        case 0xFE:  /* SET 7,(HL)  (Set bit 7 of value at HL) */
          PrintDebug("SET 7,(HL)");   /* DEBUG */
          UbyteBuff1 = MemoryRead(ReadRegHL());
          SetBit7(&UbyteBuff1);
          MemoryWrite(ReadRegHL(), UbyteBuff1);
          Register.PC += 2;
          break;
        case 0xFF:  /* SET 7,A  (Set bit 7 of A) */
          PrintDebug("SET 7,A");   /* DEBUG */
          SetBit7(&Register.A);
          Register.PC += 2;
          break;
        default:
          PrintDebug("Unknown opcode"); /* at " & HEX(Register.PC) & ": " & HEX(CpuInstruction, 4)) */
          QuitEmulator = 1;
          break;
      }
      break;
    case 0xCC:   /* CALL Z,nn (jump if Z=1) */
      PrintDebug("CALL Z,nn");   /* DEBUG */
      if (GetFlagZ() == 0) {   /* Don't jump */
        Register.PC += 3;
      } else {  /* Jump */
        cpucycles = 6; /* correct the timing when an action is taken */
        PushToStack16(Register.PC + 3);   /* First save the current value of the program counter (+3 bytes) */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      }
      break;
    case 0xCD:   /* CALL (calls a subroutine) */
      PushToStack16(Register.PC + 3);    /* First save the current value of the program counter (+3 bytes) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("CALL nn"); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2))) */ /* DEBUG */
      Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      break;
    case 0xCE:   /* ADC A,n */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("ADC A,n"); /* & HEX(UbyteBuff1,2)) */   /* DEBUG */
      AdcA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xCF:   /* RST 08h */
      PrintDebug("RST 08h");   /* DEBUG */
      PushToStack16(Register.PC + 1);           /* Save current address (+1) */
      Register.PC = 0x08;  /* Jump to 08h */
      break;
    case 0xD0:   /* RET NC */   /* RET if C = 0 */
      PrintDebug("RET NC");
      if (GetFlagC() == 0) {
        PopPCfromStack();
        cpucycles = 5; /* correct the timing when an action is taken */
      } else {
        Register.PC++;
      }
      break;
    case 0xD1:   /* POP DE (Write the value in stack into DE) */
      PrintDebug("POP DE");  /* DEBUG */
      PopFromStack(&Register.D, &Register.E);
      Register.PC++;
      break;
    case 0xD2:   /* JP NC,nn (jump if C=0) */
      PrintDebug("JP NC,nn");   /* DEBUG */
      if (GetFlagC() == 0) {  /* Jump */
        cpucycles = 4; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      } else {   /* Don't jump */
        Register.PC += 3;
      }
      break;
    case 0xD4:   /* CALL NC,nn (jump if Z=0) */
      PrintDebug("CALL NC,nn");   /* DEBUG */
      if (GetFlagC() == 0) {  /* Jump */
        cpucycles = 6; /* correct the timing when an action is taken */
        PushToStack16(Register.PC + 3);    /* First save the current value of the program counter (+3 bytes) */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      } else {   /* Don't jump */
        Register.PC += 3;
      }
      break;
    case 0xD5:   /* PUSH DE */
      PrintDebug("PUSH DE");  /* DEBUG */
      PushToStack(Register.D, Register.E);
      Register.PC++;
      break;
    case 0xD6:  /* SUB n from A */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("SUB n"); /* & HEX(UbyteBuff1)) */  /* DEBUG */
      SubValFromReg8(&Register.A, &UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xD7:   /* RST 10h */
      PrintDebug("RST 10h");   /* DEBUG */
      PushToStack16(Register.PC + 1);     /* Save current address (+1) */
      Register.PC = 0x10;  /* Jump to 10h */
      break;
    case 0xD8:   /* RET C */   /* RET if C = 1 */
      PrintDebug("RET C");
      if (GetFlagC() == 0) {
        Register.PC++;
      } else {
        cpucycles = 5; /* correct the timing when an action is taken */
        PopPCfromStack();
      }
      break;
    case 0xD9:   /* RETI  (RET + EI) */
      PrintDebug("RETI");   /* DEBUG */
      PopPCfromStack();           /* RET (return from a call or int) */
      InterruptsState = 1;        /* EI (enable interrupts) */
      break;
    case 0xDA:   /* JP C,nn (jump if C=1) */
      PrintDebug("JP C,nn");   /* DEBUG */
      if (GetFlagC() == 0) {  /* Don't jump */
        Register.PC += 3;
      } else {   /* Jump */
        cpucycles = 4; /* correct the timing when an action is taken */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      }
      break;
    case 0xDC:   /* CALL C,nn (jump if Z=1) */
      PrintDebug("CALL C,nn");   /* DEBUG */
      if (GetFlagC() == 1) {  /* Jump */
        cpucycles = 6; /* correct the timing when an action is taken */
        PushToStack16(Register.PC + 3);    /* First save the current value of the program counter (+3 bytes) */
        UbyteBuff1 = MemoryRead(Register.PC + 1);
        UbyteBuff2 = MemoryRead(Register.PC + 2);
        Register.PC = DwordVal(UbyteBuff1, UbyteBuff2);
      } else {   /* Don't jump */
        Register.PC += 3;
      }
      break;
    case 0xDE:  /* SBC A,n */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("SBC A,n"); /* & UbyteBuff1) */  /* DEBUG */
      SbcA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xDF:   /* RST 18h */
      PrintDebug("RST 18h");   /* DEBUG */
      PushToStack16(Register.PC + 1);     /* Save current address (+1) */
      Register.PC = 0x18;  /* Jump to 18h */
      break;
    case 0xE0:   /* LD ($FF00+n),A (put A into addr $FF00+n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("LD ($FF00+n),A"); /* & HEX(UbyteBuff1, 2) & "),A"); */  /* DEBUG */
      MemoryWrite(0xFF00 + UbyteBuff1, Register.A);
      Register.PC += 2;
      break;
    case 0xE1:   /* POP HL (Write the value in stack into HL) */
      PrintDebug("POP HL");  /* DEBUG */
      PopFromStack(&Register.H, &Register.L);
      Register.PC++;
      break;
    case 0xE2:   /* LD (FF00h+C),A (put A into addr $FF00+C) */
      PrintDebug("LD (FF00h+C),A"); /* & HEX(0xFF00 + Register.C) & "),A"); */  /* DEBUG */
      MemoryWrite(0xFF00 + Register.C, Register.A);
      Register.PC++;
      break;
    case 0xE5:   /* PUSH HL */
      PrintDebug("PUSH HL");  /* DEBUG */
      PushToStack(Register.H, Register.L);
      Register.PC++;
      break;
    case 0xE6:   /* A AND n */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("A AND n (n=0x%02X)", UbyteBuff1); /* & UbyteBuff1) */
      AndA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xE7:   /* RST 20h */
      PrintDebug("RST 20h");   /* DEBUG */
      PushToStack16(Register.PC + 1);    /* Save current address (+1) */
      Register.PC = 0x20;  /* Jump to 20h */
      break;
    case 0xE8:   /* ADD SP,n */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("ADD SP,n"); /* & UbyteToByte(UbyteBuff1)) */ /* DEBUG */
      AddToSP(UbyteToByte(UbyteBuff1));
      Register.PC += 2;
      break;
    case 0xE9:   /* JP (HL) / Jumps to address contained in HL (same as "LD PC,HL") */
      PrintDebug("JP (HL)");  /* DEBUG */
      Register.PC = ReadRegHL();
      break;
    case 0xEA:   /* LD (nn),A   Load value of A into address nn */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD (nn),A"); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2)) & "),A"); */  /* DEBUG */
      MemoryWrite(DwordVal(UbyteBuff1, UbyteBuff2), Register.A);
      Register.PC += 3;
      break;
    case 0xEE:   /* XOR n  (A = A XOR n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("XOR n"); /* & HEX(UbyteBuff1, 2)) */ /* DEBUG */
      XorA(UbyteBuff1); /* Xor with A, result in A */
      Register.PC += 2;
      break;
    case 0xEF:   /* RST 28h */
      PrintDebug("RST 28h");   /* DEBUG */
      PushToStack16(Register.PC + 1);    /* Save current address (+1) */
      Register.PC = 0x28;  /* Jump to 28h */
      break;
    case 0xF0:   /* LD A,($FF00+n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("LD A,($FF00+n)"); /* & HEX(UbyteBuff1, 2) & ")"); */  /* DEBUG */
      Register.A = MemoryRead(0xFF00 | UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xF1:   /* POP AF (Write the value in stack into AF) */
      PrintDebug("POP AF");  /* DEBUG */
      PopFromStack(&Register.A, &Register.F);
      Register.F &= bx11110000; /* Make sure that lower 4 bits of the F register are always reset */
      Register.PC++;
      break;
    case 0xF2:   /* LD A,(FF00h+C)  (put value at address FF00+C into A) */
      PrintDebug("LD A,(FF00+C)");    /* DEBUG */
      Register.A = MemoryRead(0xFF00 | Register.C);
      Register.PC++;
      break;
    case 0xF3:   /* DI (Disables Interrupts) */
      PrintDebug("DI - Disables interrupts");  /* DEBUG */
      InterruptsState = 0;   /* Sets IME (Interrupts Master Enable) to 0 */
      Register.PC++;
      break;
    case 0xF5:   /* PUSH AF */
      PrintDebug("PUSH AF");  /* DEBUG */
      PushToStack(Register.A, Register.F);
      Register.PC++;
      break;
    case 0xF6:   /* OR n (A = A OR n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("OR n"); /* & HEX(UbyteBuff1, 2)) */  /* DEBUG */
      OrA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xF7:   /* RST 30h */
      PrintDebug("RST 30h");   /* DEBUG */
      PushToStack16(Register.PC + 1);     /* Save current address (+1) */
      Register.PC = 0x30;  /* Jump to 30h */
      break;
    case 0xF8:   /* LD HL,SP+n */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      ByteBuff = UbyteToByte(UbyteBuff1);
      PrintDebug("LD HL,SP+n"); /* & ByteBuff) */
      UintBuff = (Register.SP + ByteBuff);
      UintBuff &= 0xFFFF;  /* Make sure to be in the range 0..65535 - not needed if the variable is an unsigned 16bit wide integer */
      if ((UintBuff & 0xFF) < (Register.SP & 0xFF)) {
        SetFlagC();
      } else {
        ResetFlagC();
      }
      if ((UintBuff & 0xF) < (Register.SP & 0xF)) {
        SetFlagH();
      } else {
        ResetFlagH();
      }
      ResetFlagZ();
      ResetFlagN();
      WriteRegHL((UintBuff >> 8), (UintBuff & bx11111111));
      Register.PC += 2;
      break;
    case 0xF9:   /* LD SP,HL  (put HL into SP) */
      PrintDebug("LD SP,HL");
      Register.SP = ReadRegHL();
      Register.PC++;
      break;
    case 0xFA:   /* LD A,nn  (put value from address nn to A) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      UbyteBuff2 = MemoryRead(Register.PC + 2);
      PrintDebug("LD A,nn"); /* & HEX(DwordVal(UbyteBuff1, UbyteBuff2), 4)); */
      Register.A = MemoryRead(DwordVal(UbyteBuff1, UbyteBuff2));
      Register.PC += 3;
      break;
    case 0xFB:   /* EI (Enables Interrupts) */
      PrintDebug("EI - Enables interrupts");  /* DEBUG */
      InterruptsState = 1;
      Register.PC++;
      break;
    case 0xFE:   /* CP n (compare A with n) */
      UbyteBuff1 = MemoryRead(Register.PC + 1);
      PrintDebug("CP A,n"); /* & HEX(UbyteBuff1, 2)); */ /* DEBUG */
      CmpA(UbyteBuff1);
      Register.PC += 2;
      break;
    case 0xFF:   /* RST 38h */
      PrintDebug("RST 38h");   /* DEBUG */
      PushToStack16(Register.PC + 1);     /* Save current address (+1) */
      Register.PC = 0x38;  /* Jump to 38h */
      break;
    default:
      {
      char msg[64];
      sprintf(msg, "UNKNOWN OPCODE: %02X", CpuInstruction);
      SetUserMsg(msg);
      QuitEmulator = -1;
      break;
      }
  }
  PrintDebug("\n");
  #ifdef DEBUGMODE
    /* DIM AS STRING LastKey */
    if (DebugCpuPause == 1) {
      DebugOnScreen();
      DebugShowCpuRegisters();
      PressAnyKey();
    } else {
      /* LastKey = INKEY */
    }
    /* if LastKey = " " THEN DebugCpuPause = 1
    if LastKey = "p" THEN
      DebugOnscreen()
      DebugShowCpuRegisters()
      DO
        SLEEP 10,1
        LastKey = INKEY
      LOOP UNTIL (LEN(LastKey) > 0)
    } */
  #endif
  /*IF UsedCycles = 0 THEN PrintMsg("ERR: CYCLES VALUE IS 0!" & CHR(10) & "LAST INSTRUCTION: " & HEX(CpuInstruction) & "H") : SLEEP : END */
  /*printf("TotalCycles = %d\n", TotalCycles); */

  /* return the amount of clock cycles this instruction took */
  cpucycles <<= 2;
  return(cpucycles); /* the cycles tables contain machine cycles, while I rely on clock cycles (x4) */
}
