/*
    Serial link emulation
    This file is part of the zBoy project
    Copyright (C) 2010-2019 Mateusz Viste


    FF02 is the serial control address
      Bit 7 - if set, the game wants to interact with serial link
      Bit 0 - Clock source selection (0 = external ; 1 = internal)

    FF01 is the serial data buffer (for in & out operations)

 Timing: When trigerring a serial transfer with an internal clock, the serial INT will occur 976 us later.
         A GameBoy is running at 4194304 CPU cycles/s
         1 CPU cycle = 0.0002384185791015625 ms = 0.2384185791015625 us
         So a serial INT should occur after 4093.64 CPU (clock) cycles.

         The serial interface transmits data at a speed of 8192 Hz (meaning one bit every 512 clock cycles, so 1 byte every 4096 clock cycles)
*/

#include <string.h>  /* memcmp() */

#include "net.h"

static int msgseq = 0, msgseqexpected = -1; /* a sequence id of the message sent and expected */


/* the purpose of this function is to autofind an available player wishing to play the exact same game on the local network. players locate each other using broadcast signaling. specifically, I open two broadcast sockets: one to send packets from a local IP, and another one to receive packets (bound to the broadcast IP). this is required, at least on Linux, because a socket won't receive broadcast datagrams unless it has been bound to the broadcast address. */
int netfindautopeer(char *bindaddr, char *remoteplayer, struct zboyparamstype *zboyparams, uint32_t romcrc) {
  char unsigned buff[64];
  int gotbytes, i;
  uint32_t myid;
  struct net_sock_t *sock_recv, *sock_send;
  /* pick a random id, will be used to avoid following my own broadcasts */
  myid = rand();
  /* open my two sockets: one for sending broadcasts, other for receiving.
   * I use a different port than the one for actual playing, as to avoid any
   * risk of polluting already ongoing games on the same LAN */
  sock_recv = net_open("255.255.255.255", "255.255.255.255", 8765);
  if (sock_recv == NULL) return(-1);
  sock_send = net_open(bindaddr, "255.255.255.255", 8765);
  if (sock_send == NULL) {
    net_close(sock_recv);
    return(-1);
  }
  /* wait some time for a peer to appear */
  for (i = 200; i != 0; i--) {
    sprintf((char *)buff, "LOOKING FOR A PLAYER ON THE LAN... %d.%d", i / 10, i % 10);
    PrintMsg((char *)buff, 0);
    RefreshScreen(0, 159, 0, 143, zboyparams);
    /* payload's format is "ZBOY HELLO" + \0 + ROM's CRC32 + MyID */
    memcpy(buff, "ZBOY HELLO", 11);
    buff[11] = (romcrc >> 24) & 0xff;
    buff[12] = (romcrc >> 16) & 0xff;
    buff[13] = (romcrc >> 8) & 0xff;
    buff[14] = romcrc & 0xff;
    buff[15] = (myid >> 24) & 0xff;
    buff[16] = (myid >> 16) & 0xff;
    buff[17] = (myid >> 8) & 0xff;
    buff[18] = myid & 0xff;
    /* send packets twice, just in case some are dropped */
    drv_delay(50);
    net_send(sock_send, buff, 19);
    drv_delay(50);
    net_send(sock_send, buff, 19);
    while (net_recvpeek(sock_recv, buff, sizeof(buff)) > 0) {
      gotbytes = net_recv(sock_recv, buff, sizeof(buff));
      if (gotbytes == 19) {
        if (memcmp(buff, "ZBOY HELLO", 11) == 0) {
          uint32_t peercrc = 0, peerid = 0;
          int x;
          for (x = 0; x < 4; x += 1) {
            peercrc <<= 8;
            peercrc |= buff[11 + x];
            peerid <<= 8;
            peerid |= buff[15 + x];
          }
          if ((peercrc == romcrc) && (peerid != myid)) {
            net_getpeeraddr(sock_recv, remoteplayer);
            net_close(sock_send);
            net_close(sock_recv);
            return(0);
          }
        }
      }
    }
    /* poll keys just in case the player wants to abort */
    if (drv_event_gettype(drv_keypoll()) == DRV_INPUT_QUIT) {
      QuitEmulator = 1;
      break;
    }
  }
  /* close sockets and report timeout */
  net_close(sock_send);
  net_close(sock_recv);
  return(-2);
}


/* synchronize both peers so they run with the exact same serial clocks */
int netserialsynch(struct net_sock_t *sock, struct zboyparamstype *zboyparams) {
  int timeoutcnt;
  unsigned char databuff[2];
  char msg[64];
  /* set msgseq tracking to initial values */
  msgseq = 0;
  msgseqexpected = -1;
  /* phase 1 - wait for the peer to become alive, but no longer than 20s */
  timeoutcnt = 200;
  for (;;) {
    sprintf(msg, "WAITING FOR NETPLAY PEER... %d.%d", timeoutcnt / 10, timeoutcnt % 10);
    PrintMsg(msg, 0);
    RefreshScreen(0, 159, 0, 143, zboyparams);
    databuff[0] = 0xff;
    databuff[1] = 1;
    net_send(sock, databuff, 2);
    if (net_recvpeek(sock, databuff, 2) == 2) {
      net_recv(sock, databuff, 2);
      if ((databuff[0] == 0xff) && (databuff[1] == 1)) {
        break;
      }
    }
    if (--timeoutcnt == 0) return(-1); /* timeout */
    drv_delay(100); /* 100 ms */
    /* poll keys just in case the player wants to abort */
    if (drv_event_gettype(drv_keypoll()) == DRV_INPUT_QUIT) {
      QuitEmulator = 1;
      return(-1);
    }
  }
  /* phase 2 - synchronize both peers with a single message */
  databuff[0] = 0xff;
  databuff[1] = 2;
  net_send(sock, databuff, 2);
  timeoutcnt = 10; /* wait for synch max 500ms (10*50ms) */
  for (;;) {
    if (net_recvpeek(sock, databuff, 2) == 2) {
      net_recv(sock, databuff, 2);
      if ((databuff[0] == 0xff) && (databuff[1] == 2)) break;
    }
    if (--timeoutcnt == 0) {
      PrintMsg("SYNCH ERROR", 0);
      RefreshScreen(0, 159, 0, 143, zboyparams);
      drv_delay(2000);
      return(-1);
    }
    drv_delay(50); /* 50 ms */
  }
  return(0);
}


struct net_sock_t *netlanstart(struct net_sock_t *oldsock, char *bindaddr, struct zboyparamstype *zboyparams, uint32_t romcrc) {
  char peer[64];
  struct net_sock_t *newsock;
  /* if NetPlay already ON, close socket first */
  if (NetPlay != 0) {
    net_close(oldsock);
    NetPlay = 0;
  }
  if (netfindautopeer(bindaddr, peer, zboyparams, romcrc) != 0) {
    SetUserMsg("NO LAN PLAYER FOUND");
    return(NULL);
  }
  /* open socket */
  newsock = net_open(NetPlayBindaddr, peer, 8764);
  if (newsock == NULL) {
    SetUserMsg("NETWORK ERROR");
    return(NULL);
  }
  /* synch */
  if (netserialsynch(newsock, zboyparams) != 0) {
    SetUserMsg("SYNCH FAILED");
    net_close(newsock);
    return(NULL);
  }
  /* if we end up here, then it's all good */
  NetPlay = 1;
  return(newsock);
}


/* No net player - just fakes a Serial GB interface which is connected nowhere */
inline void CheckSerialLink_dummy(void) {
  if ((IoRegisters[0xFF02] & bx10000000) != 0) {    /* The game tries to send some stuff out */
    if ((IoRegisters[0xFF02] & 1) != 0) {  /* If external clock is selected, ignore, otherwise let's see */
      /*printf("%08d cycles: %c\n", TotalCycles, IoRegisters[0xFF01]); */ /* for DEBUG purpose, you can print out to console */
      #ifdef DEBUGSERIAL
      printf("%c", IoRegisters[0xFF01]);
      #endif
      IoRegisters[0xFF01] = 0xff;  /* 0xFF means "no peer connected" */
      IoRegisters[0xFF02] &= bx01111111;   /* Reset the bit 7 of FF02 (see page 31 of GameBoy CPU manual) */
      INT(INT_SERIAL);               /* When serial handling done, request serial interrupt (INT 58), via bit 3 of FF0F */
    }
  }
}


inline void CheckSerialLink(int cycles, struct net_sock_t *sock) {
  static int pendingbyte = -1;
  static int clockcounter = 0;
  static unsigned char databuff[4];
  static unsigned char databuffout[4];
  static unsigned char databuffoutprev[4] = {0xff,0xff,0xff,0xff};
  static unsigned char databuffoutprevprev[4] = {0xff,0xff,0xff,0xff};
  static int clockpulse;
  static unsigned long curtime, lastretry;

  if (pendingbyte >= 0) {
    IoRegisters[0xFF01] = pendingbyte;
    IoRegisters[0xFF02] &= bx01111111;   /* Reset the bit 7 of FF02 (see page 31 of GameBoy CPU manual) */
    INT(INT_SERIAL);               /* When serial handling done, request serial interrupt (INT 58), via bit 3 of FF0F */
    pendingbyte = -1;
  }

  clockcounter += cycles;           /* the serial interface runs at 8192 Hz */
  if (clockcounter < 1024) return;  /* bits per sec, that is 1024 bytes/s   */
  clockcounter -= 1024;             /* (1 byte every 4096 clock cycles).    */
                                    /* I am driving the serial link 4x too  */
                                    /* fast here, but for some reasons I do */
                                    /* not understand yet, this provides    */
                                    /* the best compatibilty across games.  */

  msgseq++;        /* increment msgseq and make sure this counter fits */
  msgseq &= 0xff;  /* in 8 bits */

  clockpulse = 0;
  /* if I provide clock signal, AND I want to send something, then we have clock pulse */
  if ((IoRegisters[0xFF02] & bx10000001) == bx10000001) clockpulse = 1;

  /* if I have to send stuff, send it now */
  databuffout[0] = clockpulse;
  databuffout[1] = IoRegisters[0xFF01];
  databuffout[2] = msgseq;
  if (net_send(sock, databuffout, 3) != 3) printf("send() problem on line #%d\n", __LINE__);

  /* do not expect any answer at first iteration */
  if (msgseqexpected < 0) {
    msgseqexpected = 0;
    memcpy(databuffoutprev, databuffout, sizeof(databuffout));
    return;
  } else {
    msgseqexpected++;
    msgseqexpected &= 0xff;
  }

  /* wait for data */
  curtime = drv_getticks();
  lastretry = curtime;
  for (;;) {
    unsigned long now = drv_getticks();
    if (net_recvpeek(sock, databuff, 3) == 3) {
      net_recv(sock, databuff, 3);                    /* consume the datagram from socket */
      if (databuff[2] == msgseqexpected) break; /* have we received the correct seq? */
    }
    /* check for timeout (and detect timer wraparound) */
    if (now < curtime) curtime = now;
    if (now - curtime > 2000) { /* if more than 2s elapsed, it's a timeout */
      NetPlay = 0;
      SetUserMsg("LINK BROKEN");
      net_close(sock);
      return;
    }
    /* retransmission - retry once every 10ms */
    if (now - lastretry > 10) {
      lastretry = now;
      if (net_send(sock, databuffoutprevprev, 3) != 3) printf("send() problem on line #%d\n", __LINE__);
      if (net_send(sock, databuffoutprev, 3) != 3) printf("send() problem on line #%d\n", __LINE__);
      if (net_send(sock, databuffout, 3) != 3) printf("send() problem on line #%d\n", __LINE__);
    }
  }
  /* remember last data packet so I can retransmit it later if needed */
  memcpy(databuffoutprevprev, databuffoutprev, sizeof(databuffout));
  memcpy(databuffoutprev, databuffout, sizeof(databuffout));
  /* if relying on an external clock, check the presence of a clock pulse */
  if ((IoRegisters[0xFF02] & 1) == 0) clockpulse = databuff[0];

  /* the transfer 'counts' only if a clock pulse was present */
  if (clockpulse != 0) pendingbyte = databuff[1];

  if (pendingbyte >= 0) {
    IoRegisters[0xFF01] = pendingbyte;
    IoRegisters[0xFF02] &= bx01111111;   /* Reset the bit 7 of FF02 (see page 31 of GameBoy CPU manual) */
    INT(INT_SERIAL);               /* When serial handling done, request serial interrupt (INT 58), via bit 3 of FF0F */
    pendingbyte = -1;
  }
}
