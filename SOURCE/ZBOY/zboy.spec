# spec file kindly contributed by Andrea Musuruane

Name:           zboy
Version:        0.70
Release:        1%{?dist}
Summary:        A GameBoy Classic emulator

License:        GPLv3
URL:            http://zboy.sourceforge.net/
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  SDL2-devel >= 2.0.4

%description
zBoy is a multi-platform, open-source GameBoy Classic ("DMG") emulator that
provides a load/save feature, can perform PCX screenshots and emulates an
internal battery for ROMs that were designed to use one. It is also one of
the very few emulators that make it possible to play games in 2-players mode
over LAN, emulating a link between two consoles.

zBoy supports many additional features, too, like intelligent saving of
hi-scores for some games that aren't able to save their hi-scores table by
themselves because of the lack of a battery-backed RAM on the cartridge, and
improving screen's resolution output using graphic algorithms like Scale2x,
Scale3x, Eagle...


%prep
%setup -q


%build
#%set_build_flags
%make_build -f Makefile.linux


%install
mkdir -p %{buildroot}%{_bindir}
install -p -m 0755 %{name} %{buildroot}%{_bindir}


%files
%{_bindir}/%{name}
%license license.txt
%doc colorize.txt history.txt todo.txt zboy.txt

%changelog
