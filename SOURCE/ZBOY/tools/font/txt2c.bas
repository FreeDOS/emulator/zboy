REM
REM   This program converts a text font file into a C array
REM   Copyright (C) Mateusz Viste 2010, 2011
REM

DIM AS STRING TempString
DIM AS UBYTE x, y, CharId = 31, CharVal

OPEN "font.txt" FOR INPUT AS #1
OPEN "font.c" FOR OUTPUT AS #2

PRINT #2, "// This file is part of the zBoy project"
PRINT #2, "// Copyright (C) Mateusz Viste 2010, 2011"
PRINT #2,
PRINT #2, "uint8_t FontData[95][8] = {{0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00},"

FOR x = 1 TO 31
  PRINT #2, "                           {0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00},"
NEXT x

DO
  CharId += 1
  PRINT #2, "                           {";
  FOR y = 0 TO 7
    CharVal = 0
    LINE INPUT #1, TempString
    FOR x = 0 TO 7
      IF MID(TempString, x + 1, 1) = "#" THEN CharVal += (2^x)
    NEXT x
    'PRINT #2, "FontData(" & CharId & ", " & y & ") = " & CharVal
    IF (y > 0) THEN
        PRINT #2, ",0x" & HEX(CharVal,2);
      ELSE
        PRINT #2, "0x" & HEX(CharVal,2);
    END IF
  NEXT y
  IF (CharId < 94) THEN
      PRINT #2, "},"
    ELSE
      PRINT #2, "}";
  END IF
LOOP UNTIL (CharId = 94)

PRINT #2, "}"

CLOSE #1
CLOSE #2

PRINT "Done. " & CharId & " glyphs have been processed."
