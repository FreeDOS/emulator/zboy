/*
 * zBoy sound emulation
 * Copyright (C) Mateusz Viste 2015-2019
 *
 * "cycles" represent CLOCK cycles (not machine cycles).
 * The GB clock runs at 4194304 Hz, hence 1 cycle = 0.0002384185791015625 ms
 *
 * Host sound format is 44.1 kHz, 8-bit unsigned, stereo.
 *
 * The GameBoy has 4 sound channels, and all of them are mixed through
 * a simplistic mixer that add signals together. To make mixing that easy,
 * I need to use signed samples internally. It means each channel's amplitude
 * must be within -31..+31 to avoid clipping. Just before submitting the
 * samples to the audio driver, they are converted to unsigned 8-bit values.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "drv.h"

/* uncomment to copy all output to file (raw 8-bit, signed, 8 kHz, stereo) */
/* #define DBGSOUNDFILE "out.raw" */

/* use the correct sample schedule delay according to host sampling rate
 * (4294967296/freq) - host sampling rate is inited in drv_xxxxx.c */
/* #define SAMPLESCHEDULER 89478  */ /* 48000 Hz */
#define SAMPLESCHEDULER 97391        /* 44100 Hz */
/* #define SAMPLESCHEDULER 194783 */ /* 22050 Hz */
/* #define SAMPLESCHEDULER 536870 */ /* 8000 Hz */

/* comment out to disable some channels */
#define CHAN1  /* square wave #1 */
#define CHAN2  /* square wave #2 */
#define CHAN3  /* custom wave    */
#define CHAN4  /* noise          */


/* define sound registers */

/* square 1 */
#define NR10 0xFF10 /* -PPP NSSS sweep period, negate, shift */
#define NR11 0xFF11 /* DDLL LLLL duty, length load (64-L) */
#define NR12 0xFF12 /* VVVV APPP starting vol, envelope add mode, period */
#define NR13 0xFF13 /* FFFF FFFF freq LSB */
#define NR14 0xFF14 /* TL-- -FFF trigger, length enable, freq MSB */

/* square 2 */
#define NR21 0xFF16 /* DDLL LLLL duty, length load (64-L) */
#define NR22 0xFF17 /* VVVV APPP starting vol, envelope add mode, period */
#define NR23 0xFF18 /* FFFF FFFF freq LSB */
#define NR24 0xFF19 /* TL-- -FFF trigger, length enable, freq MSB */

/* wave */
#define NR30 0xFF1A /* E--- ---- DAC power */
#define NR31 0xFF1B /* LLLL LLLL length load (256-L) */
#define NR32 0xFF1C /* -VV- ---- volume (0=0%, 1=100%, 2=50% 3=25% */
#define NR33 0xFF1D /* FFFF FFFF freq LSB */
#define NR34 0xFF1E /* TL-- -FFF trigger, length enable, freq MSB */

/* noise */
#define NR41 0xFF20 /* --LL LLLL length load (64-L) */
#define NR42 0xFF21 /* VVVV APPP starting vol, envelope add mode, period */
#define NR43 0xFF22 /* SSSS WDDD clock shift, width mode of LFSR, divisor */
#define NR44 0xFF23 /* TL-- ---- trigger, length enable */

/* control */
#define NR50 0xFF24 /* ALLL BRRR Vin L ena., lft vol, Vin R ena., rght vol */
#define NR51 0xFF25 /* NW21 NW21 left enable, right enable */
#define NR52 0xFF26 /* P--- NW21 pwr control/status, channel len statuses */


static struct {
  short dutyselector; /* 0-3 */
  short lengthenable;
  short length;
  short envelopecounter; /* how many times should I apply envelope op */
  short curvol;    /* channel's volume (0-15) */
  short freq;      /* sound 'ticks' every (2048-freq)*4 CPU cycles */
  short freqtimer; /* counts from (2048-freq)*4 to zero and inc dutystep */
  short dutystep; /* 0-7, what step of the duty cycle I am at the moment */
  short enabled; /* disabled when length reaches 0 */
  short sweepcounter; /* counts from NR10 shifts downards */
  short sweeptimer; /* counts from sweep time to 0 to generate sweep action */
  short sweepshadowfreq;
} sq1; /* square wave 1 channel */

static struct {
  short dutyselector; /* 0-3 */
  short lengthenable;
  short length;
  short envelopecounter; /* how many times should I apply envelope op */
  short curvol;    /* channel's volume (0-15) */
  short freq;      /* sound 'ticks' every (2048-freq)*4 CPU cycles */
  short freqtimer; /* counts from (2048-freq)*4 to zero and inc dutystep */
  short dutystep; /* 0-7, what step of the duty cycle I am at the moment */
  short enabled; /* disabled when length reaches 0 */
} sq2; /* square wave 2 channel */

static struct {
  short lengthenable;
  short length;
  short freq;
  short freqtimer;
  short curvolshift; /* volume as number of right shifts */
  short samplepos; /* 0..31, pointer to the current sample in wave table */
  short enabled; /* disabled when length reaches 0 */
} wave; /* wave channel */

static struct {
  short lengthenable;
  short length;
  short envelopecounter; /* how many times should I apply envelope op */
  short curvol;    /* channel's volume (0-15) */
  short freqtimer;
  unsigned short step; /* what step of whitenoise[] I am at now */
  short enabled; /* disabled when length reaches 0 */
} noise; /* noise channel */

static struct {
  short counter;  /* 512 Hz counter that generates clock pulses */
  short clock512hz;
  short clock256hz;
  short clock128hz;
  short clock64hz;
} frameseq; /* frame sequencer */

static short sound_sampleschedfix = 0; /* correction to sync audio with cpu clocks */


#define CHANOFF_SQ1   { sq1.enabled=0;   IoRegisters[NR52] &= bx11111110; }
#define CHANOFF_SQ2   { sq2.enabled=0;   IoRegisters[NR52] &= bx11111101; }
#define CHANOFF_WAVE  { wave.enabled=0;  IoRegisters[NR52] &= bx11111011; }
#define CHANOFF_NOISE { noise.enabled=0; IoRegisters[NR52] &= bx11110111; }

#define CHANON_SQ1   { sq1.enabled=1;   IoRegisters[NR52] |= bx00000001; }
#define CHANON_SQ2   { sq2.enabled=1;   IoRegisters[NR52] |= bx00000010; }
#define CHANON_WAVE  { wave.enabled=1;  IoRegisters[NR52] |= bx00000100; }
#define CHANON_NOISE { noise.enabled=1; IoRegisters[NR52] |= bx00001000; }


void sound_writemem(int addr, uint8_t val) {
  /* do not accept any writes to sound registers, unless of course it is a
   * write towards NR52 (DAC power control) */
  if (((IoRegisters[NR52] & 128) == 0) && (addr != NR52)) return;
  /* DEBUG section - sound registers tracking */
  /* printf("WRITE 0x%02X @ 0x%04X\n", val, addr); */

  /********* SQUARE 1 *********/
  if (addr == NR10) { /* sq 1 -PPP NSSS sweep period, add(0)/sub(1), shifts (R/W) */
    IoRegisters[NR10] = 0x80 | val;
  } else if (addr == NR11) { /* square 1 duty + length load (R/W) */
    sq1.dutyselector = (val >> 6);
    sq1.length = 64 - (val & 63);
    IoRegisters[NR11] = val | 0x3F;
  } else if (addr == NR12) { /* sq1 VVVV APPP start vol, env dir, period */
                             /* also 5 MSB control DAC (off if all zero) */
    if ((val & 0xF8) == 0) CHANOFF_SQ1; /* DAC OFF */
    IoRegisters[NR12] = val;
  } else if (addr == NR13) { /* sq1 freq 8 LSB (Write-Only) */
    sq1.freq &= 0x700;
    sq1.freq |= val;
  } else if (addr == NR14) { /* sq1 TL-- -FFF trig, length enable, freq MSB */
    short msbfreq = val;
    IoRegisters[NR14] = val | bx10111111;
    msbfreq &= 7;
    msbfreq <<= 8;
    sq1.freq &= 0x0ff;
    sq1.freq |= msbfreq;
    sq1.lengthenable = (val & 64);
    if (val & 128) { /* trigger init */
      if (IoRegisters[NR12] & 0xF8) {
        CHANON_SQ1; /* if DAC is off, channel is disabled too */
      } else {
        CHANOFF_SQ1;
      }
      if (sq1.length == 0) sq1.length = 64;
      /* frequency timer is reloaded */
      sq1.freqtimer = 4 * (2048 - sq1.freq);
      /* volume envelope timer is reloaded with period */
      sq1.envelopecounter = (IoRegisters[NR12] & 7);
      /* channel volume is reloaded with NRx2 */
      sq1.curvol = (IoRegisters[NR12] >> 4);
      /* sweep operations */
      sq1.sweepshadowfreq = sq1.freq;
      sq1.sweeptimer = (IoRegisters[NR10] >> 4) & 7;
      sq1.sweepcounter = IoRegisters[NR10] & 7;
    }
  /********* SQUARE 2 *********/
  } else if (addr == NR21) { /* square 2 duty + length load (R/W) */
    sq2.dutyselector = (val >> 6);
    sq2.length = 64 - (val & 63);
    IoRegisters[NR21] = val | 0x3F;
  } else if (addr == NR22) { /* sq2 VVVV APPP start vol, env dir, period */
                             /* also 5 MSB control DAC (off if all zero) */
    if ((val & 0xF8) == 0) CHANOFF_SQ2; /* DAC OFF */
    IoRegisters[NR22] = val;
  } else if (addr == NR23) { /* sq2 freq 8 LSB (Write-Only) */
    sq2.freq &= 0x700;
    sq2.freq |= val;
  } else if (addr == NR24) { /* sq2 TL-- -FFF trig, length enable, freq MSB (R/W) */
    short msbfreq = val;
    IoRegisters[NR24] = val | bx10111111;
    msbfreq &= 7;
    msbfreq <<= 8;
    sq2.freq &= 0x0ff;
    sq2.freq |= msbfreq;
    sq2.lengthenable = (val & 64);
    if (val & 128) { /* trigger init */
      if (IoRegisters[NR22] & 0xF8) {
        CHANON_SQ2; /* if DAC is off, channel is disabled too */
      } else {
        CHANOFF_SQ2;
      }
      if (sq2.length == 0) sq2.length = 64;
      /* frequency timer is reloaded */
      sq2.freqtimer = 4 * (2048 - sq2.freq);
      /* volume envelope timer is reloaded with period */
      sq2.envelopecounter = (IoRegisters[NR22] & 7);
      /* channel volume is reloaded with NRx2 */
      sq2.curvol = (IoRegisters[NR22] >> 4);
    }
  /********* WAVE *********/
  } else if (addr == NR30) { /* DAC power */
    IoRegisters[NR30] = val | bx01111111;
    if ((val >> 7) == 0) CHANOFF_WAVE;     /* disable channel if DAC goes
                                              off (but do NOT re-enable the
                                              channel when DAC goes ON) */
  } else if (addr == NR31) { /* wave length load (8 bits) */
    wave.length = 256 - val;
  } else if (addr == NR32) { /* wave volume (-VV- ----) */
    short volshift[] = {4, 0, 1, 2};
    wave.curvolshift = volshift[(val >> 5) & 3];
    IoRegisters[NR32] = val | bx10011111;
  } else if (addr == NR33) { /* freq lower 8 bits (Write-Only) */
    wave.freq &= 0x700;
    wave.freq |= val;
  } else if (addr == NR34) { /* TL-- -FFF  trigger, length enable, freq MSB (R/W) */
    IoRegisters[NR34] = val | bx10111111;
    wave.lengthenable = (val >> 6) & 1;
    wave.freq &= 0xff;
    wave.freq |= (val & 7) << 8;
    if (val & 127) { /* trigger */
      if (IoRegisters[NR30] >> 7) CHANON_WAVE; /* enable only if DAC is ON */
      if (wave.length == 0) wave.length = 256;
      wave.freqtimer = 2 * (2048 - wave.freq);
      wave.samplepos = 0;
    }
  /********* NOISE *********/
  } else if (addr == NR41) {
    noise.length = 64 - (val & 63);
  } else if (addr == NR42) { /* noise VVVV APPP start vol, env dir, period */
                             /* also 5 MSB control DAC (off if all zero) */
    if ((val & 0xF8) == 0) CHANOFF_NOISE; /* DAC OFF */
    IoRegisters[NR42] = val;
  } else if (addr == NR43) { /* noise clock shift, lfsr width mode, divisor code (RW) */
    IoRegisters[NR43] = val;
  } else if (addr == NR44) { /* noise TL-- ---- trigger, length enable */
    IoRegisters[NR44] = val | bx10111111;
    noise.lengthenable = (val & 64);
    if (val & 128) { /* trigger init */
      if (IoRegisters[NR42] & 0xF8) {
        CHANON_NOISE; /* if DAC is off, channel is disabled too */
      } else {
        CHANOFF_NOISE;
      }
      if (noise.length == 0) noise.length = 64;
      /* noise channel's LFSR bits should be "all set to 1" */
      /* TODO I am not emulating the LFSR, so...
       * (instead I use precomputed white noise) */
      /* volume envelope timer is reloaded with period */
      noise.envelopecounter = (IoRegisters[NR42] & 7);
      /* channel volume is reloaded with NRx2 */
      noise.curvol = (IoRegisters[NR42] >> 4);
    }

  /********* GENERAL CONTROL *********/
  } else if (addr == NR50 || addr == NR51) {
    /* volume and channel stereo dispatch - pass-trough */
    IoRegisters[addr] = val;
  } else if (addr == NR52) { /* sound on/off */
    val &= 128;
    if (val == 0) { /* all channels off, reset all registers */
      memset(IoRegisters + 0xFF10, 0xff, 0xFF30 - 0xFF10);
      IoRegisters[NR10] = 0x80;
      IoRegisters[NR11] = 0x3F;
      IoRegisters[NR12] = 0;
      IoRegisters[NR14] = 0xBF;
      IoRegisters[NR21] = 0x3F;
      IoRegisters[NR22] = 0;
      IoRegisters[NR24] = 0xBF;
      IoRegisters[NR30] = 0x7F;
      IoRegisters[NR32] = 0x9F;
      IoRegisters[NR34] = 0xBF;
      IoRegisters[NR42] = 0;
      IoRegisters[NR43] = 0;
      IoRegisters[NR44] = 0xBF;
      IoRegisters[NR50] = 0;
      IoRegisters[NR51] = 0;
      IoRegisters[NR52] = 0x70;
      memset(&sq1, 0, sizeof(sq1));
      memset(&sq2, 0, sizeof(sq2));
      memset(&wave, 0, sizeof(wave));
      memset(&noise, 0, sizeof(noise));
    } else {
      IoRegisters[NR52] |= bx11110000;
    }
    /********* WAVE TABLE *********/
  } else if ((addr >= 0xFF30) && (addr <= 0xFF3F)) {
    IoRegisters[addr] = val;
  }
}


/* length (in bytes, not samples) of the audio queue. that many bytes will be
 * buffered each time before being sent to the audio device */
#define SOUNDQUEUELEN 128    /* MUST be an even number! */


static void sound_queuesample(signed short l, signed short r) {
  /* sound sampling rate auto-adaptation - the gameboy CPU freq is not
   * divisible by the host's sound sampling rate, so there are always some
   * slight adjustements that needs to be done. besides, no oscillator in the
   * world is perfect so I have to account for that, too. */
  static unsigned char soundqueue[SOUNDQUEUELEN];
  static short soundqueuepos = 0;
  unsigned short qlen = drv_soundgetqueuelen();
  short increment[] = {0, 0, 0, 1};
  static short incstep;
  if (qlen > 10000) {
    /* printf("%u (%d)\n", qlen, sound_sampleschedfix); */
    if (sound_sampleschedfix < 1000) sound_sampleschedfix += increment[incstep];
    incstep++; /* do not change the scheduler too quickly */
    incstep &= 3;
  } else if (qlen < 5000) {
    /* printf("%u (%d)\n", qlen, sound_sampleschedfix); */
    if (sound_sampleschedfix > -1000) sound_sampleschedfix -= increment[incstep];
    incstep++; /* do not change the scheduler too quickly */
    incstep &= 3;
  }
  /* queue the sample to the output driver, making it unsigned in the process */
  soundqueue[soundqueuepos++] = l + 128;
  soundqueue[soundqueuepos++] = r + 128;
  if (soundqueuepos == SOUNDQUEUELEN) {
    drv_soundqueue(soundqueue, SOUNDQUEUELEN);
    soundqueuepos = 0;
  #ifdef DBGSOUNDFILE
    if (fp != NULL) fwrite(soundqueue, 1, SOUNDQUEUELEN, fp);
  #endif
  }
}


static void sound_gensample(void) {
  int i;
  static short output[4]; /* output for each channel */
  static short outputleft[4]; /* L output for each channel */
  static short outputright[4]; /* R output for each channel */
  static const short sqduties[4][8] = {
    {-20,-31,-31,-31,-31,-31,-20,31}, /* each waveform */
    {31,-20,-31,-31,-31,-31,-20,31},  /* takes 8 freq */
    {31,-20,-31,-31,-31,20,31,31},    /* timer clocks */
    {-31,20,31,31,31,31,20,-31}};
  /* note to self: declaring arrays "static const" (as opposed to just
   * "const") makes a *huge* speed difference under DJGPP */
  static const signed char noise7[16] = {30,25,11,-25,9,-21,-30,24,13,-13,-15,-3,20,-25,10,-31};
  static const signed char noise15[4096] = {
  31,30,31,25,31,11,31,-24,30,15,25,0,10,31,-31,30,
  30,25,25,11,11,-24,-25,14,9,-6,-21,10,-31,-30,31,24,
  31,13,31,-12,30,-8,24,-16,12,0,-11,30,-30,24,24,13,
  13,-12,-13,-9,-15,-23,-3,-19,21,-11,-31,-25,31,10,31,-30,
  30,27,25,7,11,15,-26,-1,11,25,-25,10,10,-30,-31,26,
  29,1,19,27,-9,7,-21,13,-31,-12,31,-11,30,-26,24,3,
  13,23,-13,-17,-13,-7,-14,12,-13,-11,-10,-27,-29,4,17,5,
  -6,3,3,21,21,-30,-31,24,30,14,25,-6,10,11,-31,-25,
  31,9,31,-20,30,-24,25,15,8,0,-19,30,-11,25,-25,9,
  9,-20,-21,-25,-30,9,24,-23,12,-18,-10,-13,-31,-15,31,-2,
  30,19,24,-8,14,-16,-4,-1,7,24,13,15,-15,-2,-3,18,
  20,-15,-26,-1,11,26,-25,0,10,29,-31,18,30,-14,25,-4,
  9,7,-22,14,-29,-6,21,8,-30,-18,19,-15,-9,-3,-18,17,
  -13,-5,-14,5,-13,2,-11,16,-30,-3,24,21,15,-28,-3,22,
  22,-23,-22,-17,-23,-5,-18,2,-5,18,5,-13,0,-11,31,-30,
  30,24,25,13,11,-12,-25,-9,8,-22,-19,-21,-10,-31,-31,25,
  30,11,25,-24,10,15,-30,-1,26,25,0,11,30,-25,24,10,
  13,-30,-13,26,-14,0,-4,28,6,20,10,-25,-30,13,24,-12,
  12,-8,-11,-17,-27,-7,5,13,1,-14,25,-12,10,-11,-31,-27,
  28,5,21,3,-30,23,19,-19,-11,-11,-26,-30,11,27,-24,6,
  15,9,-2,-21,27,-31,7,30,12,24,-10,15,-28,-1,22,25,
  -23,11,-17,-27,-7,6,13,11,-14,-26,-12,2,-8,16,-17,-3,
  -6,21,3,-31,20,25,-25,8,9,-17,-21,-7,-31,13,25,-15,
  9,-2,-21,18,-31,-14,30,-7,27,13,6,-12,11,-10,-25,-20,
  8,-24,-18,14,-15,-7,-3,9,20,-21,-24,-29,15,18,-3,-15,
  21,-3,-30,22,26,-21,2,-30,18,26,-13,2,-14,19,-5,-9,
  2,-24,19,-16,-8,-4,-20,23,-8,-17,-19,-2,-10,19,-20,-10,
  -28,-28,7,20,15,-28,0,7,28,14,20,-5,-24,1,14,24,
  -4,12,4,-12,4,-25,4,13,5,-15,2,-7,16,9,-4,-24,
  23,-16,-16,-3,-4,22,23,-20,-19,-27,-9,6,-21,11,-31,-24,
  31,12,31,-10,30,-28,24,23,13,-16,-12,-1,-12,24,-24,12,
  12,-10,-11,-29,-27,16,5,-2,3,18,21,-12,-31,-11,31,-25,
  30,9,25,-20,10,-24,-30,14,24,-6,12,11,-12,-25,-24,8,
  15,-18,-1,-13,25,-15,9,-1,-21,24,-31,13,30,-15,24,-2,
  12,19,-11,-9,-26,-23,11,-18,-24,-13,12,-14,-8,-5,-17,0,
  -8,29,15,17,-3,-6,21,10,-31,-29,25,18,11,-14,-25,-5,
  13,1,-14,26,-12,0,-11,28,-27,20,6,-26,9,2,-23,19,
  -19,-8,-10,-20,-20,-8,-25,-16,13,-3,-13,20,-10,-27,-31,4,
  30,5,24,3,15,23,-2,-17,16,-7,-2,14,27,-4,5,4,
  2,4,19,7,-11,12,-31,-8,25,-16,10,0,-31,30,25,25,
  8,11,-18,-25,-13,9,-14,-22,-5,-21,1,-31,24,31,14,28,
  -6,20,11,-28,-24,7,12,15,-12,-1,-24,24,14,13,-5,-13,
  0,-16,28,0,20,28,-27,21,6,-31,9,28,-23,23,-18,-16,
  -12,-4,-12,23,-24,-17,12,-6,-10,10,-21,-31,-26,29,3,16,
  23,-3,-17,22,-2,-22,16,-28,-1,20,26,-24,1,12,24,-11,
  12,-25,-11,8,-26,-19,2,-10,17,-29,-6,18,11,-13,-25,-10,
  13,-28,-13,22,-14,-23,-4,-19,4,-12,6,-25,11,13,-24,-14,
  14,-4,-7,6,12,8,-9,-16,-18,-1,-13,25,-10,9,-31,-21,
  28,-30,21,27,-31,7,29,12,18,-10,-12,-29,-9,21,-22,-29,
  -20,17,-27,-7,5,14,1,-8,25,15,10,-3,-28,20,22,-26,
  -21,3,-29,20,16,-25,-1,9,26,-22,0,-23,29,-19,16,-9,
  -2,-23,27,-22,7,-23,12,-20,-9,-8,-22,-17,-28,-6,23,8,
  -19,-18,-14,-16,-5,0,5,30,2,27,18,5,-14,0,-6,31,
  3,30,20,27,-26,4,3,6,22,8,-21,-17,-26,-5,3,2,
  21,16,-30,-1,24,26,15,1,-3,26,20,0,-25,30,8,26,
  -17,0,-6,29,10,17,-29,-6,17,11,-7,-25,9,13,-20,-14,
  -25,-4,8,4,-20,4,-9,5,-19,1,-15,27,-6,5,8,2,
  -20,19,-8,-9,-20,-18,-8,-13,-17,-10,-2,-29,18,21,-14,-31,
  -4,31,4,30,5,27,3,5,23,1,-18,24,-4,14,4,-5,
  4,5,4,1,7,25,13,9,-15,-22,-3,-21,21,-31,-31,30,
  30,26,25,1,11,27,-25,6,10,9,-31,-21,31,-30,31,27,
  28,7,21,15,-30,0,19,28,-9,21,-21,-30,-30,27,24,4,
  13,5,-14,2,-13,16,-10,-3,-31,20,31,-26,28,3,22,23,
  -23,-16,-17,-3,-6,22,3,-21,20,-26,-25,0,10,30,-31,24,
  30,13,25,-12,10,-8,-31,-17,28,-6,21,11,-31,-24,25,12,
  11,-10,-25,-29,8,17,-18,-5,-13,1,-11,25,-30,10,24,-30,
  12,27,-10,6,-28,8,22,-18,-20,-12,-25,-11,13,-25,-13,8,
  -14,-19,-5,-11,1,-28,25,7,11,12,-26,-11,11,-26,-25,2,
  9,17,-21,-5,-30,1,19,24,-10,13,-31,-14,28,-4,20,7,
  -28,15,7,-3,13,20,-13,-24,-13,14,-13,-7,-15,12,-4,-11,
  23,-27,-17,6,-6,10,10,-29,-28,21,23,-28,-16,23,-3,-20,
  21,-8,-31,-19,31,-9,31,-22,29,-20,19,-24,-11,15,-25,-2,
  8,19,-19,-9,-10,-23,-20,-22,-24,-20,12,-27,-9,4,-21,4,
  -31,5,31,0,29,29,17,19,-7,-9,13,-19,-14,-14,-5,-5,
  5,0,2,31,19,29,-11,16,-26,0,3,31,21,30,-31,26,
  29,2,19,17,-9,-5,-22,5,-29,3,21,20,-30,-26,24,0,
  14,29,-7,18,14,-16,-7,0,9,30,-21,27,-29,7,17,12,
  -6,-11,3,-31,22,25,-22,8,-20,-17,-25,-5,9,2,-23,16,
  -22,-2,-24,16,-16,-1,-4,26,23,3,-19,20,-11,-26,-25,11,
  10,-24,-31,14,29,-6,18,11,-16,-25,0,13,31,-13,30,-13,
  24,-14,12,-4,-11,6,-28,8,7,-18,13,-14,-13,-12,-10,-9,
  -29,-23,16,-18,-2,-13,16,-10,0,-31,30,31,25,28,11,21,
  -24,-29,15,17,-3,-5,21,0,-31,30,25,26,8,1,-18,26,
  -13,1,-13,25,-15,10,-1,-31,24,28,13,22,-12,-23,-8,-19,
  -19,-11,-10,-27,-20,1,-24,27,14,4,-5,4,0,4,31,7,
  29,12,17,-10,-6,-29,10,21,-30,-30,27,27,4,7,5,14,
  2,-5,19,5,-10,0,-21,31,-26,31,0,28,29,20,19,-25,
  -8,9,-19,-22,-11,-21,-26,-26,0,3,30,22,24,-21,14,-30,
  -5,26,2,0,17,31,-5,28,2,23,17,-18,-4,-16,4,-1,
  7,29,14,17,-5,-6,1,10,25,-28,9,20,-20,-27,-24,5,
  12,0,-12,28,-24,20,12,-26,-10,2,-31,16,28,-2,22,19,
  -20,-8,-28,-19,7,-9,15,-24,-2,-16,19,-1,-12,26,-24,3,
  12,23,-11,-17,-25,-7,10,13,-29,-13,21,-14,-29,-4,16,4,
  -4,5,23,1,-19,24,-14,14,-7,-4,12,6,-12,11,-25,-25,
  13,9,-12,-21,-9,-31,-22,28,-23,21,-18,-31,-12,30,-11,27,
  -26,6,3,9,22,-21,-20,-29,-24,18,12,-13,-11,-15,-30,-3,
  26,21,0,-28,30,22,26,-21,1,-30,24,26,14,2,-6,16,
  10,-4,-30,23,19,-16,-11,-1,-26,29,11,19,-27,-9,5,-22,
  1,-22,27,-28,4,20,6,-28,9,7,-23,13,-20,-12,-8,-12,
  -17,-24,-7,15,13,-3,-13,17,-15,-5,-2,0,27,28,5,22,
  0,-24,29,-16,16,-3,-2,21,27,-30,4,24,6,12,9,-9,
  -21,-18,-31,-12,29,-11,17,-26,-5,3,1,21,26,-30,2,24,
  18,12,-14,-10,-5,-20,0,-26,29,11,16,-27,-3,5,21,1,
  -30,24,19,14,-11,-7,-27,9,4,-20,7,-26,15,11,-3,-26,
  20,2,-26,18,2,-13,18,-15,-12,-2,-9,27,-23,6,-17,9,
  -8,-24,15,-17,0,-2,28,18,23,-13,-20,-14,-8,-8,-17,15,
  -8,-1,15,29,-1,17,26,-5,1,2,25,16,9,-1,-24,24,
  -16,13,-3,-16,20,0,-27,28,4,22,6,-24,9,-17,-23,-2,
  -18,19,-4,-12,4,-25,7,13,15,-15,-1,-6,24,10,12,-29,
  -8,16,-16,-3,-1,21,29,-30,16,24,-1,13,25,-14,10,-7,
  -31,12,28,-10,23,-28,-17,23,-6,-20,11,-9,-28,-18,7,-12,
  15,-10,-2,-20,18,-25,-14,10,-7,-28,12,22,-10,-20,-28,-25,
  20,9,-25,-20,8,-27,-18,4,-15,5,-4,1,23,26,-18,2,
  -15,18,-3,-12,22,-9,-20,-21,-25,-29,9,18,-23,-15,-19,-2,
  -14,16,-5,0,2,30,19,27,-11,4,-26,7,2,15,19,-1,
  -10,26,-29,3,18,23,-15,-17,-1,-2,26,18,3,-13,20,-15,
  -24,-1,14,26,-7,0,14,28,-7,23,14,-18,-6,-4,8,6,
  -17,11,-3,-25,17,13,-4,-16,6,-1,8,29,-19,17,-9,-5,
  -23,5,-23,3,-23,20,-22,-25,-23,10,-17,-29,-7,18,13,-13,
  -13,-10,-10,-29,-29,16,17,-2,-5,19,5,-11,0,-31,31,25,
  31,8,31,-18,30,-12,25,-8,9,-16,-21,-1,-31,25,25,8,
  8,-18,-19,-13,-11,-15,-30,-6,26,11,0,-24,31,14,31,-5,
  30,1,24,27,15,7,-3,14,20,-7,-24,14,14,-5,-4,0,
  6,28,11,23,-27,-18,5,-12,0,-10,31,-20,30,-27,25,5,
  8,3,-20,22,-8,-23,-19,-22,-9,-21,-21,-26,-29,3,18,20,
  -15,-27,-1,1,26,26,0,2,30,16,24,-1,14,25,-4,10,
  4,-31,4,31,5,29,3,17,23,-6,-17,8,-3,-18,17,-4,
  -5,4,5,7,1,13,25,-14,9,-7,-21,12,-31,-10,31,-31,
  30,29,25,19,11,-8,-25,-17,13,-6,-13,10,-11,-31,-30,28,
  27,21,7,-28,15,22,-3,-20,21,-25,-31,10,30,-29,24,17,
  13,-4,-13,6,-11,8,-31,-19,25,-10,11,-28,-26,22,3,-22,
  23,-21,-19,-29,-9,18,-21,-12,-31,-8,25,-19,10,-10,-31,-29,
  25,17,11,-4,-25,6,13,9,-15,-21,-6,-31,11,28,-27,23,
  5,-16,3,-2,20,27,-24,4,12,6,-12,8,-25,-19,13,-10,
  -13,-29,-10,16,-28,-3,23,21,-18,-28,-15,20,-1,-24,25,14,
  8,-5,-19,0,-12,29,-24,17,12,-4,-11,6,-31,8,25,-18,
  9,-12,-21,-9,-26,-22,2,-23,17,-19,-7,-10,14,-21,-4,-26,
  7,2,12,19,-11,-10,-26,-29,11,17,-24,-5,15,1,-3,26,
  17,0,-7,30,12,27,-9,5,-22,2,-22,17,-28,-7,20,14,
  -25,-6,13,8,-14,-19,-12,-11,-9,-27,-18,4,-12,5,-10,1,
  -21,26,-26,1,0,24,28,12,22,-9,-23,-22,-19,-23,-10,-18,
  -31,-4,30,7,27,15,5,0,1,30,26,27,2,4,17,4,
  -6,6,3,11,21,-26,-30,0,27,30,6,25,10,10,-30,-28,
  26,23,1,-16,27,-1,4,26,7,0,12,31,-11,29,-25,18,
  9,-14,-21,-5,-26,1,2,24,19,12,-11,-10,-27,-20,4,-24,
  5,14,0,-5,28,5,20,0,-25,29,13,16,-15,-3,-3,20,
  17,-27,-7,6,14,11,-8,-26,15,2,0,19,30,-9,26,-21,
  3,-30,23,26,-19,2,-10,18,-29,-12,18,-8,-12,-17,-9,-2,
  -23,18,-22,-14,-24,-7,-16,14,-2,-6,27,3,6,20,11,-28,
  -27,7,6,15,8,-2,-16,27,-1,7,26,13,0,-15,31,-3,
  30,22,24,-22,14,-20,-5,-25,2,8,18,-16,-15,-4,-3,23,
  17,-17,-7,-6,14,3,-5,21,5,-30,0,19,30,-10,25,-31,
  9,29,-20,18,-24,-14,15,-7,-2,12,18,-11,-12,-26,-9,11,
  -22,-25,-21,9,-30,-23,26,-17,1,-6,24,10,15,-29,-2,16,
  19,-3,-9,22,-18,-20,-15,-25,-1,13,26,-13,0,-13,28,-15,
  20,-1,-27,25,4,8,6,-20,8,-9,-19,-18,-14,-13,-5,-10,
  5,-30,2,19,17,-10,-5,-31,5,31,3,29,23,17,-16,-7,
  -1,14,29,-4,17,4,-6,5,3,1,21,25,-30,8,24,-17,
  12,-6,-11,10,-31,-31,25,29,11,19,-24,-9,15,-22,-2,-21,
  19,-31,-11,30,-25,27,9,7,-20,14,-26,-6,11,8,-26,-19,
  11,-10,-25,-29,13,17,-12,-5,-9,0,-19,28,-14,21,-7,-30,
  13,26,-15,3,-2,22,18,-23,-13,-18,-13,-4,-14,6,-13,8,
  -11,-19,-30,-11,27,-26,5,3,3,22,22,-20,-21,-27,-29,6,
  18,10,-16,-31,0,25,31,10,31,-29,30,17,25,-4,11,7,
  -27,14,1,-6,25,10,10,-29,-28,16,23,-2,-17,19,-2,-12,
  16,-24,0,12,31,-8,30,-19,24,-10,13,-28,-14,22,-4,-23,
  7,-20,12,-9,-10,-18,-20,-13,-25,-14,13,-7,-13,12,-16,-11,
  0,-27,30,4,25,6,10,9,-29,-21,21,-30,-28,27,20,4,
  -26,5,2,0,19,28,-10,22,-31,-20,29,-24,16,15,-1,-1,
  24,29,12,16,-9,-3,-23,20,-22,-26,-23,0,-17,30,-7,26,
  14,3,-6,21,10,-30,-29,19,18,-8,-15,-17,-6,-2,11,18,
  -26,-12,0,-8,31,-17,30,-5,25,1,9,27,-22,6,-23,9,
  -20,-23,-8,-18,-16,-4,-4,7,23,13,-19,-15,-14,-2,-5,27,
  0,6,30,11,24,-27,15,5,0,2,30,16,27,-1,4,25,
  7,10,12,-29,-11,21,-26,-29,3,17,20,-5,-27,2,1,18,
  25,-15,8,-1,-16,24,-1,13,26,-14,0,-7,28,12,20,-9,
  -24,-22,14,-23,-6,-19,8,-12,-18,-24,-4,15,7,-3,14,17,
  -7,-6,14,10,-5,-28,5,22,3,-21,23,-26,-19,0,-9,30,
  -23,27,-17,7,-6,12,10,-9,-28,-18,22,-12,-23,-8,-22,-19,
  -21,-9,-31,-21,25,-29,11,17,-27,-5,5,1,1,26,25,3,
  8,20,-19,-27,-9,5,-21,1,-31,27,31,4,28,5,20,3,
  -25,23,13,-19,-15,-11,-2,-27,27,4,7,6,14,8,-5,-16,
  5,-2,3,27,21,5,-31,0,28,30,23,25,-19,11,-10,-28,
  -29,7,17,15,-6,-1,3,29,22,17,-21,-7,-31,14,25,-5,
  9,1,-22,26,-28,1,20,24,-27,13,6,-15,8,-4,-19,23,
  -11,-17,-25,-2,10,19,-28,-9,20,-22,-24,-20,15,-27,-3,4,
  22,4,-24,6,-17,10,-3,-30,17,19,-4,-12,7,-25,13,13,
  -12,-14,-9,-5,-23,0,-20,29,-8,16,-19,0,-11,31,-30,29,
  24,19,13,-8,-13,-17,-10,-7,-29,12,21,-10,-30,-28,26,20,
  1,-26,27,2,4,18,4,-16,6,-1,11,29,-25,17,10,-4,
  -31,6,25,9,9,-20,-22,-25,-20,9,-27,-23,4,-17,5,-8,
  0,15,31,-2,29,16,18,-2,-14,19,-12,-10,-12,-29,-24,16,
  15,-2,-1,18,29,-15,16,-1,0,25,30,9,26,-23,0,-18,
  29,-13,16,-13,0,-15,30,-6,24,8,12,-18,-8,-13,-17,-15,
  -2,-3,18,17,-15,-8,-2,15,27,-1,5,26,2,0,18,31,
  -15,28,-1,23,25,-17,11,-5,-28,0,7,29,13,18,-15,-12,
  -3,-9,17,-23,-5,-18,1,-5,24,5,15,0,-3,28,17,20,
  -7,-26,13,11,-15,-26,-3,2,21,16,-29,-1,18,26,-12,1,
  -11,25,-27,10,6,-30,8,26,-18,3,-12,23,-9,-18,-21,-13,
  -30,-14,19,-7,-9,13,-24,-14,-16,-4,-1,4,24,7,15,12,
  -3,-11,17,-27,-5,6,1,11,26,-26,3,0,23,28,-17,22,
  -5,-22,1,-29,24,21,14,-31,-6,28,8,20,-18,-24,-12,15,
  -11,-2,-27,18,4,-14,6,-6,11,3,-26,21,11,-28,-27,22,
  6,-22,9,-22,-23,-28,-17,23,-5,-20,1,-9,24,-18,15,-15,
  -1,-3,29,20,18,-25,-13,9,-13,-22,-15,-21,-2,-31,16,28,
  -1,22,25,-20,11,-27,-27,4,6,5,8,2,-17,19,-2,-9,
  16,-18,0,-16,31,0,29,28,18,21,-13,-29,-14,21,-7,-29,
  13,16,-15,0,-3,30,17,24,-7,14,13,-5,-13,5,-16,2,
  -1,16,29,-3,16,22,0,-23,28,-22,22,-23,-21,-18,-29,-15,
  18,-1,-12,25,-9,9,-21,-21,-31,-30,29,24,16,13,-2,-13,
  18,-10,-15,-31,-3,31,21,30,-28,26,23,2,-16,17,-1,-8,
  26,15,3,-3,21,20,-29,-25,18,10,-13,-31,-15,25,-2,10,
  19,-31,-9,30,-22,27,-20,7,-24,12,14,-9,-4,-23,6,-20,
  9,-9,-24,-18,-16,-12,-1,-12,29,-24,18,12,-14,-11,-5,-30,
  0,26,29,3,19,20,-9,-27,-21,1,-29,27,16,4,-1,5,
  24,1,15,24,-2,12,16,-11,-3,-26,20,11,-26,-27,2,6,
  17,8,-6,-18,3,-5,23,5,-19,0,-15,30,-6,27,8,6,
  -18,11,-14,-25,-12,13,-8,-13,-17,-15,-7,-3,12,17,-11,-6,
  -26,10,11,-30,-26,26,3,1,23,26,-17,2,-5,18,0,-13,
  30,-10,27,-31,6,29,9,18,-20,-12,-25,-8,13,-19,-13,-11,
  -15,-27,-6,4,11,4,-26,7,11,15,-27,-1,1,25,26,10,
  2,-29,16,16,-2,-2,19,27,-10,4,-31,7,28,15,23,0,
  -18,31,-13,28,-13,23,-14,-17,-4,-7,4,12,7,-9,13,-19,
  -13,-14,-15,-5,-6,0,10,28,-28,23,20,-16,-26,0,0,28,
  31,20,28,-25,21,9,-31,-20,28,-27,21,5,-31,3,28,20,
  23,-26,-19,3,-9,20,-23,-24,-17,15,-5,-4,0,23,28,-18,
  22,-15,-22,-2,-28,16,22,-1,-22,25,-28,8,20,-17,-27,-6,
  5,8,1,-20,25,-8,11,-19,-26,-11,3,-26,21,2,-28,18,
  22,-13,-22,-14,-28,-7,22,14,-24,-6,-16,8,-2,-18,27,-4,
  7,4,13,4,-15,7,-7,14,9,-8,-22,15,-22,0,-28,28,
  22,22,-21,-22,-30,-23,24,-17,14,-6,-6,10,3,-31,21,31,
  -28,28,23,22,-16,-22,0,-24,28,-16,22,-3,-22,21,-28,-31,
  20,30,-25,25,9,8,-20,-19,-25,-10,9,-31,-22,28,-20,21,
  -24,-31,15,30,-3,24,21,12,-28,-9,22,-21,-23,-30,-18,24,
  -15,14,-2,-4,18,6,-16,10,-1,-28,29,22,19,-21,-8,-31,
  -19,25,-9,11,-22,-26,-21,3,-30,20,26,-25,2,9,18,-21,
  -15,-30,-2,19,16,-9,-3,-22,17,-28,-4,20,4,-25,5,13,
  0,-15,28,-6,20,8,-28,-18,7,-15,15,-4,-2,23,18,-17,
  -13,-6,-14,3,-13,22,-10,-23,-31,-19,30,-10,26,-28,3,23,
  23,-17,-16,-5,-4,2,23,19,-18,-11,-16,-26,0,11,31,-25,
  30,10,25,-30,10,27,-30,6,27,9,6,-20,11,-26,-24,11,
  12,-24,-11,14,-26,-7,2,13,16,-13,0,-14,30,-12,24,-11,
  12,-26,-11,2,-26,16,2,-2,18,18,-12,-14,-12,-12,-24,-9,
  14,-22,-7,-21,13,-31,-15,31,-1,30,25,24,11,14,-24,-7,
  14,13,-8,-13,15,-16,-1,0,24,30,12,26,-9,0,-22,28,
  -21,21,-29,-31,17,30,-7,25,13,9,-12,-22,-9,-21,-22,-31,
  -23,29,-17,16,-6,-2,11,27,-26,5,0,3,28,22,23,-21,
  -19,-30,-9,24,-21,15,-30,0,26,28,0,21,30,-29,25,18,
  8,-14,-19,-5,-14,1,-6,25,3,10,20,-28,-27,20,5,-25,
  3,8,20,-16,-27,-3,5,22,1,-24,24,-16,14,-3,-6,20,
  3,-28,20,7,-25,12,8,-9,-16,-23,-1,-19,25,-14,8,-7,
  -16,12,-2,-11,27,-30,6,24,9,12,-20,-8,-25,-16,8,-3,
  -19,20,-14,-27,-7,1,14,26,-7,3,14,22,-7,-20,14,-26,
  -5,11,2,-26,16,11,-2,-28,18,7,-14,12,-6,-8,3,-18,
  22,-4,-23,4,-23,6,-23,10,-23,-29,-22,18,-20,-13,-25,-13,
  13,-13,-13,-15,-15,-3,-3,20,20,-27,-25,6,10,10,-31,-31};

  /* compute the samples for each channel */
#ifdef CHAN1
/* if freq > 2 kHz, mute it (this is, supposedly, what happens on the DMG,
 * because of its sound hardware limitations. Some games rely on this and set
 * frequency to very high values to shut it (eg. Home Alone intro used this on
 * the wave channel) */
  if (sq1.freq > 2041) {
    output[0] = 0;
  } else {
    output[0] = sqduties[sq1.dutyselector][sq1.dutystep];
    output[0] *= sq1.enabled;
    output[0] *= sq1.curvol;
    output[0] /= 15;
  }
#else
  output[0] = 0;
#endif

#ifdef CHAN2
/* if freq > 2 kHz, mute it (this is, supposedly, what happens on the DMG,
 * because of its sound hardware limitations. Some games rely on this and set
 * frequency to very high values to shut it (eg. Home Alone intro used this on
 * the wave channel) */
  if (sq2.freq > 2041) {
    output[1] = 0;
  } else {
    output[1] = sqduties[sq2.dutyselector][sq2.dutystep];
    output[1] *= sq2.enabled;
    output[1] *= sq2.curvol;
    output[1] /= 15;
  }
#else
  output[1] = 0;
#endif

#ifdef CHAN3
/* if freq > 2 kHz, mute it (this is, supposedly, what happens on the DMG,
 * because of its sound hardware limitations. Some games rely on this and set
 * frequency to very high values to shut it (eg. Home Alone intro used this on
 * the wave channel) */
  if (wave.freq > 2041) {
    output[2] = 0;
  } else {
    output[2] = IoRegisters[0xFF30 + (wave.samplepos >> 1)]; /* get the byte */
    output[2] >>= ((wave.samplepos & 1) ^ 1) * 4; /* select the proper nibble */
    output[2] &= 15; /* drop the other nibble, if any */
    output[2] *= wave.enabled;
    output[2] >>= wave.curvolshift;
    output[2] <<= 2; /* normalize to 0..60 range */
    output[2] -= 30; /* my samples are signed */
  }
#else
  output[2] = 0;
#endif

#ifdef CHAN4
  if (IoRegisters[NR43] & 4) {
    output[3] = noise7[noise.step & (sizeof(noise7) - 1)];
  } else {
    output[3] = noise15[noise.step & (sizeof(noise15) - 1)];
  }
  output[3] *= noise.enabled;
  output[3] *= noise.curvol;
  output[3] /= 22; /* in theory this should be /= 15, but I prefer the noise
                      channel to be quiter */
#else
  output[3] = 0;
#endif

  /* stereo dispatch (each channel can belong to left, right or both) */
  outputleft[0] = output[0] * (IoRegisters[NR51] & 1);
  outputleft[1] = output[1] * ((IoRegisters[NR51] >> 1) & 1);
  outputleft[2] = output[2] * ((IoRegisters[NR51] >> 2) & 1);
  outputleft[3] = output[3] * ((IoRegisters[NR51] >> 3) & 1);
  outputright[0] = output[0] * ((IoRegisters[NR51] >> 4) & 1);
  outputright[1] = output[1] * ((IoRegisters[NR51] >> 5) & 1);
  outputright[2] = output[2] * ((IoRegisters[NR51] >> 6) & 1);
  outputright[3] = output[3] * ((IoRegisters[NR51] >> 7) & 1);

  /* mix samples for each side (mixing is a big word for this - I simply
   * add signals, but apparenly that's exactly what the DMG does) */
  for (i = 1; i < 4; i++) {
    outputright[0] += outputright[i];
    outputleft[0] += outputleft[i];
  }

  /* apply volume for each side */
  outputleft[0] *= (IoRegisters[NR50] & 7); /* LEFT volume */
  outputleft[0] /= 7; /* normalize back to -128..+127 range */
  outputright[0] *= ((IoRegisters[NR50] >> 3) & 7); /* RIGHT volume */
  outputright[0] /= 7; /* normalize back to -128..+127 range */

  sound_queuesample(outputleft[0], outputright[0]);
}


inline static void UpdateSound(int cycles, int nosoundflag) {
  static const short clockpattern256[8] = {1, 0, 1, 0, 1, 0, 1, 0};
  static const short clockpattern128[8] = {0, 0, 1, 0, 0, 0, 1, 0};
  static const short clockpattern64[8]  = {0, 0, 0, 0, 0, 0, 0, 1};
  static long nextsamplecounter = 0;

  #ifdef DBGSOUNDFILE
    static FILE *fp = NULL;
    if (fp == NULL) fp = fopen(DBGSOUNDFILE, "wb");
  #endif

  /* update the frame sequencer counters and clock flags */
  frameseq.counter -= cycles; /* 512 Hz is one clock every 8192 CPU cycles */
  if (frameseq.counter <= 0) {
    frameseq.counter += 8192;
    frameseq.clock512hz++;
    frameseq.clock512hz &= 7; /* the 512hz clock loops through 8 states */
                              /* that the other clocks are derived from */
    frameseq.clock256hz = clockpattern256[frameseq.clock512hz];
    frameseq.clock128hz = clockpattern128[frameseq.clock512hz];
    frameseq.clock64hz = clockpattern64[frameseq.clock512hz];
  } else {
    frameseq.clock256hz = 0;
    frameseq.clock128hz = 0;
    frameseq.clock64hz = 0;
  }

  /* update freq timers for each channel */
  /* CH1 (sq1) */
  sq1.freqtimer -= cycles;
  if (sq1.freqtimer < 0) { /* got a clock! */
    sq1.freqtimer += 4 * (2048 - sq1.freq);
    sq1.dutystep += 1;
    sq1.dutystep &= 7;
  }
  /* CH2 (sq2) */
  sq2.freqtimer -= cycles;
  if (sq2.freqtimer < 0) { /* got a clock! */
    sq2.freqtimer += 4 * (2048 - sq2.freq);
    sq2.dutystep += 1;
    sq2.dutystep &= 7;
  }
  /* CH3 (wave) */
  wave.freqtimer -= cycles;
  if (wave.freqtimer < 0) { /* got a clock! */
    wave.freqtimer += 2 * (2048 - wave.freq);
    wave.samplepos++;
    wave.samplepos &= 31;
  }
  /* CH4 (noise) */
  noise.freqtimer -= cycles;
  if (noise.freqtimer < 0) { /* got a clock! */
    /* Controlled by NR43 (SSSS WDDD)
     * Bit 7-4 - Shift Clock Frequency (s)
     * Bit 3   - Counter Step/Width (0=15 bits, 1=7 bits)
     * Bit 2-0 - Dividing Ratio of Frequencies (r)
     * Frequency = 524288 Hz / r / 2^(s+1) ;For r=0 assume r=0.5 instead */
    unsigned long shiftclock = (1 << ((IoRegisters[NR43] >> 4) + 1));
    unsigned long divider = (IoRegisters[NR43] & 7) * 2;
    if (divider == 0) divider = 1;
    noise.freqtimer += 4 * divider * shiftclock;
    noise.step++;
  }

  /* update length counters (unless 0 already) */
  /* each length counter is clocked at 256 Hz by the frame sequencer. When
   * clocked while enabled by NRx4 and the counter is not zero, it is
   * decremented. If it becomes zero, the channel is disabled. */
  if (frameseq.clock256hz) {
    if (sq1.lengthenable && sq1.length) {
      if (sq1.length <= 1) {
        CHANOFF_SQ1;
      } else {
        sq1.length--;
      }
    }
    if (sq2.lengthenable && sq2.length) {
      if (sq2.length <= 1) {
        CHANOFF_SQ2;
      } else {
        sq2.length--;
      }
    }
    if (wave.lengthenable && wave.length) {
      if (wave.length <= 1) {
        CHANOFF_WAVE;
      } else {
        wave.length--;
      }
    }
    if (noise.lengthenable && noise.length) {
      if (noise.length <= 1) {
        CHANOFF_NOISE;
      } else {
        noise.length--;
      }
    }
  }

  /* update volume envelope */
  if (frameseq.clock64hz) {
    short envdir[2] = {-1, 1};
    /* sq1 */
    if (IoRegisters[NR12] & 7) {
      sq1.envelopecounter--;
      if (sq1.envelopecounter < 1) {
        sq1.curvol += envdir[(IoRegisters[NR12] >> 3) & 1];
        if (sq1.curvol < 0) sq1.curvol = 0;
        if (sq1.curvol > 15) sq1.curvol = 15;
      }
    }
    /* sq2 */
    if (IoRegisters[NR22] & 7) {
      sq2.envelopecounter--;
      if (sq2.envelopecounter < 1) {
        sq2.curvol += envdir[(IoRegisters[NR22] >> 3) & 1];
        if (sq2.curvol < 0) sq2.curvol = 0;
        if (sq2.curvol > 15) sq2.curvol = 15;
        sq2.envelopecounter = IoRegisters[NR22] & 7;
      }
    }
    /* noise */
    if (IoRegisters[NR42] & 7) {
      noise.envelopecounter--;
      if (noise.envelopecounter < 1) {
        noise.curvol += envdir[(IoRegisters[NR42] >> 3) & 1];
        if (noise.curvol < 0) noise.curvol = 0;
        if (noise.curvol > 15) noise.curvol = 15;
        noise.envelopecounter = IoRegisters[NR42] & 7;
      }
    }
  }

  /* update freq sweep (channel 1) */
  if (frameseq.clock128hz) {
    short sweepperiod = (IoRegisters[NR10] >> 4) & 7;
    short sweepexp[8] = {1, 2, 4, 8, 16, 32, 64, 128};
    short newfreq;
    short multip[2] = {1, -1};
    if ((sq1.enabled) && (sq1.sweepcounter > 0) && (sweepperiod != 0)) {
      sq1.sweeptimer--;
      if (sq1.sweeptimer < 1) {
        sq1.sweeptimer = sweepperiod;
        newfreq = sq1.sweepshadowfreq + (multip[(IoRegisters[NR10] >> 3) & 1] * (sq1.sweepshadowfreq / sweepexp[sq1.sweepcounter]));
        if (newfreq < 0) newfreq = 0;
        if (newfreq > 2047) {
          CHANOFF_SQ1;
        } else {
          sq1.freq = newfreq;
          sq1.sweepshadowfreq = newfreq;
        }
        sq1.sweepcounter--;
      }
    }
  }

  /* do I need to output an actual sound sample? */
  if (nosoundflag != 0) return;
  nextsamplecounter -= (cycles * 1024l);
  if (nextsamplecounter > 0) return;

  /* if sound is off, generate silence */
  if ((IoRegisters[NR52] & 128) == 0) {
    sound_queuesample(0, 0);
  } else { /* otherwise generate an actual sample */
    sound_gensample();
  }

  /* schedule next sample */
  nextsamplecounter += SAMPLESCHEDULER;
  nextsamplecounter += sound_sampleschedfix;

}
