/*
   ----------------------------------------
    MMU emulation (Memory Management Unit)
    This file is part of the zBoy project.
    Copyright (C) Mateusz Viste 2010-2019
   ----------------------------------------
*/

#include "sound.h"

enum mbc1_models {
  MBC1_16_8 = 1,
  MBC1_4_32 = 2
};

uint8_t MemoryBankedRAM[0x20A001];    /* Banked RAM [2MiB] */
#ifdef EMBEDROM
  #include EMBEDROM
#else
  uint8_t MemoryROM[4194304];       /* Declare an empty table for ROM Area (redim later at loadrom time) */
#endif
uint8_t MemoryMAP[0x10000];    /* Regular memory (fallback for unmapped regions) */

/* definitions below all point to MemoryMAP - in the past storage was separate
 * for each, and many places still refer to these old names */
#define MemoryInternalRAM MemoryMAP
#define MemoryInternalHiRAM MemoryMAP
#define VideoRAM MemoryMAP
#define SpriteOAM MemoryMAP
#define IoRegisters MemoryMAP

int Mbc1Model = MBC1_16_8;    /* MBC1 memory model (MbcModel can be 1 or 2)  1=16/8 ; 2=4/32 */
int CurRomBank = 0;           /* Used for ROM bank switching (must be at least 9 bits long for MBC5 support!) I am unsure whether this should default to 0 or rather 1 (since a RomBank of 0 is supposed to point to 1 anyway..) */
int CurRamBank = 0;           /* Current RAM bank selection */
int SaveScoresWriteProtection[2048];

#include "mbc0.c"  /* Support for ROM-ONLY ROMs */
#include "mbc1.c"  /* Support for MBC1 memory controllers */
#include "mbc2.c"  /* Support for MBC2 memory controllers */
#include "mbc3.c"  /* Support for MBC3 memory controllers (without TIMER support so far) */
#include "mbc5.c"  /* Support for MBC5 memory controllers (usually found in GBC games) */


uint8_t (*MemoryReadSpecial)(register int memaddr);
void (*MemoryWriteSpecial)(register int memaddr, uint8_t DataByte);



void InitRAM(void) {
  int x;
  /* first 16K of address space is always mapped to the first 16K of ROM */
  for (x = 0; x < 0x4000; x++) {
    MemoryMAP[x] = MemoryROM[x];
  }
  /* init all sound registers to 0xff */
  for (x = 0xFF10; x < 0xFF27; x++) {
    MemoryMAP[x] = 0xff;
  }
}


/* Below are generic MemoryRead and MemoryWrite routines. These routines  *
 * check if the called address is a well-known address. If this address   *
 * is behaving the same on all known MBC controllers, then it is answered *
 * here (and it is FAST). Otherwise, a Memory routine specialized for the *
 * given MBC is called.                                                   */


inline static int MemoryRead(int addr) {
  PrintDebug("MemRead 0x%04X\n", addr);
  /* is it special MBC memory? */
  if ((addr >= 0x4000) && (addr < 0x8000)) { /* MBC switchable ROM bank */
    return(MemoryReadSpecial(addr));
  } else if ((addr >= 0xA000) && (addr < 0xC000)) { /* MBC switchable RAM bank */
    return(MemoryReadSpecial(addr));
  } else if ((addr >= 0xE000) && (addr < 0xFE00)) { /* internal RAM mirror */
    return(MemoryMAP[addr - 8192]);
  } else { /* else we're good */
    return(MemoryMAP[addr]);
  }
}

inline static void MemoryWrite(int WriteAddr, uint8_t DataHolder) {
  PrintDebug("MemoryWrite 0x%04X [%02Xh]\n", WriteAddr, DataHolder);
  if ((WriteAddr >= 0xC000) && (WriteAddr < 0xE000)) {    /* Internal 8KiB RAM */
    if (TotalCycles < 4000000) { /* If the game has just been started (less than 1s) then check if some areas of RAM are not write protected for high-scores loading... */
     unsigned int TempUintCounter = 0;
     while ((SaveScoresWriteProtection[TempUintCounter] != WriteAddr) && (SaveScoresWriteProtection[TempUintCounter] != 0)) TempUintCounter++;
     if (SaveScoresWriteProtection[TempUintCounter] == 0) MemoryInternalRAM[WriteAddr] = DataHolder;
    } else {
      MemoryInternalRAM[WriteAddr] = DataHolder;
    }
  } else if ((WriteAddr >= 0xE000) && (WriteAddr < 0xFE00)) {    /* RAM mirror */
    MemoryInternalRAM[WriteAddr - 8192] = DataHolder;
  } else if ((WriteAddr >= 0xFE00) && (WriteAddr < 0xFEA0)) {    /* Sprite OAM memory */
    SpriteOAM[WriteAddr] = DataHolder;
  } else if ((WriteAddr >= 0xFF10) && (WriteAddr <= 0xFF3F)) {   /* Sound registers */
    sound_writemem(WriteAddr, DataHolder);
  } else if ((WriteAddr >= 0xFF80) && (WriteAddr <= 0xFFFF)) {   /* Hi RAM area */
    if (TotalCycles < 4000000) {  /* If the game has just been started (less than 1s) then check if some areas of RAM are not write protected for high-scores loading... */
      unsigned int TempUintCounter = 0;
      while ((SaveScoresWriteProtection[TempUintCounter] != WriteAddr) && (SaveScoresWriteProtection[TempUintCounter] != 0)) TempUintCounter++;
      if (SaveScoresWriteProtection[TempUintCounter] == 0) MemoryInternalHiRAM[WriteAddr] = DataHolder;
    } else {
      MemoryInternalHiRAM[WriteAddr] = DataHolder;
    }
  } else if ((WriteAddr >= 0x8000) && (WriteAddr < 0xA000)) {   /* Video RAM (8KiB) */
    /*if (GetLcdMode() != 3) { */ /* Check if LCD not in mode 3 (if so, write pulse is ignored) */
    VideoRAM[WriteAddr] = DataHolder;
    /*} */
  } else if (WriteAddr == 0xFF41) {                            /* STAT register: Do not allow to write into 2 last bits of the STAT */
    IoRegisters[0xFF41] = ((IoRegisters[0xFF41] & bx00000011) | (DataHolder & bx11111100)); /* register, as these bits are the mode flag. */
  } else if (WriteAddr == 0xFF44) { /* CURLINE [RW] Current Scanline. */
    IoRegisters[WriteAddr] = 0;     /* Writing into this register resets it. */
    /* SetUserMsg("LY RESET"); */
  } else if (WriteAddr == 0xFF46) {   /* Starts LCD OAM DMA transfer */
    static int x;
    if (DataHolder <= 0xF1) {         /* Normally, should take 160us... but who cares? */
      for (x = 0; x < 160; x++) { /* Let's copy XX00-XX9F to FE00-FE9F */
        SpriteOAM[0xFE00 | x] = MemoryRead((DataHolder << 8) | x);
      }
    } else {
      char msg[128];
      sprintf(msg, "MMU ERROR. INVALID OPERAND FOR OAM DMA TRANSFER: %02Xh", DataHolder);
      SetUserMsg(msg);
      QuitEmulator = -1;
    }
  } else if (WriteAddr == 0xFF04) {
    IoRegisters[0xFF04] = 0;      /* Divide register: Writing any value sets it to 0 */
  } else if ((WriteAddr >= 0xFF00) && (WriteAddr <= 0xFF4B)) {   /* I/O registers */
    IoRegisters[WriteAddr] = DataHolder;
  } else if (WriteAddr <= 65535) {
    MemoryWriteSpecial(WriteAddr, DataHolder);
  } else {
    PrintDebug("MemoryWrite called with invalid addr (0x%X)", WriteAddr);
  }
}
