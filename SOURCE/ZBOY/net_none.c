/*
 * dummy network driver that does nothing
 * This file is part of the zBoy project
 * Copyright (C) 2010-2019 Mateusz Viste
 */

#include <stdlib.h>

#include "net.h"

struct net_sock_t *net_open(char *bindaddr, char *playpeer, unsigned short port) {
  bindaddr = bindaddr;
  playpeer = playpeer;
  port = port;
  return(NULL);
}


int net_send(struct net_sock_t *sock, void *databuff, int len) {
  sock = sock;
  databuff = databuff;
  len = len;
  return(-1);
}


int net_recvpeek(struct net_sock_t *sock, void *databuff, int len) {
  sock = sock;
  databuff = databuff;
  len = len;
  return(-1);
}


int net_recv(struct net_sock_t *sock, void *databuff, int len) {
  sock = sock;
  databuff = databuff;
  len = len;
  return(-1);
}


void net_getpeeraddr(struct net_sock_t *sock, char *databuff) {
  sock = sock;
  databuff = databuff;
}


void net_close(struct net_sock_t *sock) {
  sock = sock;
}
