#!/bin/sh

OUTFILE='../colorize.c'

echo '/* generated automatically by colorize/compile.sh -- do not modify! */' > $OUTFILE
echo '' >> $OUTFILE
echo 'void colorize(uint32_t crc, uint32_t *screenpalette) {' >> $OUTFILE
echo '  uint32_t newpal[16];' >> $OUTFILE
echo '  switch (crc) {' >> $OUTFILE

for i in *.pal
do
  grep -o '^CRC [0-9a-fA-F]*' "$i" | sed 's/CRC /    case 0x/g' | sed 's/$/:/' >> $OUTFILE
  head -n16 "$i" | cut -c1-6 | sed 's/^/0x/g' | tr "\n" "," | sed 's/,$//g' | sed 's/^/      { const uint32_t pal[16] = {/' | sed 's/$/}/' >> $OUTFILE
  echo ';' >> $OUTFILE
  echo '      memcpy(newpal, pal, sizeof(newpal));' >> $OUTFILE
  echo '      break; }' >> $OUTFILE
done

echo '    default: return;' >> $OUTFILE
echo '  }' >> $OUTFILE

cat << EOF >> $OUTFILE
  screenpalette[0] = newpal[0];    /* background */
  screenpalette[1] = newpal[1];    /* background */
  screenpalette[2] = newpal[2];    /* background */
  screenpalette[3] = newpal[3];    /* background */
  screenpalette[32] = newpal[4];   /* window */
  screenpalette[33] = newpal[5];   /* window */
  screenpalette[34] = newpal[6];   /* window */
  screenpalette[35] = newpal[7];   /* window */
  screenpalette[64] = newpal[8];   /* OBJ0 */
  screenpalette[65] = newpal[9];   /* OBJ0 - mario skin */
  screenpalette[66] = newpal[10];  /* OBJ0 - mario pants */
  screenpalette[67] = newpal[11];  /* OBJ0 */
  screenpalette[128] = newpal[12]; /* OBJ1 */
  screenpalette[129] = newpal[13]; /* OBJ1 */
  screenpalette[130] = newpal[14]; /* OBJ1 */
  screenpalette[131] = newpal[15]; /* OBJ1 */
}
EOF

exit 0
