/*
 * Saves the content of ScreenBuffer to OutFile as a 160x144 PCX graphic file.
 * The PCX file is a 16-color, 1-bit, 4-planes image.
 *
 * This file is part of the zBoy emulator project.
 * Copyright (C) Mateusz Viste 2010,2011,2012,2013,2014,2015
 *
 * Note: I could optimise PCX snapshots even further, by limiting the number
 *       of planes when possible (ie. 2 planes for 4 colors and 3 planes for
 *       8 colors), but I found out that modern image viewers usually don't
 *       know how to read such variants. This is why I decided to stick to the
 *       commonly-supported 1bpp x 4 planes model all the time.
 */

#include <stdint.h>
#include <stdio.h>


/* gets a zboy color index, and returns a 0..15 index color */
static int normalizecolor(int x, uint8_t *p) {
  switch (x) {
    case 0:
    case 1:
    case 2:
    case 3:
      return(p[x]);
    case 32: /* -> 4 */
    case 33:
    case 34:
    case 35: /* -> 7 */
      return(p[x - 28]);
    case 64: /* -> 8 */
    case 65:
    case 66:
    case 67: /* -> 11 */
      return(p[x - 56]);
    case 128: /* -> 12 */
    case 129:
    case 130:
    case 131: /* -> 15 */
      return(p[x - 116]);
  }
  return(0);
}


static int fetchnextbyte(int initflag) {
  const int ntozb[16] = {0,1,2,3,32,33,34,35,64,65,66,67,128,129,130,131};
  static int x = 0, y = 0, p = 0; /* 'p' is for 'plane' */
  static uint8_t rpal[16]; /* reduced palette with merged duplicates */
  int res;
  if (initflag != 0) {
    int i, n;
    x = 0;
    y = 0;
    p = 0;
    /* prefill the reduced palette pointers to default values */
    for (i = 0; i < 16; i++) rpal[i] = i;
    /* see if the palette could be optimized to merge some colors together,
     * limiting the number of colors will help the RLE compression later */
    for (i = 1; i < 16; i++) {
      /* check all colors before 'i', one could be the same already */
      for (n = 0; n < i; n++) {
        if (ScreenPalette32[ntozb[i]] == ScreenPalette32[ntozb[n]]) {
          rpal[i] = n;
          break;
        }
      }
    }
    return(0);
  }
  if (x == 160) {
    x = 0;
    p += 1;
    if (p >= 4) {
      p = 0;
      y += 1;
    }
  }
  if (y > 143) {
    x = 0;
    y = 0;
    p = 0;
  }

  /* fill the byte with 8x 1bpp pixels */
  res = (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  res <<= 1;
  res |= (normalizecolor(ScreenBuffer[x++][y], rpal) >> p) & 1;
  return(res);
}


void SavePcxFile(char *outfile) {
  FILE *PcxFileHandler;
  int x, y;
  int RleCounter, OldPixel;
  const int ntozb[16] = {0,1,2,3,32,33,34,35,64,65,66,67,128,129,130,131};
  uint8_t PcxDataHeader[128] = {
    10,     /* PCX ID number (always 0x0a) */
     5,     /* version (0-5) */
     1,     /* encoding format (1=RLE) */
     1,     /* bit depth of a each single plane */
     0,0,   /* xmin */
     0,0,   /* ymin */
   159,0,   /* xmax */
   143,0,   /* ymax */
     0,0,   /* horizontal DPI */
     0,0,   /* vertical DPI */
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 48 bytes of room   */
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* for EGA palette    */
     0,     /* reserved (always 0) */
     4,     /* number of color planes */
    20,0,   /* nb of bytes per line for a single plane */
     1,0,   /* palette type indication (1=color or b/w ; 2=grayscale */
     0,0,   /* HScrSize (optional) */
     0,0,   /* VScrSize (optional) */
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* then come 54 bytes */
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* of reserved bytes  */
     0,0,0,0,0,0                                      /* (always 0)         */
  };

  /* open the snapshot file, quit on failure */
  PcxFileHandler = fopen(outfile, "wb");
  if (PcxFileHandler == NULL) return;

  /* update the 16-color palette */
  for (x = 0; x < 16; x++) {
    PcxDataHeader[16 + x * 3] = (ScreenPalette32[ntozb[x]] >> 16) & 0xff;
    PcxDataHeader[17 + x * 3] = (ScreenPalette32[ntozb[x]] >> 8) & 0xff;
    PcxDataHeader[18 + x * 3] = ScreenPalette32[ntozb[x]] & 0xff;
  }

  /* init the byte fetcher */
  fetchnextbyte(1);

  /* write the PCX header */
  fwrite(PcxDataHeader, 1, 128, PcxFileHandler);

  /* Write PCX image data while RLE-compressing on the fly */
  for (y = 0; y < 144; y++) {
    OldPixel = fetchnextbyte(0);
    RleCounter = 1;
    for (x = 1; x < 80; x++) { /* each RLE line is 4 x 20 bytes */
      int bytebuff = fetchnextbyte(0);
      if ((bytebuff == OldPixel) && (RleCounter < 63)) {
          RleCounter++;
        } else {
          if ((RleCounter > 1) || (OldPixel > 191)) {
            fputc(192 | RleCounter, PcxFileHandler);
          }
          fputc(OldPixel, PcxFileHandler);
          RleCounter = 1;
          OldPixel = bytebuff;
      }
    }
    if ((RleCounter > 1) || (OldPixel > 191)) {
      fputc(192 | RleCounter, PcxFileHandler);
    }
    fputc(OldPixel, PcxFileHandler);
  }

  /* close the snapshot file */
  fclose(PcxFileHandler);
}
