/*
 * save/load config for DOS (Allegro) setup
 * Copyright (C) Mateusz Viste 2015-2019
 */

#include <allegro.h>
#include <stdio.h>

#include "dosconf.h"  /* include self for control */


static int loadint32(unsigned char *buff) {
  int res = 0, i;
  for (i = 0; i < 4; i++) {
    res <<= 8;
    res |= buff[i];
  }
  return(res);
}


static void writeint32(unsigned char *buff, int value) {
  buff[0] = (value >> 24) & 0xff;
  buff[1] = (value >> 16) & 0xff;
  buff[2] = (value >> 8) & 0xff;
  buff[3] = value & 0xff;
}


static void load_default_values(int *videomodel, int *soundmodel, int *joystmodel) {
  if (videomodel != NULL) *videomodel = GFX_AUTODETECT;
  if (soundmodel != NULL) *soundmodel = DIGI_AUTODETECT;
  if (joystmodel != NULL) *joystmodel = JOY_TYPE_4BUTTON;
}


void load_allegro_config(char *file, int *videomodel, int *soundmodel, int *joystmodel) {
  unsigned char buff[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  int sig;
  FILE *fd;
  fd = fopen(file, "rb");
  if (fd == NULL) {
    load_default_values(videomodel, soundmodel, joystmodel);
    return;
  }
  fread(buff, 1, 16, fd);
  fclose(fd);
  sig = loadint32(buff + 12);
  if (sig != 1983) {
    load_default_values(videomodel, soundmodel, joystmodel);
    return;
  }
  if (videomodel != NULL) *videomodel = loadint32(buff);
  if (soundmodel != NULL) *soundmodel = loadint32(buff + 4);
  if (joystmodel != NULL) *joystmodel = loadint32(buff + 8);
}


void save_allegro_config(char *file, int videomodel, int soundmodel, int joystmodel) {
  unsigned char buff[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,191}; /* last 4 bytes contain the signature value (1983) */
  FILE *fd;
  fd = fopen(file, "wb");
  if (fd == NULL) return;
  writeint32(buff, videomodel);
  writeint32(buff + 4, soundmodel);
  writeint32(buff + 8, joystmodel);
  fwrite(buff, 1, 16, fd);
  fclose(fd);
}
