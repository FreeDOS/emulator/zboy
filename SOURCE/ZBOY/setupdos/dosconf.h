/*
 * Routines that save/load allegro settings for the DOS port of zBoy
 * Copyright (C) Mateusz Viste 2015
 */

#ifndef dosconfig_h_sentinel
#define dosconfig_h_sentinel

void load_allegro_config(char *file, int *videomodel, int *soundmodel, int *joystickmodel);
void save_allegro_config(char *file, int videomodel, int soundmodel, int joystickmodel);

#endif
