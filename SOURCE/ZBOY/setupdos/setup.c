/*
 * Setup tool for the DOS version of zBoy.
 * Copyright (C) Mateusz Viste 2015-2019
 *
 * To be compiled with DJGPP
 */

#include <allegro.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>

#include "dosconf.h"

/* define the color scheme */
#define COLOR_TITLE  (2 << 4 | 0)
#define COLOR_BKG    (3 << 4 | 0)
#define COLOR_FRAME  (0 << 4 | 8)
#define COLOR_STATUS (7 << 4 | 0)
#define COLOR_DESC   (0 << 4 | 7)

#define CHAR_LINE_H  196
#define CHAR_LINE_V  179
#define CHAR_CORN_UL 218
#define CHAR_CORN_UR 191
#define CHAR_CORN_BL 192
#define CHAR_CORN_BR 217
#define CHAR_BG      178


struct drvdb {
  int val;
  char *name;
  char *desc;
};


static int drvdb_countentries(struct drvdb *db) {
  int x;
  for (x = 0; db[x].name != NULL; x++);
  return(x);
}


static char *drvdb_getname(struct drvdb *db, int value) {
  int x;
  for (x = 0; db[x].name != NULL; x++) {
    if (db[x].val == value) return(db[x].name);
  }
  return(NULL);
}


static int drvdb_findval(struct drvdb *db, int value) {
  int x;
  for (x = 0; db[x].name != NULL; x++) {
    if (db[x].val == value) return(x);
  }
  return(0);
}


static int drvdb_getmaxnamewidth(struct drvdb *db) {
  int x, len, res = 0;
  for (x = 0; db[x].name != NULL; x++) {
    len = strlen(db[x].name);
    if (len > res) res = len;
  }
  return(res);
}


static void printtitle(char *title) {
  char strbuff[96];
  int lmargin, titlen;
  textattr(COLOR_TITLE);
  memset(strbuff, ' ', 80);
  strbuff[80] = 0;
  titlen = strlen(title);
  lmargin = (80 - titlen) / 2;
  memcpy(strbuff + lmargin, title, titlen);
  gotoxy(1, 1);
  cprintf("%s", strbuff);
}


static void printbackground(void) {
  int x, y;
  textattr(COLOR_BKG);
  for (y = 2; y <= 24; y++) {
    gotoxy(1, y);
    for (x = 0; x < 80; x++) {
      putch(CHAR_BG);
    }
  }
}


static void printstatus(char *msg) {
  char strbuff[96];
  memset(strbuff, ' ', 80);
  memcpy(strbuff, msg, strlen(msg));
  strbuff[80 - 1] = 0;
  gotoxy(1, 25);
  textattr(COLOR_STATUS);
  cprintf("%s", strbuff);
}


static char *wordwrap(char *src, char *res, int maxlen) {
  int x, lastblank = -1;
  if (src == NULL) {
    res[0] = 0;
    return(NULL);
  }
  for (x = 0; x <= maxlen; x++) {
    res[x] = src[x];
    if (src[x] == 0) {
      return(src + x);
    } else if (src[x] == ' ') {
      lastblank = x;
    }
  }
  if (lastblank < 0) {
    res[x] = 0;
    return(src + x);
  } else {
    res[lastblank + 1] = 0;
    return(src + lastblank + 1);
  }
}


static void printdesc(char *desc) {
  int y;
  int descwidth = 62;
  int lmargin;
  char buff1[64], buff2[64];
  lmargin = 1 + (80 - descwidth) / 2;
  /* print the desc frame */
  textattr(COLOR_FRAME);
  gotoxy(lmargin - 1, 17);
  putch(CHAR_CORN_UL);
  memset(buff1, CHAR_LINE_H, descwidth);
  buff1[descwidth] = 0;
  cprintf("%s", buff1);
  putch(CHAR_CORN_UR);
  for (y = 18; y < 23; y++) {
    gotoxy(lmargin - 1, y);
    putch(CHAR_LINE_V);
    gotoxy(descwidth + lmargin, y);
    putch(CHAR_LINE_V);
  }
  gotoxy(lmargin - 1, 23);
  putch(CHAR_CORN_BL);
  memset(buff1, CHAR_LINE_H, descwidth);
  buff1[descwidth] = 0;
  cprintf("%s", buff1);
  putch(CHAR_CORN_BR);
  /* drop in the description now */
  gotoxy(11, 18);
  textattr(COLOR_DESC);
  for (y = 18; y < 23; y++) {
    gotoxy(lmargin, y);
    desc = wordwrap(desc, buff1, descwidth);
    memset(buff2, ' ', descwidth);
    memcpy(buff2, buff1, strlen(buff1));
    buff2[descwidth] = 0;
    cprintf("%s", buff2);
  }
}


/* prints a menu and asks to select a position from the db list */
static int selectpos(struct drvdb *db, int selectedpos, int abortchoice) {
  int menuwidth, menuxpos, i, x;
  int lastkey, itemscount, firstitemdisplayed = 0;
  static int screencleared = 0;
  char strbuff[128];
  int menuheight;
  int menuypos = 3;
  textattr(COLOR_STATUS);
  if (screencleared == 0) {
    screencleared = 1;
    clrscr();
  }
  /* count the number of items and set the menu size accordingly */
  itemscount = drvdb_countentries(db);
  if (itemscount < 11) {
    menuheight = itemscount;
    menuypos = 3 + ((11 - itemscount) / 2);
  } else {
    menuheight = 11;
    menuypos = 3;
  }
  /* print the status line */
  printstatus(" up/down: browse through positions | enter: validate your choice | esc: cancel");
  /* print out the title line */
  printtitle("ZBOY SETUP PROGRAM");
  /* */
  printbackground();
  /* */
  menuwidth = drvdb_getmaxnamewidth(db);
  menuxpos = 1 + (80 - menuwidth) / 2;
  /* draw the selection box */
  gotoxy(menuxpos - 2, menuypos);
  textattr(COLOR_FRAME);
  putch(CHAR_CORN_UL);
  for (x = 0; x < menuwidth + 2; x++) {
    putch(CHAR_LINE_H);
  }
  putch(CHAR_CORN_UR);
  for (x = 0; x < menuheight; x++) {
    gotoxy(menuxpos - 2, menuypos + 1 + x);
    putch(CHAR_LINE_V);
    gotoxy(menuxpos + menuwidth + 1, menuypos + 1 + x);
    putch(CHAR_LINE_V);
  }
  gotoxy(menuxpos - 2, menuypos + 1 + menuheight);
  putch(CHAR_CORN_BL);
  for (x = 0; x < menuwidth + 2; x++) {
    putch(CHAR_LINE_H);
  }
  putch(CHAR_CORN_BR);
  /* start the selection loop */
  for (;;) {
    for (x = 0; x < menuheight; x++) {
      /* adjust the first displayed item, if needed */
      if (firstitemdisplayed > selectedpos) firstitemdisplayed = selectedpos;
      if (selectedpos >= firstitemdisplayed + menuheight) firstitemdisplayed = 1 + selectedpos - menuheight;
      gotoxy(menuxpos - 1, menuypos + 1 + x);
      i = firstitemdisplayed + x;
      if (i == selectedpos) {
        textattr(1 << 4 | 7);
      } else {
        textattr(7);
      }
      memset(strbuff, ' ', menuwidth + 2);
      if (i < itemscount) {
        memcpy(strbuff + 1, db[i].name, strlen(db[i].name));
      } else {
        memset(strbuff + 1, '-', menuwidth);
      }
      strbuff[menuwidth + 2] = 0;
      cprintf("%s", strbuff);
    }
    /* print the description */
    printdesc(db[selectedpos].desc);
    /* read keyboard input and react accordingly */
    lastkey = getch();
    if (lastkey == 0) lastkey = 0x100 | getch();
    switch (lastkey) {
      case 0x0D: /* return */
        return(selectedpos);
        break;
      case 0x1B: /* esc */
        return(abortchoice);
        break;
      case 0x147: /* home */
        selectedpos = 0;
        break;
      case 0x148: /* up */
        if (selectedpos > 0) selectedpos -= 1;
        break;
      case 0x149: /* pgup */
        selectedpos -= menuheight;
        if (selectedpos < 0) selectedpos = 0;
        break;
      case 0x14F: /* end */
        selectedpos = itemscount - 1;
        break;
      case 0x150: /* down */
        if (selectedpos + 1 < itemscount) selectedpos += 1;
        break;
      case 0x151: /* pgdown */
        selectedpos += menuheight;
        if (selectedpos > itemscount - 1) selectedpos = itemscount - 1;
        break;
    }
  }
  return(0);
}


int main(void) {
  int cfgvideo, cfgsound, cfgjoyst;
  int mainmenupos = 0;
  int x;
  char cfgvideostr[64];
  char cfgsoundstr[64];
  char cfgjoyststr[64];
  struct drvdb dbjoy[] = {
    {JOY_TYPE_AUTODETECT,    "Autodetect", "Attempts to autodetect your joystick hardware. It isn't possible to reliably distinguish between all the possible input setups, so this can only ever detect the standard joystick, Sidewider, GamePad Pro, or GrIP, hence you are advised to select the exact hardware you have."},
    {JOY_TYPE_STANDARD,      "Standard 2 buttons", "A normal two button stick."},
    {JOY_TYPE_4BUTTON,       "Standard 4 buttons", "Enable the extra buttons on a 4-button joystick."},
    {JOY_TYPE_6BUTTON,       "Standard 6 buttons", "Enable the extra buttons on a 6-button joystick."},
    {JOY_TYPE_8BUTTON,       "Standard 8 buttons", "Enable the extra buttons on a 8-button joystick."},
    {JOY_TYPE_FSPRO,         "CH FlightStick Pro", "CH Flightstick Pro or compatible stick, which provides four buttons, an analogue throttle control, and a 4-direction coolie hat."},
    {JOY_TYPE_WINGEX,        "Logitech Wingman Extreme", "A Logitech Wingman Extreme, which should also work with any Thrustmaster Mk.I compatible joystick. Supports four buttons and a coolie hat. Also works with the Wingman Warrior, if you plug in the 15 pin plug (remember to unplug the 9-pin plug!) and set the tiny switch in front to the 'H' position."},
    {JOY_TYPE_SIDEWINDER,    "Microsoft Sidewinder", "The Microsoft Sidewinder digital pad (supports up to four controllers, each with ten buttons and a digital direction control)."},
    {JOY_TYPE_SIDEWINDER_AG, "Microsoft Sidewinder (alt)", "An alternative driver to 'Sidewinder'. Try this if your Sidewinder pad isn't functionning with 'Sidewinder'."},
    {JOY_TYPE_SIDEWINDER_PP, "Microsoft Sidewinder 3D", "The Microsoft Sidewinder 3D/Precision/Force Feedback Pro joysticks."},
    {JOY_TYPE_GAMEPAD_PRO,   "Gravis GamePad Pro", "The Gravis GamePad Pro (supports up to two controllers, each with ten buttons and a digital direction control)."},
    {JOY_TYPE_GRIP4,         "Gravis GrIP", "Gravis GrIP driver"},
    {JOY_TYPE_SNESPAD_LPT1,  "SNES PAD on LPT1", "SNES joypad connected to a parallel (LPT) port."},
    {JOY_TYPE_SNESPAD_LPT2,  "SNES PAD on LPT2", "SNES joypad connected to a parallel (LPT) port."},
    {JOY_TYPE_SNESPAD_LPT3,  "SNES PAD on LPT3", "SNES joypad connected to a parallel (LPT) port."},
    {JOY_TYPE_PSXPAD_LPT1,   "PSX PAD on LPT1", "PSX joypad connected to a parallel (LPT) port. See http://www.ziplabel.com/dpadpro/index.html for information about the parallel cable required. The driver automatically detects the type of PSX pad (digital, analog red/green mode, NegCon, multi taps, Namco light guns, Jogcons and the mouse)."},
    {JOY_TYPE_PSXPAD_LPT2,   "PSX PAD on LPT2", "PSX joypad connected to a parallel (LPT) port. See http://www.ziplabel.com/dpadpro/index.html for information about the parallel cable required. The driver automatically detects the type of PSX pad (digital, analog red/green mode, NegCon, multi taps, Namco light guns, Jogcons and the mouse)."},
    {JOY_TYPE_PSXPAD_LPT3,   "PSX PAD on LPT3", "PSX joypad connected to a parallel (LPT) port. See http://www.ziplabel.com/dpadpro/index.html for information about the parallel cable required. The driver automatically detects the type of PSX pad (digital, analog red/green mode, NegCon, multi taps, Namco light guns, Jogcons and the mouse)."},
    {JOY_TYPE_N64PAD_LPT1,   "N64 PAD on LPT1", "N64 joypad connected to a parallel (LPT) port. See http://www.st-hans.de/N64.htm for information about the necessary hardware adaptor."},
    {JOY_TYPE_N64PAD_LPT2,   "N64 PAD on LPT1", "N64 joypad connected to a parallel (LPT) port. See http://www.st-hans.de/N64.htm for information about the necessary hardware adaptor."},
    {JOY_TYPE_N64PAD_LPT3,   "N64 PAD on LPT1", "N64 joypad connected to a parallel (LPT) port. See http://www.st-hans.de/N64.htm for information about the necessary hardware adaptor."},
    {JOY_TYPE_TURBOGRAFX_LPT1, "TurboGraFX joystick on LPT1", "A TurboGraFX joystick connected to a parallel (LPT) port. This driver uses the TurboGraFX interface by Steffen Schwenke. See http://www.burg-halle.de/~schwenke/parport.html for details on how to build this."},
    {JOY_TYPE_TURBOGRAFX_LPT2, "TurboGraFX joystick on LPT2", "A TurboGraFX joystick connected to a parallel (LPT) port. This driver uses the TurboGraFX interface by Steffen Schwenke. See http://www.burg-halle.de/~schwenke/parport.html for details on how to build this."},
    {JOY_TYPE_TURBOGRAFX_LPT3, "TurboGraFX joystick on LPT3", "A TurboGraFX joystick connected to a parallel (LPT) port. This driver uses the TurboGraFX interface by Steffen Schwenke. See http://www.burg-halle.de/~schwenke/parport.html for details on how to build this."},
    {JOY_TYPE_WINGWARRIOR,   "Wingman Warrior", "A Wingman Warrior joystick."},
    {0, NULL, NULL}  /* last entry must be set to NULLs */
  };

  struct drvdb dbvid[] = {
    {GFX_AUTODETECT, "Autodetect", "Let the program auto-detect the best video driver for your hardware."},
    {GFX_VGA,    "VGA", "The standard 256-color VGA mode 13h. This is limited to a resolution of 320x200, which will work on any VGA."},
    {GFX_VESA1,  "VESA v1.x", "VESA v1.x driver. You should use newer VESA drivers if your video adapter supports them."},
    {GFX_VESA2B, "VESA v2.0 (banked mode)", "VESA v2.x driver running in banked mode."},
    {GFX_VESA2L, "VESA v2.0 (linear mode)", "VESA v2.x driver running in linear mode."},
    {GFX_VBEAF,  "VBE/AF", "VBE/AF is a superset of the VBE 2.0 standard, which provides an API for accessing hardware accelerator features. VBE/AF drivers can give dramatic speed improvements when used with suitable hardware."},
    {GFX_VESA3,  "VESA v3.0", "Use the VBE 3.0 driver. The standard VESA modes are 640x480, 800x600, and 1024x768. These ought to work with any SVGA card: if they don't, get a copy of the SciTech Display Doctor and see if that fixes it."},
    {0, NULL, NULL}  /* last entry must be set to NULLs */
  };

  struct drvdb dbsnd[] = {
    {DIGI_AUTODETECT, "Autodetect", "Tries to autodetect your sound hardware."},
    {DIGI_NONE, "None", "No sound"},
    {DIGI_SB10, "SoundBlaster 1.0", "SB 1.0 (8-bit mono single shot DMA)"},
    {DIGI_SB15, "SoundBlaster 1.5", "SB 1.5 (8-bit mono single shot DMA)"},
    {DIGI_SB20, "SoundBlaster 2.0", "SB 2.0 (8-bit mono auto-initialised DMA)"},
    {DIGI_SBPRO, "SoundBlaster Pro", "SB Pro (8-bit stereo)"},
    {DIGI_SB16, "SoundBlaster 16", "SB16 (16-bit stereo)"},
    {DIGI_AUDIODRIVE, "ESS AudioDrive", NULL},
    {DIGI_SOUNDSCAPE, "Ensoniq Soundscape", NULL},
    {DIGI_WINSOUNDSYS, "Windows Sound System", NULL},
    {0, NULL, NULL}  /* last entry must be set to NULLs */
  };

  struct drvdb dbmenu[] = {
    {0, NULL, "Select the video driver appropriate to your hardware configuration. Most of the time auto-detection works fine."},
    {0, NULL, "Select your sound system"},
    {0, NULL, "Joystick configuration - you are advised to select the model that is closest to your actual joystick."},
    {0, "Exit and save", "Saves the new configuration and quits this setup utility to go back to the operating system."},
    {0, NULL, NULL}  /* last entry must be set to NULLs */
  };

  x = 0;
  dbmenu[x++].name = cfgvideostr;
  dbmenu[x++].name = cfgsoundstr;
  dbmenu[x++].name = cfgjoyststr;

  /* load current configuration values */
  load_allegro_config("setup.cfg", &cfgvideo, &cfgsound, &cfgjoyst);

  /* hide the blinking cursor */
  _setcursortype(_NOCURSOR);

  for (;;) {
    /* update main menu entries */
    sprintf(cfgvideostr, "Video card: %s", drvdb_getname(dbvid, cfgvideo));
    sprintf(cfgsoundstr, "Sound card: %s", drvdb_getname(dbsnd, cfgsound));
    sprintf(cfgjoyststr, "Joystick..: %s", drvdb_getname(dbjoy, cfgjoyst));
    /* call the menuing system */
    mainmenupos = selectpos(dbmenu, mainmenupos, -1);
    if (mainmenupos == 0) {
      x = drvdb_findval(dbvid, cfgvideo);
      cfgvideo = dbvid[selectpos(dbvid, x, x)].val;
    } else if (mainmenupos == 1) {
      x = drvdb_findval(dbsnd, cfgsound);
      cfgsound = dbsnd[selectpos(dbsnd, x, x)].val;
    } else if (mainmenupos == 2) {
      x = drvdb_findval(dbjoy, cfgjoyst);
      cfgjoyst = dbjoy[selectpos(dbjoy, x, x)].val;
    } else if (mainmenupos == 3) {
      /* save new config and quit */
      save_allegro_config("setup.cfg", cfgvideo, cfgsound, cfgjoyst);
      break;
    } else if (mainmenupos == -1) { /* cancel */
      break;
    }
  }

  textattr(7);
  clrscr();

  return(0);
}
