/*
    Z80 Timer emulation
    This file is part of the zBoy project.
    Copyright (C) Mateusz Viste 2010,2011,2012,2013


   FF05 - TIMA - Timer counter (R/W)
          This timer is incremented by a clock frequency
          specified by the TAC register ($FF07). When the
          value overflows (gets bigger than FFh) then it
          will be reset to the value specified in TMA
          (FF06), and an interrupt will be requested.

   FF07 - TAC - Timer Control (R/W)
          Bit 2 - Timer Stop
            0: Stop Timer
            1: Start Timer
          Bits 1+0 - Input Clock Select
            00: 4.096 KHz
            01: 262.144 Khz
            10: 65.536 KHz
            11: 16.384 KHz
*/


int TimerC = 0;     /* Timer counter */


static inline void CheckTIMA(void) {
  if (IoRegisters[0xFF05] == 255) {
      IoRegisters[0xFF05] = IoRegisters[0xFF06];
      INT(INT_TIMA);  /* Request an interrupt via bit 2 of the IF (if bit 2 enabled in IE) */
    } else {
      IoRegisters[0xFF05] += 1;
  }
}


inline static void uTimer(unsigned int cycles) {  /* Timer emulation */
  int v;
  static const int timing[4] = {1024, 16, 64, 256}; /* in order: 4096 Hz, 262144 Hz, 65536 Hz, 16384 Hz */
  if ((IoRegisters[0xFF07] & bx00000100) == 0) return; /* test TAC bit 2 (start/stop timer) */
  /* timer is enabled, so run it */
  v = timing[IoRegisters[0xFF07] & bx00000011];   /* bits 1+0 of FF07 -> input clock select */
  TimerC += cycles;
  while (TimerC >= v) {
    TimerC -= v;
    CheckTIMA();
  }
}
