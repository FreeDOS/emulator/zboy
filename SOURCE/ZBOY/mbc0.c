/*
 * --------------------------------------------------------------------------
 * This file is part of the zBoy project.
 * Copyright (C) Mateusz Viste 2010-2019
 * --------------------------------------------------------------------------
 *
 * ROM-ONLY support
 *  - no switchable ROM banks, everything is mapped hard-wired at 0...7FFF.
 *  - no switchable RAM banks, the 0xA000...0xBFFF area may or may not be
 *    linked to hard-wired RAM cells (RAM on such carts is unlikely, but
 *    technically it's possible some did contain a few KiB of RAM)
 */


static uint8_t MBC0_MemoryRead(int memaddr) {
  if (memaddr < 0x8000) {        /* ROM memory [0000-7FFF] */
    return(MemoryROM[memaddr]);
  } else {                       /* else I don't know what you want (RAM?) */
    return(MemoryMAP[memaddr]);
  }
}


static void MBC0_MemoryWrite(int memaddr, uint8_t DataByte) {
  /* allow for a tiny amount of hard-wired cartridge RAM
   * I do not know if any MBC0 cart ever had such on-board RAM, but I assume
   * that handling it won't harm anything anyway. */
  if ((memaddr >= 0xA000) && (memaddr < 0xC000)) MemoryMAP[memaddr] = DataByte;
}
