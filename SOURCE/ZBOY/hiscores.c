/*
 *  This file is part of the zBoy project.
 *  Copyright (C) 2010-2019 Mateusz Viste
 */


/* provides an offset range 'from-to' where hiscore data is stored for a given game */
uint8_t *GetHiScoresOffsets(unsigned int *offset1, unsigned int *offset2) {
  switch (RomInfos.CrcSum) {
    case 0x51300CFD:  /* Crystal Quest */
      *offset1 = 0xC500;
      *offset2 = 0xC537;
      break;
    case 0x0509069C:  /* Pac-Man */
    case 0xB681E243:  /* Pac-Man (another version) */
      *offset1 = 0xFF8C;
      *offset2 = 0xFF8E;
      break;
    case 0xB3A86164:  /* Hyper Lode Runner */
      *offset1 = 0xFFA7;
      *offset2 = 0xFFA9;
      break;
    case 0x0E5BB1C4:  /* Ms. Pac-Man */
      *offset1 = 0xD8C7;
      *offset2 = 0xD8C9;
      break;
    case 0xF59CEDEA:  /* Pipe Dream */
      *offset1 = 0xC212;
      *offset2 = 0xC22C;
      break;
    case 0x90776841:  /* Super Marioland v1.0 */
    case 0x2C27EC70:  /* Super Marioland v1.1 */
      *offset1 = 0xC0C0;
      *offset2 = 0xC0C3;
      break;
    case 0x63F9407D:  /* Tetris v1.0 */
    case 0x46DF91AD:  /* Tetris v1.1 */
      *offset1 = 0xD000;
      *offset2 = 0xD761;
      break;
    case 0xC1F88833:  /* Asteroids */
      *offset1 = 0xDB00;
      *offset2 = 0xDB53;
      break;
    case 0xA37A814A:  /* BattleCity */
      *offset1 = 0xCA02;
      *offset2 = 0xCA06;
      break;
    case 0xB76C769B:  /* Tetris Attack v1.0 */
      *offset1 = 0xCF00;
      *offset2 = 0xCF2C;
      break;
    case 0xBFB112AD:  /* Quarth */
      *offset1 = 0xC419;
      *offset2 = 0xC41B;
      break;
    case 0xC5ACCE7C:  /* Tail Gator */
      *offset1 = 0xC165;
      *offset2 = 0xC167;
      break;
    case 0x6C742478:  /* Dig Dug */
      *offset1 = 0xDE08;
      *offset2 = 0xDE17;
      break;
    case 0xA2F30B4B:  /* Flipul */
      *offset1 = 0xC131;
      *offset2 = 0xC137;
      break;
    case 0x6A6ECFEC:  /* Galaga & Galaxian */
      *offset1 = 0xD70D;
      *offset2 = 0xD716;
      break;
    case 0x11817103:  /* SolarStriker */
      *offset1 = 0xCFF1;
      *offset2 = 0xCFF7;
      break;
    default:  /* not found */
      return(NULL);
      break;
  }
  /* return the RAM segment where the offset lies */
  if (*offset1 >= 0xFF00) return(MemoryInternalHiRAM);
  return(MemoryInternalRAM);
}


#define XorKey 1983


void SaveHiScoresMem(void) {
  unsigned int UintVar, offset1, offset2;
  uint8_t *memarea;
  uint32_t HiScoreCheckSum = 1985;
  FILE *FileHandler;
  char SaveDir[1024];
  char SaveScoresFile[1040];
  drv_getsavedir(SaveDir, sizeof(SaveDir));  /* provides the SaveDir variable */
  snprintf(SaveScoresFile, sizeof(SaveScoresFile), "%s%08X.hi", SaveDir, (unsigned int)RomInfos.CrcSum);
  memarea = GetHiScoresOffsets(&offset1, &offset2);
  if (memarea != NULL) {
    /* open file */
    FileHandler = fopen(SaveScoresFile, "wb");
    if (FileHandler == NULL) return;
    /* write memory dump and compute checksum */
    for (UintVar = offset1; UintVar <= offset2; UintVar++) {
      fputc(memarea[UintVar], FileHandler);
      HiScoreCheckSum += memarea[UintVar];
    }
    /* write the checksum value into the file */
    HiScoreCheckSum ^= XorKey;
    fputc((HiScoreCheckSum >> 24) & 0xFF, FileHandler);
    fputc((HiScoreCheckSum >> 16) & 0xFF, FileHandler);
    fputc((HiScoreCheckSum >> 8) & 0xFF, FileHandler);
    fputc(HiScoreCheckSum & 0xFF, FileHandler);
    /* close the file */
    fclose(FileHandler);
  }
}


void LoadHiScoresMem(void) {
  unsigned int UintVar, offset1, offset2;
  unsigned int HiScoreCheckSum = 1985, SavedCheckSum;
  char SaveDir[1024];
  char SaveScoresFile[1040];
  uint8_t *memarea;
  FILE *FileHandler;
  drv_getsavedir(SaveDir, sizeof(SaveDir));  /* provides the SaveDir variable */
  snprintf(SaveScoresFile, sizeof(SaveScoresFile), "%s%08X.hi", SaveDir, (unsigned int)RomInfos.CrcSum);
  memarea = GetHiScoresOffsets(&offset1, &offset2);
  if (memarea != NULL) {
    /* open file */
    FileHandler = fopen(SaveScoresFile, "rb");
    if (FileHandler == NULL) return;
    /* write-protect memory and compute checksum */
    for (UintVar = offset1; UintVar <= offset2; UintVar++) {
      memarea[UintVar] = fgetc(FileHandler);
      SaveScoresWriteProtection[UintVar - offset1] = UintVar;
      HiScoreCheckSum += memarea[UintVar];
    }
    /* check if hiscores haven't been modified by a cheater */
    SavedCheckSum = ((unsigned int) fgetc(FileHandler) << 24);
    SavedCheckSum |= ((unsigned int) fgetc(FileHandler) << 16);
    SavedCheckSum |= ((unsigned int) fgetc(FileHandler) << 8);
    SavedCheckSum |= (unsigned int) fgetc(FileHandler);
    SavedCheckSum ^= XorKey;
    if (HiScoreCheckSum != SavedCheckSum) {
      printf("Cksum mismatch on hiscores!\n");
      for (UintVar = 0; UintVar < 2048; UintVar++) SaveScoresWriteProtection[UintVar] = 0;
    }
    /* close file */
    fclose(FileHandler);
  }
}
