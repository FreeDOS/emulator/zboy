/*
    Part of the zBoy project
    Copyright (C) Mateusz Viste 2010, 2011, 2012
*/

#include <stdint.h>
#include <string.h>
#include "binary.h"


void LoadRomInfos(struct RomInformations *RomInfos) {
  char TempString[1024];
  int x, y, z;
  uint8_t NewLicenseeCode;
  uint8_t *MemoryROM = RomInfos->MemoryROM_PTR;

  memcpy(TempString, &MemoryROM[0x134], 16);

  x = 0;
  while ((TempString[x] > 31) && (TempString[x] < 127) && (x < 16)) x++;
  TempString[x] = 0;   /* Trim all zeroed bytes at the end, as well as high bytes (occuring on new-style licenses) */
  strcpy(RomInfos->Title, TempString);

  /* Now let's see if we can fix the title in any possible way... */

  switch (RomInfos->CrcSum) {  /* Look for well known CRC32 to provide a good title */
    case 0x00CD1876:  /* Bram's Stoker Dracula */
      strcpy(RomInfos->NiceTitle, "Bram Stoker's Dracula");
      break;
    case 0x1D0F279C:  /* 2nd Space (Sachen) */
      strcpy(RomInfos->NiceTitle, "2nd Space");
      break;
    case 0x3D4E1779:  /* Black Forest Tale (Sachen) */
      strcpy(RomInfos->NiceTitle, "Black Forest Tale");
      break;
    case 0x3F47ED05:  /* Dan Laser */
      strcpy(RomInfos->NiceTitle, "Dan Laser");
      break;
    case 0x66130A7A:  /* Worm Visitor (Sachen) */
      strcpy(RomInfos->NiceTitle, "Worm Visitor");
      break;
    case 0x6B4FEFB0:  /* Captain Knick-Knack (Sachen) */
      strcpy(RomInfos->NiceTitle, "Captain Knick-Knack");
      break;
    case 0x776D03DC:  /* A-Force (Sachen) */
      strcpy(RomInfos->NiceTitle, "A-Force");
      break;
    case 0x88190730:  /* Zen Intergalactic Ninja */
      strcpy(RomInfos->NiceTitle, "Zen Intergalactic Ninja");
      break;
    case 0xB1ACBD28:  /* Magic Maze (Sachen) */
      strcpy(RomInfos->NiceTitle, "Magic Maze");
      break;
    case 0xB3A86164:  /* Hyper Lode Runner */
      strcpy(RomInfos->NiceTitle, "Hyper Lode Runner");
      break;
    case 0xB4A5936E:  /* Ant Soldiers (Sachen) */
      strcpy(RomInfos->NiceTitle, "Ant Soldiers");
      break;
    case 0xB5B38860:  /* Attack of the Killer Tomatoes */
      strcpy(RomInfos->NiceTitle, "Attack of the Killer Tomatoes");
      break;
    case 0xC0EF39F0:  /* Track & Field */
      strcpy(RomInfos->NiceTitle, "Track & Field");
      break;
    case 0xCD6E3136:  /* Crazy Burger (Sachen) */
      strcpy(RomInfos->NiceTitle, "Crazy Burger");
      break;
    case 0xD25F3B3C:  /* Duck Adventures (Sachen) */
      strcpy(RomInfos->NiceTitle, "Duck Adventures");
      break;
    case 0xF157AE9E:  /* Magical Tower (Sachen) */
      strcpy(RomInfos->NiceTitle, "Magical Tower");
      break;
    case 0xF82D7223:  /* Robocop Vs. Terminator */
      strcpy(RomInfos->NiceTitle, "Robocop vs. The Terminator");
      break;
    default:  /* Look for mistyped titles... */
      if (strcmp(RomInfos->Title, "") == 0) {
        strcpy(RomInfos->NiceTitle, "Unknown game");
      } else if (strcmp(RomInfos->Title, "ADVENTUREISLAND2") == 0) {
        strcpy(RomInfos->NiceTitle, "Adventure Island II");
      } else if (strcmp(RomInfos->Title, "ARTHUR AFD") == 0) {
        strcpy(RomInfos->NiceTitle, "Arthur's Absolutely Fun Day!");
      } else if (strcmp(RomInfos->Title, "ASTEROIDS/MISCMD") == 0) {
        strcpy(RomInfos->NiceTitle, "Asteroids & Missile Command");
      } else if (strcmp(RomInfos->Title, "BEAVIS & BUTTHE") == 0) {
        strcpy(RomInfos->NiceTitle, "Beavis & Butt-Head");
      } else if (strcmp(RomInfos->Title, "BATMAN ROJ") == 0) {
        strcpy(RomInfos->NiceTitle, "Batman - Return of the Joker");
      } else if (strcmp(RomInfos->Title, "BONKSREVENGE") == 0) {
        strcpy(RomInfos->NiceTitle, "Bonk's Revenge");
      } else if (strcmp(RomInfos->Title, "BUBBLEBOBBLE2") == 0) {
        strcpy(RomInfos->NiceTitle, "Bubble Bobble 2");
      } else if (strcmp(RomInfos->Title, "CASTLEVANIA ADVE") == 0) {
        strcpy(RomInfos->NiceTitle, "The Castlevania Adventure");
      } else if (strcmp(RomInfos->Title, "CONTRA ALIEN WAR") == 0) {
        strcpy(RomInfos->NiceTitle, "Contra - The Alien Wars");
      } else if (strcmp(RomInfos->Title, "DARK WING DUCK") == 0) {
        strcpy(RomInfos->NiceTitle, "Darkwing Duck");
      } else if (strcmp(RomInfos->Title, "DONKEYKONGLAND 2") == 0) {
        strcpy(RomInfos->NiceTitle, "Donkey Kong Land 2");
      } else if (strcmp(RomInfos->Title, "DONKEYKONGLAND95") == 0) {
        strcpy(RomInfos->NiceTitle, "Donkey Kong Land 95");
      } else if (strcmp(RomInfos->Title, "ELMO IN GROAELE") == 0) {
        strcpy(RomInfos->NiceTitle, "Elmo in Grouchland");
      } else if (strcmp(RomInfos->Title, "EVEL KNIEVLAEVE") == 0) {
        strcpy(RomInfos->NiceTitle, "Evel Knievel");
      } else if (strcmp(RomInfos->Title, "FIFA 2000") == 0) {
        strcpy(RomInfos->NiceTitle, "FIFA 2000");
      } else if (strcmp(RomInfos->Title, "GALAGA&GALAXIAN ") == 0) {
        strcpy(RomInfos->NiceTitle, "Galaga & Galaxian");
      } else if (strcmp(RomInfos->Title, "GBWARS2") == 0) {
        strcpy(RomInfos->NiceTitle, "Gameboy Wars 2");
      } else if (strcmp(RomInfos->Title, "GHOSTSNGOBLAG9E") == 0) {
        strcpy(RomInfos->NiceTitle, "Ghosts'N Goblins");
      } else if (strcmp(RomInfos->Title, "HELLO KITTYAF4E") == 0) {
        strcpy(RomInfos->NiceTitle, "Hello Kitty Cube Frenzy");
      } else if (strcmp(RomInfos->Title, "JURASSIC PARK II") == 0) {
        strcpy(RomInfos->NiceTitle, "Jurassic Park II");
      } else if (strcmp(RomInfos->Title, "KYORCHANLANDHIRO") == 0) {
        strcpy(RomInfos->NiceTitle, "Kyorchan Land");
      } else if (strcmp(RomInfos->Title, "LAZLOS LEAP") == 0) {
        strcpy(RomInfos->NiceTitle, "Lazlo's Leap");
      } else if (strcmp(RomInfos->Title, "MAGICMUSEUMAHKJ") == 0) {
        strcpy(RomInfos->NiceTitle, "Hello Kitty Magical Museum");
      } else if (strcmp(RomInfos->Title, "NASCARFASTTRACKS") == 0) {
        strcpy(RomInfos->NiceTitle, "Nascar Fast Tracks");
      } else if (strcmp(RomInfos->Title, "PA BINGO") == 0) {
        strcpy(RomInfos->NiceTitle, "Panel Action Bingo");
      } else if (strcmp(RomInfos->Title, "PAC-MAN SCEAACE") == 0) {
        strcpy(RomInfos->NiceTitle, "Pac-Man Special Color Edition");
      } else if (strcmp(RomInfos->Title, "PAINTERMOMOPIE") == 0) {
        strcpy(RomInfos->NiceTitle, "Painter Momo Pie");
      } else if (strcmp(RomInfos->Title, "PERSONAL ORGANIZ") == 0) {
        strcpy(RomInfos->NiceTitle, "Personal Organizer");
      } else if (strcmp(RomInfos->Title, "PINBALL FANTASIE") == 0) {
        strcpy(RomInfos->NiceTitle, "Pinball Fantasies");
      } else if (strcmp(RomInfos->Title, "POKE MISSION97") == 0) {
        strcpy(RomInfos->NiceTitle, "Poke Mission 97");
      } else if (strcmp(RomInfos->Title, "POKEBOM") == 0) {
        strcpy(RomInfos->NiceTitle, "Pocket Bomberman");
      } else if (strcmp(RomInfos->Title, "PUZZLEX") == 0) {
        strcpy(RomInfos->NiceTitle, "Puzzle X");
      } else if (strcmp(RomInfos->Title, "SPY VS SPY") == 0) {
        strcpy(RomInfos->NiceTitle, "Spy vs Spy");
      } else if (strcmp(RomInfos->Title, "SUPERMARIOLAND3") == 0) {
        strcpy(RomInfos->NiceTitle, "Warioland - Super Mario Land 3");
      } else if (strcmp(RomInfos->Title, "TETRIS DX") == 0) {
        strcpy(RomInfos->NiceTitle, "Tetris DX");
      } else if (strcmp(RomInfos->Title, "TIGER_A_DRAGON") == 0) {
        strcpy(RomInfos->NiceTitle, "Tiger A Dragon");
      } else if (strcmp(RomInfos->Title, "TINY BUSTERBUSE") == 0) {
        strcpy(RomInfos->NiceTitle, "Tiny Toons - Buster Saves the Day");
      } else if (strcmp(RomInfos->Title, "TINY TOON 2 MMM") == 0) {
        strcpy(RomInfos->NiceTitle, "Tiny Toon Adventures 2 - Montana's Movie Madness");
      } else if (strcmp(RomInfos->Title, "TINY TOON ADVENT") == 0) {
        strcpy(RomInfos->NiceTitle, "Tiny Toon Adventures");
      } else if (strcmp(RomInfos->Title, "TINY TOON WACKY") == 0) {
        strcpy(RomInfos->NiceTitle, "Tiny Toon Adventures - Wacky Sports");
      } else if (strcmp(RomInfos->Title, "WEREBACK BC") == 0) {
        strcpy(RomInfos->NiceTitle, "We're Back!");
      } else if (strcmp(RomInfos->Title, "WILDSNAKE") == 0) {
        strcpy(RomInfos->NiceTitle, "Wild Snake");
      } else if (strcmp(RomInfos->Title, "WORDZAP") == 0) {
        strcpy(RomInfos->NiceTitle, "WordZap");
      } else {   /* Do whatever you can to make the title a bit nicer to look at... */
        y = 0;
        z = 0;
        TempString[0] = 0;
        TempString[1] = 'A';   /* Small caps / big caps flag */
        while (RomInfos->Title[y] != 0) y++;   /* y is the length of the string */
        for (x = 0; x < y; x++) {
          /* Add a space after every dot which is not followed by a space (but not if the dot is the last char of the string) */
          if (x < (y - 1)) {
            if ((RomInfos->Title[x] == '.') || (RomInfos->Title[x] == ',')) {
              if (RomInfos->Title[x+1] != ' ') TempString[0] = ' ';
            }
          }
          /* Use upper case chars only for first char or chars following a space or dash */
          if (z > 0) {
            switch (RomInfos->NiceTitle[z - 1]) {
              case ' ':
              case '-':
                TempString[1] = 'A';
                break;
              default:
                TempString[1] = 'a';
                break;
            }
          }
          RomInfos->NiceTitle[z] = RomInfos->Title[x];
          if (TempString[1] == 'A') { /* Big caps */
            if ((RomInfos->Title[x] >= 97) && (RomInfos->Title[x] <= 122)) RomInfos->NiceTitle[z] -= 32;
          } else {  /* Small caps */
            if ((RomInfos->Title[x] >= 65) && (RomInfos->Title[x] <= 90)) RomInfos->NiceTitle[z] += 32;
          }
          z++;
          if (TempString[0] == ' ') {
            TempString[0] = 0;
            RomInfos->NiceTitle[z] = ' ';
            z++;
          }
        }
        RomInfos->NiceTitle[z] = 0; /* terminate the string */
      }
      break;
  }

  if (MemoryROM[0x14B] == 0x33) {    /* New-style license */
      NewLicenseeCode = ((MemoryROM[0x144] & bx11110000) | (MemoryROM[0x145] & bx00001111));
      switch (NewLicenseeCode) {
        default:
          sprintf(RomInfos->Licensee, "unknown [%02Xh/%02Xh -> %02Xh]", MemoryROM[0x144], MemoryROM[0x145], NewLicenseeCode);
          break;
      }
    } else {   /* Old style license */
      switch (MemoryROM[0x14B]) {  /* Get licensee code from 014Bh */
        case 0x00:
          strcpy(RomInfos->Licensee, "none");
          break;
        case 0x01:
          strcpy(RomInfos->Licensee, "Nintendo");
          break;
        case 0x08:
          strcpy(RomInfos->Licensee, "Capcom");
          break;
        case 0x09:
          strcpy(RomInfos->Licensee, "HOT-B");
          break;
        case 0x0A:
          strcpy(RomInfos->Licensee, "Jaleco");
          break;
        case 0x0B:
          strcpy(RomInfos->Licensee, "Coconuts");
          break;
        case 0x0C:
          strcpy(RomInfos->Licensee, "Elite Systems");
          break;
        case 0x13:
          strcpy(RomInfos->Licensee, "Electronic Arts");
          break;
        case 0x18:
          strcpy(RomInfos->Licensee, "Hudson Soft");
          break;
        case 0x19:
          strcpy(RomInfos->Licensee, "ITC Entertainment");
          break;
        case 0x1A:
          strcpy(RomInfos->Licensee, "Yanoman");
          break;
        case 0x1D:
          strcpy(RomInfos->Licensee, "Clary");
          break;
        case 0x1F:
          strcpy(RomInfos->Licensee, "Virgin");
          break;
        case 0x24:
          strcpy(RomInfos->Licensee, "PCM Complete");
          break;
        case 0x25:
          strcpy(RomInfos->Licensee, "San-X");
          break;
        case 0x28:
          strcpy(RomInfos->Licensee, "Kotobuki Systems");
          break;
        case 0x29:
          strcpy(RomInfos->Licensee, "Seta");
          break;
        case 0x30:
          strcpy(RomInfos->Licensee, "Infogrames");
          break;
        case 0x31:
          strcpy(RomInfos->Licensee, "Nintendo");
          break;
        case 0x32:
          strcpy(RomInfos->Licensee, "Bandai");
          break;
        case 0x34:
          strcpy(RomInfos->Licensee, "Konami");
          break;
        case 0x35:
          strcpy(RomInfos->Licensee, "Hector");
          break;
        case 0x38:
          strcpy(RomInfos->Licensee, "Capcom");
          break;
        case 0x39:
          strcpy(RomInfos->Licensee, "Banpresto");
          break;
        case 0x3C:
          strcpy(RomInfos->Licensee, "Entertainment I (?)");
          break;
        case 0x3E:
          strcpy(RomInfos->Licensee, "Gremlin");
          break;
        case 0x41:
          strcpy(RomInfos->Licensee, "Ubi Soft");
          break;
        case 0x42:
          strcpy(RomInfos->Licensee, "Atlus");
          break;
        case 0x44:
          strcpy(RomInfos->Licensee, "Malibu");
          break;
        case 0x46:
          strcpy(RomInfos->Licensee, "Angel");
          break;
        case 0x47:
          strcpy(RomInfos->Licensee, "Spectrum Holoby");
          break;
        case 0x49:
          strcpy(RomInfos->Licensee, "Irem");
          break;
        case 0x4A:
          strcpy(RomInfos->Licensee, "Virgin");
          break;
        case 0x4D:
          strcpy(RomInfos->Licensee, "Malibu");
          break;
        case 0x4F:
          strcpy(RomInfos->Licensee, "U.S. Gold");
          break;
        case 0x50:
          strcpy(RomInfos->Licensee, "Absolute");
          break;
        case 0x51:
          strcpy(RomInfos->Licensee, "Acclaim");
          break;
        case 0x52:
          strcpy(RomInfos->Licensee, "Activision");
          break;
        case 0x53:
          strcpy(RomInfos->Licensee, "American Sammy");
          break;
        case 0x54:
          strcpy(RomInfos->Licensee, "Gametek");
          break;
        case 0x55:
          strcpy(RomInfos->Licensee, "Park Place");
          break;
        case 0x56:
          strcpy(RomInfos->Licensee, "LJN");
          break;
        case 0x57:
          strcpy(RomInfos->Licensee, "Matchbox");
          break;
        case 0x59:
          strcpy(RomInfos->Licensee, "Milton Bradley");
          break;
        case 0x5A:
          strcpy(RomInfos->Licensee, "Mindscape");
          break;
        case 0x5B:
          strcpy(RomInfos->Licensee, "Romstar");
          break;
        case 0x5C:
          strcpy(RomInfos->Licensee, "Naxat Soft");
          break;
        case 0x5D:
          strcpy(RomInfos->Licensee, "Tradewest");
          break;
        case 0x60:
          strcpy(RomInfos->Licensee, "Titus");
          break;
        case 0x61:
          strcpy(RomInfos->Licensee, "Virgin");
          break;
        case 0x67:
          strcpy(RomInfos->Licensee, "Ocean");
          break;
        case 0x69:
          strcpy(RomInfos->Licensee, "Electronic Arts");
          break;
        case 0x6E:
          strcpy(RomInfos->Licensee, "Elite Systems");
          break;
        case 0x6F:
          strcpy(RomInfos->Licensee, "Electro Brain");
          break;
        case 0x70:
          strcpy(RomInfos->Licensee, "Infogrames");
          break;
        case 0x71:
          strcpy(RomInfos->Licensee, "Interplay");
          break;
        case 0x72:
          strcpy(RomInfos->Licensee, "Broderbund");
          break;
        case 0x73:
          strcpy(RomInfos->Licensee, "Sculptered Soft");
          break;
        case 0x75:
          strcpy(RomInfos->Licensee, "The Sales Curve");
          break;
        case 0x78:
          strcpy(RomInfos->Licensee, "T*HQ");
          break;
        case 0x79:
          strcpy(RomInfos->Licensee, "Accolade");
          break;
        case 0x7A:
          strcpy(RomInfos->Licensee, "Triffix Entertainment");
          break;
        case 0x7C:
          strcpy(RomInfos->Licensee, "Microprose");
          break;
        case 0x7F:
          strcpy(RomInfos->Licensee, "Kemco");
          break;
        case 0x80:
          strcpy(RomInfos->Licensee, "Misawa Entertainment");
          break;
        case 0x83:
          strcpy(RomInfos->Licensee, "Lozc");
          break;
        case 0x86:
          strcpy(RomInfos->Licensee, "Tokuma Shoten I (?)");
          break;
        case 0x8B:
          strcpy(RomInfos->Licensee, "Bullet-Proof Software");
          break;
        case 0x8C:
          strcpy(RomInfos->Licensee, "Vic Tokai");
          break;
        case 0x8E:
          strcpy(RomInfos->Licensee, "Ape");
          break;
        case 0x8F:
          strcpy(RomInfos->Licensee, "I'Max");
          break;
        case 0x91:
          strcpy(RomInfos->Licensee, "Chun Soft");
          break;
        case 0x92:
          strcpy(RomInfos->Licensee, "Video System");
          break;
        case 0x93:
          strcpy(RomInfos->Licensee, "Tsuburava");
          break;
        case 0x95:
          strcpy(RomInfos->Licensee, "Varie");
          break;
        case 0x96:
          strcpy(RomInfos->Licensee, "Yonezawa/s'pal");
          break;
        case 0x97:
          strcpy(RomInfos->Licensee, "Kaneko");
          break;
        case 0x99:
          strcpy(RomInfos->Licensee, "Arc");
          break;
        case 0x9A:
          strcpy(RomInfos->Licensee, "Nihon Bussan");
          break;
        case 0x9B:
          strcpy(RomInfos->Licensee, "Tecmo");
          break;
        case 0x9C:
          strcpy(RomInfos->Licensee, "Imagineer");
          break;
        case 0x9D:
          strcpy(RomInfos->Licensee, "Banpresto");
          break;
        case 0x9F:
          strcpy(RomInfos->Licensee, "Nova");
          break;
        case 0xA1:
          strcpy(RomInfos->Licensee, "Hori Electric");
          break;
        case 0xA2:
          strcpy(RomInfos->Licensee, "Bandai");
          break;
        case 0xA4:
          strcpy(RomInfos->Licensee, "Konami");
          break;
        case 0xA6:
          strcpy(RomInfos->Licensee, "Kawada");
          break;
        case 0xA7:
          strcpy(RomInfos->Licensee, "Takara");
          break;
        case 0xA9:
          strcpy(RomInfos->Licensee, "Technos Japan");
          break;
        case 0xAA:
          strcpy(RomInfos->Licensee, "Broderbund");
          break;
        case 0xAC:
          strcpy(RomInfos->Licensee, "Toei Animation");
          break;
        case 0xAD:
          strcpy(RomInfos->Licensee, "Toho");
          break;
        case 0xAF:
          strcpy(RomInfos->Licensee, "Namco");
          break;
        case 0xB0:
          strcpy(RomInfos->Licensee, "Acclaim");
          break;
        case 0xB1:
          strcpy(RomInfos->Licensee, "Ascii / Nexoft");
          break;
        case 0xB2:
          strcpy(RomInfos->Licensee, "Bandai");
          break;
        case 0xB4:
          strcpy(RomInfos->Licensee, "Enix");
          break;
        case 0xB6:
          strcpy(RomInfos->Licensee, "Hal");
          break;
        case 0xB7:
          strcpy(RomInfos->Licensee, "SNK");
          break;
        case 0xB9:
          strcpy(RomInfos->Licensee, "Pony Canyon");
          break;
        case 0xBA:
          strcpy(RomInfos->Licensee, "Culture Brain O (?)");
          break;
        case 0xBB:
          strcpy(RomInfos->Licensee, "Sunsoft");
          break;
        case 0xBD:
          strcpy(RomInfos->Licensee, "Sony Imagesoft");
          break;
        case 0xBF:
          strcpy(RomInfos->Licensee, "Sammy");
          break;
        case 0xC0:
          strcpy(RomInfos->Licensee, "Taito");
          break;
        case 0xC2:
          strcpy(RomInfos->Licensee, "Kemco");
          break;
        case 0xC3:
          strcpy(RomInfos->Licensee, "Squaresoft");
          break;
        case 0xC4:
          strcpy(RomInfos->Licensee, "Tokuma Shoten I (?)");
          break;
        case 0xC5:
          strcpy(RomInfos->Licensee, "Data East");
          break;
        case 0xC6:
          strcpy(RomInfos->Licensee, "Tonkin House");
          break;
        case 0xC8:
          strcpy(RomInfos->Licensee, "Koei");
          break;
        case 0xC9:
          strcpy(RomInfos->Licensee, "UFL");
          break;
        case 0xCA:
          strcpy(RomInfos->Licensee, "Ultra");
          break;
        case 0xCB:
          strcpy(RomInfos->Licensee, "Vap");
          break;
        case 0xCC:
          strcpy(RomInfos->Licensee, "Use");
          break;
        case 0xCD:
          strcpy(RomInfos->Licensee, "Meldac");
          break;
        case 0xCE:
          strcpy(RomInfos->Licensee, "Pony Canyon Or (?)");
          break;
        case 0xCF:
          strcpy(RomInfos->Licensee, "Angel");
          break;
        case 0xD0:
          strcpy(RomInfos->Licensee, "Taito");
          break;
        case 0xD1:
          strcpy(RomInfos->Licensee, "Sofel");
          break;
        case 0xD2:
          strcpy(RomInfos->Licensee, "Quest");
          break;
        case 0xD3:
          strcpy(RomInfos->Licensee, "Sigma Enterprises");
          break;
        case 0xD4:
          strcpy(RomInfos->Licensee, "Ask Kodansha");
          break;
        case 0xD6:
          strcpy(RomInfos->Licensee, "Naxat Soft");
          break;
        case 0xD7:
          strcpy(RomInfos->Licensee, "Copya Systems");
          break;
        case 0xD9:
          strcpy(RomInfos->Licensee, "Banpresto");
          break;
        case 0xDA:
          strcpy(RomInfos->Licensee, "Tomy");
          break;
        case 0xDB:
          strcpy(RomInfos->Licensee, "LJN");
          break;
        case 0xDD:
          strcpy(RomInfos->Licensee, "NCS");
          break;
        case 0xDE:
          strcpy(RomInfos->Licensee, "Human");
          break;
        case 0xDF:
          strcpy(RomInfos->Licensee, "Altron");
          break;
        case 0xE0:
          strcpy(RomInfos->Licensee, "Jaleco");
          break;
        case 0xE1:
          strcpy(RomInfos->Licensee, "Towachiki");
          break;
        case 0xE2:
          strcpy(RomInfos->Licensee, "Uutaka");
          break;
        case 0xE3:
          strcpy(RomInfos->Licensee, "Varie");
          break;
        case 0xE5:
          strcpy(RomInfos->Licensee, "Epoch");
          break;
        case 0xE7:
          strcpy(RomInfos->Licensee, "Athena");
          break;
        case 0xE8:
          strcpy(RomInfos->Licensee, "Asmik");
          break;
        case 0xE9:
          strcpy(RomInfos->Licensee, "Natsume");
          break;
        case 0xEA:
          strcpy(RomInfos->Licensee, "King Records");
          break;
        case 0xEB:
          strcpy(RomInfos->Licensee, "Atlus");
          break;
        case 0xEC:
          strcpy(RomInfos->Licensee, "Epic / Sony Records");
          break;
        case 0xEE:
          strcpy(RomInfos->Licensee, "IGS");
          break;
        case 0xF0:
          strcpy(RomInfos->Licensee, "A Wave");
          break;
        case 0xF3:
          strcpy(RomInfos->Licensee, "Extreme Entertainment");
          break;
        case 0xFF:
          strcpy(RomInfos->Licensee, "LJN");
          break;
        default:
          sprintf(RomInfos->Licensee, "unknown [%02Xh]", MemoryROM[0x14B]);
          break;
      }
  }

  /* Check bit 7 of the CGB flag (&80 usually means that the game is GB compatible) */
  if ((MemoryROM[0x143] & 128) > 0) RomInfos->ColorGB = 1;
  if (MemoryROM[0x143] == 0xC0) RomInfos->ColorGB = 2;  /* GBC flag + not compatible with older GB */
  if (MemoryROM[0x146] == 0x03) RomInfos->SuperGB = 1;

  switch (MemoryROM[0x147]) {   /* Get the cartridge type */
    case 0x00:
      sprintf(RomInfos->CartTypeString, "ROM ONLY");
      RomInfos->MbcModel = 0;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x01:
      sprintf(RomInfos->CartTypeString, "ROM+MBC1");
      RomInfos->MbcModel = 1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x02:
      sprintf(RomInfos->CartTypeString, "ROM+MBC1+RAM");
      RomInfos->MbcModel = 1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x03:
      sprintf(RomInfos->CartTypeString, "ROM+MBC1+RAM+BATT");
      RomInfos->MbcModel = 1;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x05:
      sprintf(RomInfos->CartTypeString, "ROM+MBC2");
      RomInfos->MbcModel = 2;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x06:
      sprintf(RomInfos->CartTypeString, "ROM+MBC2+BATT");
      RomInfos->MbcModel = 2;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x08:
      sprintf(RomInfos->CartTypeString, "ROM+RAM");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x09:
      sprintf(RomInfos->CartTypeString, "ROM+RAM+BATT");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x0B:
      sprintf(RomInfos->CartTypeString, "ROM+MMMO1");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x0C:
      sprintf(RomInfos->CartTypeString, "ROM+MMMO1+SRAM");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x0D:
      sprintf(RomInfos->CartTypeString, "ROM+MMMO1+SRAM+BATT");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x0F:
      sprintf(RomInfos->CartTypeString, "ROM+MBC3+TIMER+BATT");
      RomInfos->MbcModel = 3;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 1;
      break;
    case 0x10:
      sprintf(RomInfos->CartTypeString, "ROM+MBC3+TIMER+RAM+BATT");
      RomInfos->MbcModel = 3;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 1;
      break;
    case 0x11:
      sprintf(RomInfos->CartTypeString, "ROM+MBC3");
      RomInfos->MbcModel = 3;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x12:
      sprintf(RomInfos->CartTypeString, "ROM+MBC3+RAM");
      RomInfos->MbcModel = 3;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x13:
      sprintf(RomInfos->CartTypeString, "ROM+MBC3+RAM+BATT");
      RomInfos->MbcModel = 3;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x19:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1A:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5+RAM");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1B:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5+RAM+BATT");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1C:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5+RUMBLE");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 1;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1D:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5+RUMBLE+SRAM");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 1;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1E:
      sprintf(RomInfos->CartTypeString, "ROM+MBC5+RUMBLE+SRAM+BATT");
      RomInfos->MbcModel = 5;
      RomInfos->Battery = 1;
      RomInfos->Rumble = 1;
      RomInfos->TimerRTC = 0;
      break;
    case 0x1F:
      sprintf(RomInfos->CartTypeString, "Pocket Camera");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0xFD:
      sprintf(RomInfos->CartTypeString, "Bandai TAMA5");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0xFE:
      sprintf(RomInfos->CartTypeString, "Hudson HuC-3");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    case 0xFF:
      sprintf(RomInfos->CartTypeString, "Hudson HuC-1");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
    default:
      sprintf(RomInfos->CartTypeString, "Unknown cartridge type");
      RomInfos->MbcModel = -1;
      RomInfos->Battery = 0;
      RomInfos->Rumble = 0;
      RomInfos->TimerRTC = 0;
      break;
  }

  switch (MemoryROM[0x148]) {
    case 0x00: /* 256 Kbit (2 banks) */
      RomInfos->RomBanks = 2;
      RomInfos->RomAddrTrim = 1;
      break;
    case 0x01: /* 512 Kbit (4 banks) */
      RomInfos->RomBanks = 4;
      RomInfos->RomAddrTrim = 3;
      break;
    case 0x02: /* 1 Mbit (8 banks) */
      RomInfos->RomBanks = 8;
      RomInfos->RomAddrTrim = 7;
      break;
    case 0x03: /* 2 Mbit (16 banks) */
      RomInfos->RomBanks = 16;
      RomInfos->RomAddrTrim = 15;
      break;
    case 0x04: /* 4 Mbit (32 banks) */
      RomInfos->RomBanks = 32;
      RomInfos->RomAddrTrim = 31;
      break;
    case 0x05: /* 8 Mbit (64 banks) */
      RomInfos->RomBanks = 64;
      RomInfos->RomAddrTrim = 63;
      break;
    case 0x06: /* 16 Mbit (128 banks) */
      RomInfos->RomBanks = 128;
      RomInfos->RomAddrTrim = 127;
      break;
    case 0x52: /* 9 Mbit (72 banks) */
      RomInfos->RomBanks = 72;
      RomInfos->RomAddrTrim = 127;
      break;
    case 0x53: /* 10 Mbit (80 banks) */
      RomInfos->RomBanks = 80;
      RomInfos->RomAddrTrim = 127;
      break;
    case 0x54: /* 12 Mbit (96 banks) */
      RomInfos->RomBanks = 96;
      RomInfos->RomAddrTrim = 127;
      break;
  }
  RomInfos->RomSize = RomInfos->RomBanks * 16384;

  switch (MemoryROM[0x149]) {
    case 0: /* no RAM */
      RomInfos->RamSize = 0;
      RomInfos->RamAddrTrim = 0;
      break;
    case 1: /* 16 kBit (2KiB, 1 bank) */
      RomInfos->RamSize = 2048;
      RomInfos->RamAddrTrim = 0;
      break;
    case 2: /* 64 kBit (8KiB, 1 bank) */
      RomInfos->RamSize = 8192;
      RomInfos->RamAddrTrim = 0;
      break;
    case 3: /* 256 kBit (32 KiB, 4 banks) */
      RomInfos->RamSize = 32768;
      RomInfos->RamAddrTrim = 3;
      break;
    case 4: /* 1 MBit (128 KiB, 16 banks) */
      RomInfos->RamSize = 131072;
      RomInfos->RamAddrTrim = 15;
      break;
  }

  /* All MBC2 chips contain a 512x4 bits internal RAM, although the byte at 149h always indicates 0 bytes of RAM. */
  if (RomInfos->MbcModel == 2) RomInfos->RamSize = 512;

  if (MemoryROM[0x14A] == 0) {
    RomInfos->Destination = 0;
  } else {
    RomInfos->Destination = 1;
  }

}
