/*
 * This file is part of zBoy.
 * Copyright (C) Mateusz Viste 2013. All rights reserved.
 *
 * It is a simple library providing functions to unzip files from zip archives.
 */


#ifndef libunzip_sentinel
#define libunzip_sentinel

#include <time.h>  /* required for the time_t definition */

#define ZIP_FLAG_ISADIR    1
#define ZIP_FLAG_ENCRYPTED 2

struct ziplist {
  char *filename;
  long filelen;
  long compressedfilelen;
  short compmethod;
  unsigned long crc32;
  long dataoffset;  /* the offset in the file where the compressed data starts */
  char flags;      /* zero for files, non-zero for directories */
  time_t timestamp; /* the timestamp of the file */
  struct ziplist *nextfile;
};

struct ziplist *zip_listfiles(FILE *fd);
int zip_unzip(FILE *zipfd, struct ziplist *curzipnode, char *fulldestfilename, void *memptr);
void zip_freelist(struct ziplist **ziplist);

#endif
