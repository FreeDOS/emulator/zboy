/*
 * This function provides a few operations to handle zip files.
 * Copyright (C) Mateusz Viste 2012, 2013
 */


#include <stdio.h>     /* printf(), FILE, fclose()... */
#include <stdlib.h>    /* NULL */
#include <string.h>    /* memset() */
#include <time.h>      /* mktime() */
#include <utime.h>     /* utime() */
#include "crc32.h"
#include "lzmadec.h"   /* LZMA support */
#include "tinfl.h"     /* DEFLATE support */
#include "libunzip.h"  /* include self for control */


/* converts a "DOS format" timestamp into unix timestamp. The DOS timestamp is constructed an array of 4 bytes, that contains following data at the bit level:
 * HHHHHMMM MMMSSSSS YYYYYYYM MMMDDDDD
 *  where:
 * day of month is always within 1-31 range;
 * month is always within 1-12 range;
 * year starts from 1980 and continues for 127 years
 * seconds are actually not 0-59 but rather 0-29 as there are only 32 possible values – to get actual seconds multiply this field by 2;
 * minutes are always within 0-59 range;
 * hours are always within 0-23 range.     */
static time_t dostime2unix(unsigned char *buff) {
  struct tm curtime;
  time_t result;
  memset(&curtime, 0, sizeof(curtime)); /* make sure to set everything in curtime to 0's */
  curtime.tm_sec = (buff[0] & 31) << 1; /* seconds (0..60) */
  curtime.tm_min = (((buff[1] << 8) | buff[0]) >> 5) & 63 ; /* minutes after the hour (0..59) */
  curtime.tm_hour = (buff[1] >> 3); /* hours since midnight (0..23) */
  curtime.tm_mday = buff[2] & 31; /* day of the month (1..31) */
  curtime.tm_mon = ((((buff[3] << 8) | buff[2]) >> 5) & 15) - 1; /* months since January (0, 11) */
  curtime.tm_year = (buff[3] >> 1) + 80; /* years since 1900 */
  curtime.tm_wday = 0; /* days since Sunday (0..6) - leave 0, mktime() will set it */
  curtime.tm_yday = 0; /* days since January 1 (0..365]) - leave 0, mktime() will set it */
  curtime.tm_isdst = -1; /* Daylight Saving Time flag. Positive if DST is in effect, zero if not and negative if no information is available */
  result = mktime(&curtime);
  if (result == (time_t)-1) return(0);
  return(result);
}


/* this is a wrapper on malloc(), used as a callback by lzmadec */
static void *SzAlloc(void *p, size_t size) {
  p = p; /* for gcc to not complain */
  if (size == 0) return(0);
  return(malloc(size));
}

/* this is a wrapper on free(), used as a callback by lzmadec */
static void SzFree(void *p, void *address) {
  p = p;  /* for gcc to not complain */
  free(address);
}


/* used to write the uncompressed stream into either a file or memory buffer,
 * if working on memory buffer, it increments it.
 * returns the amount of bytes written */
static int write_to_file_or_mem(FILE *dstfd, unsigned char **dstbuff, void *srcmem, int len) {
  if (dstfd != NULL) {
    return(fwrite(srcmem, 1, len, dstfd));
  } else {
    memcpy(*dstbuff, srcmem, len);
    *dstbuff += len;
    return(len);
  }
}


/* opens a zip file and provides the list of files in the archive.
   returns a pointer to a ziplist (linked list) with all records, or NULL on error.
   The ziplist is allocated automatically, and must be freed via zip_freelist. */
struct ziplist *zip_listfiles(FILE *fd) {
  struct ziplist *reslist = NULL;
  struct ziplist *newentry;
  unsigned int entrysig;
  unsigned int filenamelen, extrafieldlen, filecommentlen, compfilelen;
  int x, bytebuff, eofflag, centraldirectoryfound = 0;
  unsigned int ux;
  unsigned char hdrbuff[64];

  rewind(fd);  /* make sure the file cursor is at the very beginning of the file */

  for (;;) { /* read entry after entry */
    entrysig = 0;
    eofflag = 0;
    /* read the entry signature first */
    for (x = 0; x < 4; x++) {
      if ((bytebuff = fgetc(fd)) == EOF) {
        eofflag = 1;
        break;
      }
      entrysig |= (bytebuff << (x * 8));
    }
    if (eofflag != 0) break;
    /* printf("sig: 0x%08x\n", entrysig); */
    if (entrysig == 0x04034b50) { /* local file */
        unsigned int generalpurposeflags;
        /* create new entry and link it into the list */
        newentry = malloc(sizeof(struct ziplist));
        if (newentry == NULL) {
          zip_freelist(&reslist);
          break;
        }
        newentry->filename = NULL;
        newentry->nextfile = reslist;
        newentry->flags = 0;
        reslist = newentry;
        /* parse header now */
        fread(hdrbuff, 1, 26, fd);
        generalpurposeflags = hdrbuff[3];  /* parse the general */
        generalpurposeflags <<= 8;         /* purpose flags and */
        generalpurposeflags |= hdrbuff[2]; /* save them for later */
        newentry->compmethod = hdrbuff[4] | (hdrbuff[5] << 8);
        newentry->timestamp = dostime2unix(&hdrbuff[6]);
        newentry->crc32 = hdrbuff[10] | (hdrbuff[11] << 8) | (hdrbuff[12] << 16) | (hdrbuff[13] << 24);
        newentry->compressedfilelen = hdrbuff[14] | (hdrbuff[15] << 8) | (hdrbuff[16] << 16) | (hdrbuff[17] << 24);
        newentry->filelen = hdrbuff[18] | (hdrbuff[19] << 8) | (hdrbuff[20] << 16) | (hdrbuff[21] << 24);
        filenamelen = hdrbuff[22] | (hdrbuff[23] << 8);
        extrafieldlen = hdrbuff[24] | (hdrbuff[25] << 8);
        /* printf("Filename len: %d / extrafield len: %d / Compfile len: %d\n", filenamelen, extrafieldlen, compfilelen); */
        newentry->filename = calloc(filenamelen + 1, 1); /* alloc space for filename */
        if (newentry->filename == NULL) {
          zip_freelist(&reslist);
          break;
        }
        /* check general purpose flags */
        if ((generalpurposeflags & 1) != 0) newentry->flags |= ZIP_FLAG_ENCRYPTED;
        /* parse the filename */
        for (ux = 0; ux < filenamelen; ux++) newentry->filename[ux] = fgetc(fd); /* store filename */
        if (newentry->filename[filenamelen - 1] == '/') newentry->flags |= ZIP_FLAG_ISADIR; /* if filename ends with / it's a dir. Note that ZIP forbids the usage of '\' in ZIP paths anyway */
        /* printf("Filename: %s (%ld bytes compressed)\n", newentry->filename, newentry->compressedfilelen); */
        newentry->dataoffset = ftell(fd) + extrafieldlen;
        /* skip rest of fields and data */
        fseek(fd, (extrafieldlen + newentry->compressedfilelen), SEEK_CUR);
      } else if (entrysig == 0x02014b50) { /* central directory */
        centraldirectoryfound = 1;
        /* parse header now */
        fread(hdrbuff, 1, 42, fd);
        filenamelen = hdrbuff[22] | (hdrbuff[23] << 8);
        extrafieldlen = hdrbuff[24] | (hdrbuff[25] << 8);
        filecommentlen = hdrbuff[26] | (hdrbuff[27] << 8);
        compfilelen = hdrbuff[14] | (hdrbuff[15] << 8) | (hdrbuff[16] << 16) | (hdrbuff[17] << 24);
        /* printf("central dir\n"); */
        /* skip rest of fields and data */
        fseek(fd, (filenamelen + extrafieldlen + compfilelen + filecommentlen), SEEK_CUR);
      } else if (entrysig == 0x08074b50) { /* Data descriptor header */
        /* no need to read the header we just have to skip it */
        fseek(fd, 12, SEEK_CUR); /* the header is 3x4 bytes (CRC + compressed len + uncompressed len) */
      } else { /* unknown sig */
        zip_freelist(&reslist);
        break;
    }
  }
  /* if we got no central directory record, the file is incomplete */
  if (centraldirectoryfound == 0) zip_freelist(&reslist);
  return(reslist);
}



/* unzips a file. zipfd points to the open zip file, curzipnode to the entry to extract, and fulldestfilename is the destination file where to unzip it. returns 0 on success, non-zero otherwise. */
int zip_unzip(FILE *zipfd, struct ziplist *curzipnode, char *fulldestfilename, void *memptr) {
  #define buffinsize 32 * 1024   /* the input buffer must be at least 32K, because that's the (usual) dic size in deflate, apparently */
  #define buffoutsize 128 * 1024 /* it's better for the output buffer to be significantly larger than the input buffer (we are decompressing here, remember? */
  FILE *filefd = NULL;
  unsigned long cksum;
  int extract_res;
  unsigned char *buffout;
  unsigned char *buffin;
  struct utimbuf filetimestamp;
  unsigned char *membuf = memptr;

  /* first of all, check we support the compression method */
  switch (curzipnode->compmethod) {
    case 0:  /* stored */
    case 8:  /* deflated */
    case 14: /* lzma */
      break;
    default: /* unsupported compression method, sorry */
      return(-1);
      break;
  }

  /* open the dst file */
  if (fulldestfilename != NULL) {
    filefd = fopen(fulldestfilename, "wb");
    if (filefd == NULL) return(-2);  /* failed to open the dst file */
  }

  /* allocate buffers for data I/O */
  buffin = malloc(buffinsize);
  buffout = malloc(buffoutsize);
  if ((buffin == NULL) || (buffout == NULL)) {
    if (buffin != NULL) free(buffin);
    if (buffout != NULL) free(buffout);
    if (filefd != NULL) fclose(filefd);
    return(-6);
  }

  fseek(zipfd, curzipnode->dataoffset, SEEK_SET); /* set the reading position inside the zip file */
  extract_res = -255;

  cksum = crc32_init(); /* init the crc32 */

  if (curzipnode->compmethod == 0) { /* if the file is stored, copy it over */
      unsigned long i, toread;
      extract_res = 0;   /* assume we will succeed */
      for (i = 0; (signed)i < curzipnode->filelen;) {
        toread = curzipnode->filelen - i;
        if (toread > buffoutsize) toread = buffoutsize;
        if (fread(buffout, toread, 1, zipfd) != 1) extract_res = -3;   /* read a chunk of data */
        crc32_feed(&cksum, buffout, toread); /* update the crc32 checksum */
        /* write data chunk to dst file */
        /* if (fwrite(buffout, toread, 1, filefd) != 1) extract_res = -4; OLD */
        if (write_to_file_or_mem(filefd, &membuf, buffout, toread) != (signed)toread) extract_res = -4;
        i += toread;
      }
    } else if (curzipnode->compmethod == 8) {  /* if the file is deflated, inflate it */
      size_t total_in = 0, total_out = 0;
      unsigned int infile_remaining = curzipnode->compressedfilelen;
      size_t avail_in = 0;
      tinfl_decompressor *tinflhandler;
      size_t avail_out = buffoutsize;
      void *next_out = buffout;
      const void *next_in = buffin;

      extract_res = 0; /* assume we will succeed */
      tinflhandler = malloc(sizeof(tinfl_decompressor));
      if (tinflhandler == NULL) {
          extract_res = -19;
        } else {
          tinfl_init(tinflhandler);
      }

      while (extract_res == 0) {
         size_t in_bytes, out_bytes;
         tinfl_status status;
         if (!avail_in) {
            /* Input buffer is empty, so read more bytes from input file. */
            unsigned int n = buffinsize;
            if (n > infile_remaining) n = infile_remaining;
            fread(buffin, 1, n, zipfd);
            next_in = buffin;
            avail_in = n;
            infile_remaining -= n;
         }

         in_bytes = avail_in;
         out_bytes = avail_out;
         status = tinfl_decompress(tinflhandler, (const mz_uint8 *)next_in, &in_bytes, buffout, (mz_uint8 *)next_out, &out_bytes, (infile_remaining ? TINFL_FLAG_HAS_MORE_INPUT : 0));

         avail_in -= in_bytes;
         next_in = (const mz_uint8 *)next_in + in_bytes;
         total_in += in_bytes;

         avail_out -= out_bytes;
         next_out = (mz_uint8 *)next_out + out_bytes;
         total_out += out_bytes;

         if ((status <= TINFL_STATUS_DONE) || (!avail_out)) {
            /* Output buffer is full, or decompression is done, so write buffer to output file. */
            unsigned int n = buffoutsize - (unsigned int)avail_out;
            /* fwrite(buffout, 1, n, filefd); OLD */
            write_to_file_or_mem(filefd, &membuf, buffout, n);
            crc32_feed(&cksum, buffout, n); /* update the crc32 checksum */
            next_out = buffout;
            avail_out = buffoutsize;
         }

         /* If status is <= TINFL_STATUS_DONE then either decompression is done or something went wrong. */
         if (status <= TINFL_STATUS_DONE) {
            if (status == TINFL_STATUS_DONE) { /* Decompression completed successfully. */
                extract_res = 0;
              } else {  /* Decompression failed. */
                extract_res = -15;
            }
            break;
         }
      }
      if (tinflhandler != NULL) free(tinflhandler);

    } else if (curzipnode->compmethod == 14) {  /* LZMA */
      long bytesread, bytesreadtotal = 0, byteswritetotal = 0;
      SizeT buffoutreslen;
      ISzAlloc g_alloc;
      ELzmaStatus lzmastatus;
      SRes lzmaresult;
      CLzmaDec lzmahandle;
      unsigned char lzmahdr[LZMA_PROPS_SIZE]; /* 5 bytes of properties */

      extract_res = -5; /* assume we will fail. if we don't - then we will update this flag */

      fread(lzmahdr, 4, 1, zipfd); /* load the 4 bytes long 'zip-lzma header */
      bytesreadtotal = 4; /* remember we read 4 bytes already */

      /* lzma properties should be exactly 5 bytes long. If it's not, it's either not valid lzma, or some version that wasn't existing yet when I wrote these words. Also, check that the lzma content is at least 9 bytes long and that our previous malloc() calls suceeded. */
      if ((lzmahdr[2] == 5) && (lzmahdr[3] == 0) && (curzipnode->compressedfilelen >= 9)) {

        extract_res = 0;  /* since we got so far, let's assume we will succeed now */

        g_alloc.Alloc = SzAlloc; /* these will be used as callbacks by lzma to manage memory */
        g_alloc.Free = SzFree;

        fread(lzmahdr, sizeof(lzmahdr), 1, zipfd); /* load the lzma header */
        bytesreadtotal += sizeof(lzmahdr);

        /* Note, that in a 'normal' lzma stream we would have now 8 bytes with the uncompressed length of the file. Here we don't. ZIP cut this information out, since it stores it already in its own header. */

        memset(&lzmahandle, 0, sizeof(lzmahandle)); /* reset the whole lzmahandle structure - not doing this leads to CRASHES!!! */
        LzmaDec_Init(&lzmahandle);
        lzmaresult = LzmaDec_Allocate(&lzmahandle, lzmahdr, LZMA_PROPS_SIZE, &g_alloc); /* forget not to LzmaDec_Free() later! */
        if (lzmaresult != 0) extract_res = -13;

        while (extract_res == 0) {
          bytesread = buffinsize;
          if (bytesread > curzipnode->compressedfilelen - bytesreadtotal) bytesread = curzipnode->compressedfilelen - bytesreadtotal;
          buffoutreslen = buffoutsize;
          /* printf("Will read %d bytes from input stream\n", bytesread); */
          fread(buffin, bytesread, 1, zipfd); /* read stuff from input stream */
          fseek(zipfd, 0 - bytesread, SEEK_CUR); /* get back to the position at the start of our chunk of data */
          lzmaresult = LzmaDec_DecodeToBuf(&lzmahandle, buffout, &buffoutreslen, buffin, (SizeT *)&bytesread, LZMA_FINISH_ANY, &lzmastatus);
          bytesreadtotal += bytesread;
          /* printf("expanded %ld bytes into %ld (total read: %ld bytes)\n", (long)bytesread, (long)buffoutreslen, (long)bytesreadtotal); */
          fseek(zipfd, bytesread, SEEK_CUR); /* go forward to the position next to the input we processed */
          if (lzmaresult != SZ_OK) {
            extract_res = -20;
            if (lzmaresult == SZ_ERROR_DATA) extract_res = -21;        /* DATA ERROR */
            if (lzmaresult == SZ_ERROR_MEM) extract_res = -22;         /* MEMORY ALLOC ERROR */
            if (lzmaresult == SZ_ERROR_UNSUPPORTED) extract_res = -23; /* UNSUPPORTED PROPERTY */
            if (lzmaresult == SZ_ERROR_INPUT_EOF) extract_res = -24;   /* NEED MORE INPUT */
            break;
          }
          /* check that we haven't got TOO MUCH decompressed data, and trim if necessary. It happens that LZMA provides a few bytes more than it should at the end of the stream. */
          if (byteswritetotal + (long)buffoutreslen > curzipnode->filelen) {
            buffoutreslen = curzipnode->filelen - byteswritetotal;
          }
          byteswritetotal += buffoutreslen;
          /* write stuff to output file */
          /* fwrite(buffout, buffoutreslen, 1, filefd); OLD */
          write_to_file_or_mem(filefd, &membuf, buffout, buffoutreslen);
          crc32_feed(&cksum, buffout, buffoutreslen);
          /* if (lzmastatus == LZMA_STATUS_FINISHED_WITH_MARK) puts("lzma says we are done!"); */
          if ((lzmastatus == LZMA_STATUS_FINISHED_WITH_MARK) || (bytesreadtotal >= curzipnode->compressedfilelen)) {
            extract_res = 0; /* looks like we succeeded! */
            break;
          }
        }
        LzmaDec_Free(&lzmahandle, &g_alloc); /* this will free all the stuff we allocated via LzmaDec_Allocate() */
        /* printf("Processed %d bytes of input into %d bytes of output. CRC32: %08lX\n", bytesreadtotal, byteswritetotal, crc32); */
      }
  }

  /* clean up stuff */
  if (buffin != NULL) free(buffin);
  if (buffout != NULL) free(buffout);
  if (filefd != NULL) fclose(filefd);  /* close the dst file, if used */
  crc32_finish(&cksum);

  /* printf("extract_res=%d / cksum_expected=%08lX / cksum_obtained=%08lX\n", extract_res, curzipnode->crc32, cksum); */
  if (extract_res != 0) return(extract_res);   /* was the extraction process successful? */
  if (cksum != curzipnode->crc32) return(-9); /* is the crc32 ok after extraction? */
  /* Set the timestamp of the new file to what was set in the zip file */
  filetimestamp.actime  = curzipnode->timestamp;
  filetimestamp.modtime = curzipnode->timestamp;
  utime(fulldestfilename, &filetimestamp);
  return(0);
}



/* Call this to free a ziplist computed by zip_listfiles() */
void zip_freelist(struct ziplist **ziplist) {
  struct ziplist *zipentrytobefreed;
  while (*ziplist != NULL) { /* iterate through the linked list and free all nodes */
    zipentrytobefreed = *ziplist;
    *ziplist = zipentrytobefreed->nextfile;
    /* free memory storing the filename (if allocated) */
    if (zipentrytobefreed->filename != NULL) free(zipentrytobefreed->filename);
    /* free the linked list entry itself */
    free(zipentrytobefreed);
  }
  *ziplist = NULL;
}
