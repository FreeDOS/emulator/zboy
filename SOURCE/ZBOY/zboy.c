/*
 * zBoy - A multiplatform, open-source GameBoy emulator
 * Copyright (C) Mateusz Viste 2010-2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* include some standard C headers */
#include <stdio.h>      /* for printf() and fprintf() */
#include <stdlib.h>     /* for atoi() and exit(), random() and randomize() */
#include <string.h>
#include <time.h>       /* for time-related functions (clock, time...) */
#include <stdint.h>     /* for uint8_t type */
#include <signal.h>

#define pVer "0.71"
#define pDate "2010-2020"

/* include the proper drv backend */
#ifdef DRV_SDL2
#include "drv_sdl2.c"
#endif

#ifdef DRV_ALLEGRO4
#include "drv_alle.c"
#endif


/*  The options below make zBoy to be compiled in debug mode, and optionally trace CPU activity to a trace file */
/* #define DEBUGMODE */
/* #define DEBUGSERIAL */      /* outputs raw serial activity to console */
/* #define DEBUGTRACEFILE */   /* traces CPU activity to file */
/* #define MEMDUMP_AT_EXIT */  /* dumps the entire memory to a memdump file before quitting */

/* Produce a stand-alone exe file with an embedded ROM file */
/* #define EMBEDROM "embedrom/pacman.c" */

#ifndef DEBUGMODE   /* If not in debugmode, then disable PrintDebug */
  /* #define PrintDebug printf */
  /* #define PrintDebug(...) */
  void PrintDebug(char *s, ...) {
    s = s;  /* just to make gcc warnings shut up */
  }
  #else
  #define PrintDebug printf
#endif

#include "zbstruct.h"

struct RomInformations RomInfos;

signed int AskForScreenshot = 0;
unsigned int LastScreenshotTime = 0;
int NetPlay = 0;
int NetPlay_LANSTART = 0; /* flag to reset game into a LAN game with auto peer */
char *NetPlayPeer;
char *NetPlayBindaddr = NULL;
int QuitEmulator = 0;

unsigned int GraphicWidth, GraphicHeight;
char UserMessage[128];
unsigned int UserMessageFramesLeft = 0;
unsigned int FPSCOUNTER = 60; /* used for timing of user on-screen messages */

/* the zboyversion() function is used by many modules to display the app version */
char *zboyversion(void) {
  return(pVer);
}
/* the zboyversion() function is used by many modules to display the app date */
char *zboydate(void) {
  return(pDate);
}

#include "about.h"
#include "declares.h"         /* Some SUB/FUNCTION declarations */
#include "binary.h"           /* Provides basic support to binary notation (eg. bx01100111) */
#include "adjtimng.c"        /* AdjustTiming() */
#include "config.c"           /* zboy_loadconfig(), zboy_saveconfig(), zboy_fixconfig() */
#include "numconv.c"          /* UbyteToByte() and DwordVal() */
#include "presskey.c"         /* PressAnyKey() */
#include "getsnapn.c"
#include "mmu.c"              /* Include Memory Management Unit emulation core */
#include "hiscores.c"         /* Provides SaveHiScoresMem() and LoadHiScoresMem() */
#include "cpu-ints.c"         /* Interrupts routines */
#include "video.c"            /* Video emulation core */
#include "loadpal.h"          /* LoadPalette() */
#include "colorize.c"         /* colorize() */
#include "joypad.c"           /* Joypad interface emulation */
#include "cpu-timr.c"         /* Include timer emulation */
#include "cpu-div.c"          /* Include divider emulation */
#ifdef DEBUGMODE
  #include "debug.c"          /* Includes some debugging routines */
#endif
#include "cpu-z80.c"          /* Z80 CPU emulation core */
#include "battery.c"          /* Provides LoadBattRAM() and SaveBattRAM() */
#include "font.c"             /* Includes font data used by zBoy to display onscreen messages */
#include "wordwrap.h"         /* WordWrap(), required by PrintMsg() */
#include "printmsg.c"         /* PrintMsg(text AS STRING) */
#include "loadrom.h"          /* LoadROM(RomFile AS STRING) */
#include "serial.c"           /* Serial interface emulation */
#include "savepcx.c"          /* Screenshot routine SavePcxFile() */
#include "save.c"             /* SaveGame() and LoadGame() */
#include "reset.c"            /* ResetEmulator() */
#include "sound.c"            /* UpdateSound() */


/* checks whether a joystick is needed in current input configuration
 * returns 0 if not, non-zero otherwise */
static int isjoystickneeded(struct zboyparamstype *p) {
  int res;
  res = p->key_bckg | p->key_sprt | p->key_wind | p->key_save | p->key_load |
        p->key_asht | p->key_shot | p->key_rset | p->key_quit | p->key_up |
        p->key_down | p->key_left | p->key_right | p->key_start |
        p->key_select | p->key_a | p->key_b | p->key_turboa | p->key_turbob |
        p->key_lanrset;
  res &= 0x100; /* keyb keys are 0x200, while joypad actions are 0x100 */
  return(res);
}


int main(int argc, char **argv) {
  char *NoYes[2] = {"No", "Yes"};
  struct zboyparamstype zboyparams;
  int x;
  int errflag = 0;
  int UsedCycles;
  struct net_sock_t *sock = NULL;

  RomInfos.MemoryROM_PTR = MemoryROM;

  /* preload a default zBoy configuration */
  zboy_loaddefaultconfig(&zboyparams);

  /* (try to) load parameters from the zBoy configuration file */
  zboy_loadconfig(&zboyparams);

  /* Parse the command-line parameters */
  for (x = 1; x < argc; x++) {
    /*printf("Argument #%d: %s\n", x, argv[x]); */
    if (strcasecmp(argv[x], "--SCALE") == 0) {
        x++;  /* go to next param */
        if (argc > x) {
          if ((atoi(argv[x]) < 1) || (atoi(argv[x]) > 8)) {
            zboyparams.GraphicScaleFactor = 4;    /* Fallback to x4 */
            RomInfos.Filename[0] = 0;  /* Clear out filename, to make sure to display usage screen later */
            x = 250;  /* make sure to quit parsing now */
          } else if (atoi(argv[x]) == 1) {
            zboyparams.GraphicScaleFactor = 1;    /* use 1x scaling */
            zboyparams.scalingalgo = REFRESHSCREEN_NOSCALE; /* No scaling at all */
          } else {
            zboyparams.GraphicScaleFactor = atoi(argv[x]);  /* retrieve provided scale parameter */
            zboyparams.scalingalgo = REFRESHSCREEN_BASICSCALE; /* Nearest Neighbor scaling */
          }
        } else {
          RomInfos.Filename[0] = 0;
        }
    } else if (strcasecmp(argv[x], "--help") == 0) {
      printhelp();
      return(0);
    } else if (strcasecmp(argv[x], "--resetcfg") == 0) {
      zboy_loaddefaultconfig(&zboyparams);
    } else if (strcasecmp(argv[x], "--colorize") == 0) {
      zboyparams.colorize = 1;
    } else if (strcasecmp(argv[x], "--nocolorize") == 0) {
      zboyparams.colorize = 0;
    } else if (strcasecmp(argv[x], "--custcolor") == 0) {
      x++;  /* go to next param */
      zboyparams.custcolor = argv[x];
    } else if (strcasecmp(argv[x], "--scale2x") == 0) {
        x++;  /* go to next param */
        if (argc > x) {
          if ((atoi(argv[x]) != 2) && (atoi(argv[x]) != 4) && (atoi(argv[x]) != 6) && (atoi(argv[x]) != 8)) {
            zboyparams.GraphicScaleFactor = 4;     /* Fallback to x4 */
            RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
            x = 250;  /* make sure to quit parsing now */
          } else {
            zboyparams.GraphicScaleFactor = atoi(argv[x]);  /* retrieve provided scale parameter */
            zboyparams.scalingalgo = REFRESHSCREEN_2X;
          }
        } else {
          RomInfos.Filename[0] = 0;
        }
    } else if (strcasecmp(argv[x], "--scale3x") == 0) {
        x++;  /* go to next param */
        if (argc > x) {
          if ((atoi(argv[x]) != 3) && (atoi(argv[x]) != 6)) {
            zboyparams.GraphicScaleFactor = 4;     /* Fallback to x4 */
            RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
            x = 250;  /* make sure to quit parsing now */
          } else {
            zboyparams.GraphicScaleFactor = atoi(argv[x]);  /* retrieve provided scale parameter */
            zboyparams.scalingalgo = REFRESHSCREEN_3X; /* aka AdvMAME3x */
          }
        } else {
          RomInfos.Filename[0] = 0;
        }
    } else if (strcasecmp(argv[x], "--fpslimit") == 0) {
        x++;  /* go to next param */
        if (argc > x) {
          if ((atoi(argv[x]) < 1) || (atoi(argv[x]) > 60)) {
            zboyparams.fpslimit = 60;  /* Fallback to 60 (no frameskip) */
            RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
            x = 250;  /* make sure to quit parsing now */
          } else {
            zboyparams.fpslimit = atoi(argv[x]);  /* retrieve provided skipframes parameter */
          }
        } else {
          RomInfos.Filename[0] = 0;
        }
    } else if (strcasecmp(argv[x], "--palette") == 0) {   /* load a custom palette */
        x++;  /* go to next param */
        if (argc > x) {
          if (strcasecmp(argv[x], "gbmicro") == 0) zboyparams.palette = 0;
          if (strcasecmp(argv[x], "peasoup") == 0) zboyparams.palette = 1;
        } else {
          RomInfos.Filename[0] = 0;
        }
    } else if (strcasecmp(argv[x], "--rominfo") == 0) {  /* shows various informations about the ROM before running it */
        zboyparams.ShowRomInfos = 1;
    } else if (strcasecmp(argv[x], "--norominfo") == 0) {
        zboyparams.ShowRomInfos = 0;
    } else if (strcasecmp(argv[x], "--cpuburn") == 0) {  /* do not attempt to idle CPU when the emulator have some spare time */
        zboyparams.NoCpuIdle = 1;
    } else if (strcasecmp(argv[x], "--nocpuburn") == 0) {
        zboyparams.NoCpuIdle = 0;
    } else if (strcasecmp(argv[x], "--input") == 0) {
      if (argc <= (x + 10)) {
         RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
         x = 250;  /* make sure to quit parsing now */
      } else {
        zboyparams.key_up = zboyScancodeToBackendScancode(argv[x + 1], &errflag);
        zboyparams.key_down = zboyScancodeToBackendScancode(argv[x + 2], &errflag);
        zboyparams.key_left = zboyScancodeToBackendScancode(argv[x + 3], &errflag);
        zboyparams.key_right = zboyScancodeToBackendScancode(argv[x + 4], &errflag);
        zboyparams.key_select = zboyScancodeToBackendScancode(argv[x + 5], &errflag);
        zboyparams.key_start = zboyScancodeToBackendScancode(argv[x + 6], &errflag);
        zboyparams.key_a = zboyScancodeToBackendScancode(argv[x + 7], &errflag);
        zboyparams.key_b = zboyScancodeToBackendScancode(argv[x + 8], &errflag);
        zboyparams.key_turboa = zboyScancodeToBackendScancode(argv[x + 9], &errflag);
        zboyparams.key_turbob = zboyScancodeToBackendScancode(argv[x + 10], &errflag);
        x += 10;
      }
    } else if (strcasecmp(argv[x], "--fnkeys") == 0) {
      if (argc <= (x + 10)) {
         RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
         x = 250;  /* make sure to quit parsing now */
      } else {
        zboyparams.key_bckg = zboyScancodeToBackendScancode(argv[x + 1], &errflag);
        zboyparams.key_sprt = zboyScancodeToBackendScancode(argv[x + 2], &errflag);
        zboyparams.key_wind = zboyScancodeToBackendScancode(argv[x + 3], &errflag);
        zboyparams.key_save = zboyScancodeToBackendScancode(argv[x + 4], &errflag);
        zboyparams.key_load = zboyScancodeToBackendScancode(argv[x + 5], &errflag);
        zboyparams.key_asht = zboyScancodeToBackendScancode(argv[x + 6], &errflag);
        zboyparams.key_shot = zboyScancodeToBackendScancode(argv[x + 7], &errflag);
        zboyparams.key_rset = zboyScancodeToBackendScancode(argv[x + 8], &errflag);
        zboyparams.key_lanrset = zboyScancodeToBackendScancode(argv[x + 9], &errflag);
        zboyparams.key_quit = zboyScancodeToBackendScancode(argv[x + 10], &errflag);
        x += 10;  /* Redefinition of keys successful, let's continue parsing... */
      }
    } else if (strcasecmp(argv[x], "--fullspeed") == 0) {
        zboyparams.NoSpeedLimit = 1;
    } else if (strcasecmp(argv[x], "--nofullspeed") == 0) {
        zboyparams.NoSpeedLimit = 0;
    } else if (strcasecmp(argv[x], "--showfps") == 0) {
        zboyparams.ShowFPS = 1;
    } else if (strcasecmp(argv[x], "--noshowfps") == 0) {
        zboyparams.ShowFPS = 0;
    } else if (strcasecmp(argv[x], "--hiscores") == 0) {
        zboyparams.HiScoresMem = 1;
    } else if (strcasecmp(argv[x], "--nohiscores") == 0) {
        zboyparams.HiScoresMem = 0;
    } else if (strcasecmp(argv[x], "--netplay") == 0) {
        x += 1;
        if (x < argc) {
          NetPlay = 1; /* atoi(argv[x]); */
          NetPlayPeer = argv[x];
        } else {
          RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
        }
    } else if (strcasecmp(argv[x], "--nosound") == 0) {
        zboyparams.nosound = 1;
    } else if (strcasecmp(argv[x], "--sound") == 0) {
        zboyparams.nosound = 0;
    } else if (strcasecmp(argv[x], "--bindaddr") == 0) {
        x += 1;
        if (x < argc) {
          NetPlayBindaddr = argv[x];
        } else {
          RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
        }
    } else {  /* anything else is assumed to be a filename */
        if (argv[x][0] != 0) { /* ignore if the parameter is empty */
          if (RomInfos.Filename[0] == 0) {
            strncpy(RomInfos.Filename, argv[x], 1022);
          } else {
            RomInfos.Filename[0] = 0;   /* Clear out filename, to make sure to display usage screen later */
            x = 250;  /* make sure to quit parsing now */
          }
        }
    }
  }

  /* check the configuration, and fix it if needed */
  zboy_fixconfig(&zboyparams);
  switch (zboyparams.palette) {
    case 1:
      LoadPalette("peasoup", ScreenPalette32); /* Load the palette */
      break;
    default:
      LoadPalette("gbmicro", ScreenPalette32); /* Load the palette */
      break;
  }

  /* init the screen via the whole SDL mess */
/*  #ifdef DEBUGMODE
    GraphicWidth = 800;
    GraphicHeight = 900;
    GraphicScaleFactor = 2;
  #else */
    GraphicWidth = 160 * zboyparams.GraphicScaleFactor;
    GraphicHeight = 144 * zboyparams.GraphicScaleFactor;
/*  #endif */

  /* init the I/O subsystem and set a window title */
  if (drv_init(GraphicWidth, GraphicHeight, isjoystickneeded(&zboyparams) != 0 ? zboyparams.joyid : -1, zboyparams.nosound ^ 1) != 0) {
    puts("ERROR: Failed to init the audio/video subsystem.");
    return(0);
  }
  drv_setwintitle("zBoy");

  InitScreenOldBuffer();   /* Init the screen buff used for LCD change detection (will force the complete draw of the 1st frame) */

  /* load the palette into the I/O subsystem now, in case we need to display an error message */
  drv_loadpal(ScreenPalette32);

  if (errflag != 0) {
    PrintMsg("\nINPUT DEFINITION ERROR.\n\nSEE ZBOY.TXT FOR DETAILED INSTRUCTIONS.", 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    PressAnyKey();
    return(0);
  }

  #ifndef EMBEDROM
  if (RomInfos.Filename[0] == 0) {
    sprintf(UserMessage, "\n  *** ZBOY V%s ***\n\nUSAGE: ZBOY FILE.GB\n\nSEE ZBOY.TXT FOR DETAILED INSTRUCTIONS.", pVer);
    PrintMsg(UserMessage, 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    PressAnyKey();
    return(0);
  }

  /* Loading the ROM data into memory */
  x = LoadRom(&RomInfos, UserMessage);

  if (x != 0) {
    char *errstr = LoadRomErrStr(x);
    printf("Error: Could not load the ROM file. [%d]\n", x);
    PrintMsg(errstr, 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    PressAnyKey();
    return(0);
  }
  #endif

  /* if a custom color palette has been provided, load it */
  if (zboyparams.custcolor != NULL) {
    if (LoadPaletteFromFile(zboyparams.custcolor, ScreenPalette32) != 0) {
      strcpy(UserMessage, "LOADING THE CUSTOM PALETTE FAILED");
      PrintMsg(UserMessage, 0);
      RefreshScreen(0, 159, 0, 143, &zboyparams);
      PressAnyKey();
      return(0);
    }
  } else if (zboyparams.colorize != 0) { /* else check if we have a color palette for this ROM if colorization enabled */
    colorize(RomInfos.CrcSum , ScreenPalette32);
  }
  drv_loadpal(ScreenPalette32);

  if (NetPlay != 0) {
    /* if autodetection, then keep it for later */
    if (strcasecmp(NetPlayPeer, "auto") == 0) {
      NetPlay = 0;
      NetPlay_LANSTART = 1;
    } else {
      /* open socket */
      sock = net_open(NetPlayBindaddr, NetPlayPeer, 8764);
      if (sock == NULL) {
        printf("Error: Failed to init networking!\n");
        NetPlay = 0;
      }
    }
  }

  if (zboyparams.ShowRomInfos != 0) {
    char messagetoprint[1024];
    sprintf(messagetoprint, "TITLE: %s\n\nCRC32: %08X\n\nLICENSEE: %s\n\nCART TYPE: %s\n\nBATTERY: %s\n\nROM SIZE: %u KIB\n\nROM BANKS: %u\n\nRAM SIZE: %u KIB\n\nPRESS ANY KEY...", RomInfos.NiceTitle, (unsigned int)RomInfos.CrcSum, RomInfos.Licensee, RomInfos.CartTypeString, NoYes[RomInfos.Battery], RomInfos.RomSize >> 10, RomInfos.RomBanks, RomInfos.RamSize >> 10);
    /* Switch the string to all-uppercase */
    for (x = 0; messagetoprint[x] != 0; x++) if ((messagetoprint[x] >= 'a') && (messagetoprint[x] <= 'z')) messagetoprint[x] -= 32;
    PrintMsg(messagetoprint, 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    printf("Title......: %s (\"%s\")\n", RomInfos.Title, RomInfos.NiceTitle);
    printf("Filename...: %s\n", RomInfos.Filename);
    printf("CRC32 sum..: %8X\n", (unsigned int)RomInfos.CrcSum);
    printf("Licensee...: %s\n", RomInfos.Licensee);
    if (RomInfos.ColorGB == 2) {
      printf("ColorGB....: Yes (GBC ONLY)\n");
    } else {
      printf("ColorGB....: %s\n", NoYes[RomInfos.ColorGB]);
    }
    printf("SuperGB....: %s\n", NoYes[RomInfos.SuperGB]);
    printf("Cart Type..: %s\n", RomInfos.CartTypeString);
    printf("Battery....: %s\n", NoYes[RomInfos.Battery]);
    if (RomInfos.Destination == 0) {
      printf("Country....: Japan\n");
    } else {
      printf("Country....: Other (not Japan)\n");
    }
    printf("ROM Size...: %u KiB\n", (RomInfos.RomSize >> 10));
    printf("RAM Size...: %u KiB\n", (RomInfos.RamSize >> 10));
    PressAnyKey();
  }

  if (RomInfos.Rumble != 0) {
    PrintMsg("THIS ROM USES A RUMBLE MECHANISM. ZBOY DOESN'T SUPPORT THAT YET! SORRY.", 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    PressAnyKey();
    return(0);
  }
  if (RomInfos.TimerRTC != 0) {
    PrintMsg("THIS ROM USES AN INTERNAL RTC TIMER CLOCK. ZBOY DOESN'T SUPPORT THAT YET! SORRY.", 0);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    PressAnyKey();
    return(0);
  }
  /*
  IF RomInfos.SuperGB = 1 THEN PRINT : PRINT "Error: SuperGB ROMs are not supported (yet). Sorry." : SLEEP : END
  IF RomInfos.ColorGB = 2 THEN PRINT : PRINT "Error: This ROM requires a GameBoy Color emulator. zBoy supports only legacy GameBoy. Sorry." : SLEEP : END
  */

  switch (RomInfos.MbcModel) {  /* Check the MBC chip type, and select the right Read/Write functions */
    case 0:   /* ROM ONLY */
      MemoryWriteSpecial = &MBC0_MemoryWrite;
      MemoryReadSpecial = &MBC0_MemoryRead;
      break;
    case 1:   /* MBC1 */
      MemoryWriteSpecial = &MBC1_MemoryWrite;
      MemoryReadSpecial = &MBC1_MemoryRead;
      break;
    case 2:   /* MBC2 */
      MemoryWriteSpecial = &MBC2_MemoryWrite;
      MemoryReadSpecial = &MBC2_MemoryRead;
      break;
    case 3:   /* MBC3 */
      MemoryWriteSpecial = &MBC3_MemoryWrite;
      MemoryReadSpecial = &MBC3_MemoryRead;
      break;
    case 5:   /* MBC5 */
      MemoryWriteSpecial = &MBC5_MemoryWrite;
      MemoryReadSpecial = &MBC5_MemoryRead;
      break;
    default:  /* Any other (not supported) memory controller */
      PrintMsg("ERROR: THE MEMORY CONTROLLER USED BY THIS ROM IS NOT SUPPORTED (YET) BY ZBOY. SORRY.", 0);
      RefreshScreen(0, 159, 0, 143, &zboyparams);
      PressAnyKey();
      return(0);
      break;
  }

  /* Set up the window's title */
  if (RomInfos.NiceTitle[0] != 0) {
    snprintf(zboyparams.windowtitle, sizeof(zboyparams.windowtitle), "%s (zBoy)", RomInfos.NiceTitle);
  } else {
    strncpy(zboyparams.windowtitle, "zBoy", sizeof(zboyparams.windowtitle));
  }
  drv_setwintitle(zboyparams.windowtitle);

  srand(time(NULL));  /* Seed the random generator */

  InitRAM();
  if ((RomInfos.Battery != 0) && (RomInfos.RamSize > 0)) LoadBattRAM();  /* check for battery-saved RAM */

  ResetEmulator(); /* Fire up the Zilog80 monster, and init all required variables */

  if (zboyparams.HiScoresMem != 0) LoadHiScoresMem();   /* Check out if need to look after hiscores */

  if (NetPlay != 0) {
    if (netserialsynch(sock, &zboyparams) != 0) QuitEmulator = 1;
  }

  /* add some silence to audio buffer upfront and start the sound stream */
  if ((zboyparams.nosound == 0) && (zboyparams.NoSpeedLimit == 0)) {
    unsigned char soundsample[6000]; /* 6000 bytes is 68ms at 44.1 kHz stereo 8 bit*/
    memset(soundsample, 0, sizeof(soundsample));
    drv_soundunpause(); /* start sound */
    drv_soundqueue(soundsample, sizeof(soundsample));
  }

  /* starting emulation... */

  while (QuitEmulator == 0) {

    /* controlled run-time (for debug & benchmarking) */
    #ifdef LIMITRUN
    QuitEmulator = TotalCycles >> LIMITRUN;
    #endif

    /* if LAN game needs to be restarted... */
    if (NetPlay_LANSTART != 0) {
      NetPlay_LANSTART = 0;
      ResetEmulator();
      sock = netlanstart(sock, NetPlayBindaddr, &zboyparams, RomInfos.CrcSum);
    }

    UsedCycles = CpuExec();
    TotalCycles += UsedCycles;               /* Increment cycles counter */
    uTimer(UsedCycles);                      /* Update uTimer */
    incDivider(UsedCycles);                  /* Increment the DIV register */
    VideoSysUpdate(UsedCycles, &zboyparams); /* Update Video subsystem */
    if (NetPlay != 0) {
      CheckSerialLink(UsedCycles, sock);       /* Check serial link */
    } else {
      CheckSerialLink_dummy();  /* Simulate a disconnected serial link */
    }
    CheckJoypad(UsedCycles, &zboyparams);    /* Update the Joypad register */
    UpdateSound(UsedCycles, zboyparams.nosound | zboyparams.NoSpeedLimit);
    CheckInterrupts();  /* Check if any interrupt has been requested since last time */
/*    #ifdef DEBUGMODE
      if ((TIMER - LastDebugOnscreen) > 0.5) { */
/*        DebugSpriteDump(1); */  /* perform some debug dumps onscreen (OBJ table at &h8000) */
/*        DebugSpriteDump(2); */  /* perform some debug dumps onscreen (OBJ table at &h8800) */
/*        DebugPaletteDump(); */  /* Dump system's palette onscreen */
/*        DebugShowCPUspeed(); */ /* Calc current CPU speed and put it onscreen */
/*        DebugShowCpuRegisters(); */ /* Display state of CPU registers */
/*        DebugOnscreen(); */     /* Display debug buffer informations onscreen */
/*        LastDebugOnscreen = TIMER;
      }
    #endif */
  }

  /* if quitting on error, make sure to display the error screen */
  if (QuitEmulator < 0) {
    PrintMsg(UserMessage, 1);
    RefreshScreen(0, 159, 0, 143, &zboyparams);
    drv_delay(2000);
    PressAnyKey();
  }

  if ((RomInfos.Battery != 0) && (RomInfos.RamSize > 0)) SaveBattRAM();  /* save the battery-saved RAM, if any */
  if (zboyparams.HiScoresMem != 0) SaveHiScoresMem();
  zboy_saveconfig(&zboyparams);

  if (NetPlay != 0) net_close(sock);

  #ifdef MEMDUMP_AT_EXIT
  {
    FILE *fd;
    int x, v;                          /* ----------------- */
    fd = fopen("memdump.bin", "wb");   /* This is a memory  */
    for (x = 0; x < 0xffff; x++) {     /* dump routine.     */
      v = MemoryRead(x);               /*     --  --        */
      fputc(v, fd);                    /* Don't include in  */
    }                                  /* official release! */
    fclose(fd);                        /* ----------------- */
  }
  #endif

  #ifdef DEBUGMODE
    DebugOnScreen();
    PressAnyKey();
  #endif

  /* shutdown the I/O subsystem */
  drv_close();

  fflush(stdin); /* Flush keyb buffer */

  return(0);
}
