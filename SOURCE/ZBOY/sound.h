/*
 * zBoy sound emulation
 * Copyright (C) Mateusz Viste 2015-2019
 */

#ifndef SOUND_H
#define SOUND_H

void sound_writemem(int addr, unsigned char val);

#endif
