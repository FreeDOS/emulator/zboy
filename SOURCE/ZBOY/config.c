/*
   This file provides functions to load/save
   zBoy parameters from/to the configuration
   file.

   This file is part of the zBoy project.

   Copyright (C) Mateusz Viste 2010-2015
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h> /* strcat() */
#include "binary.h" /* provides convenience bxXXXXXXXX notations */
#include "drv.h"
#include "zbstruct.h"



/* This routine checks the consistency of a zBoy configuration, and fixes it if needed */
void zboy_fixconfig(struct zboyparamstype *zboyconf) {
  if ((zboyconf->ShowFPS != 0) && (zboyconf->ShowFPS != 1)) zboyconf->ShowFPS = 0;
  if ((zboyconf->ShowRomInfos != 0) && (zboyconf->ShowRomInfos != 1)) zboyconf->ShowRomInfos = 0;
  if ((zboyconf->NoSpeedLimit != 0) && (zboyconf->NoSpeedLimit != 1)) zboyconf->NoSpeedLimit = 0;
  if ((zboyconf->HiScoresMem != 0) && (zboyconf->HiScoresMem != 1)) zboyconf->HiScoresMem = 0;
  if ((zboyconf->NoCpuIdle != 0) && (zboyconf->NoCpuIdle != 1)) zboyconf->NoCpuIdle = 0;
  if ((zboyconf->fpslimit < 1) || (zboyconf->fpslimit > 60)) zboyconf->fpslimit = 60;
  if ((zboyconf->palette != 0) && (zboyconf->palette != 1)) zboyconf->palette = 0;
  if ((zboyconf->colorize != 0) && (zboyconf->colorize != 1)) zboyconf->colorize = 1;
  switch (zboyconf->scalingalgo) {
    case REFRESHSCREEN_NOSCALE:
      if (zboyconf->GraphicScaleFactor != 1) zboyconf->GraphicScaleFactor = 1;
      break;
    case REFRESHSCREEN_BASICSCALE:
      if ((zboyconf->GraphicScaleFactor < 1) || (zboyconf->GraphicScaleFactor > 8)) zboyconf->GraphicScaleFactor = 4;
      break;
    case REFRESHSCREEN_2X:
      if ((zboyconf->GraphicScaleFactor != 2) && (zboyconf->GraphicScaleFactor != 4) && (zboyconf->GraphicScaleFactor != 6) && (zboyconf->GraphicScaleFactor != 8)) zboyconf->GraphicScaleFactor = 4;
      break;
    case REFRESHSCREEN_3X:
      if ((zboyconf->GraphicScaleFactor != 3) && (zboyconf->GraphicScaleFactor != 6)) zboyconf->GraphicScaleFactor = 3;
      break;
    default:
      zboyconf->scalingalgo = REFRESHSCREEN_2X;
      zboyconf->GraphicScaleFactor = 4;
      break;
  }
}


void zboy_loaddefaultconfig(struct zboyparamstype *zboyparams) {
  /* Set zBoy parameters to default values */
  memset(zboyparams, 0, sizeof(struct zboyparamstype));
  zboyparams->HiScoresMem = 1;
  zboyparams->scalingalgo = REFRESHSCREEN_2X;  /* By default, use Scale2x scaling... */
  zboyparams->GraphicScaleFactor = 4;          /* ...with 4x magnification. */
  zboyparams->fpslimit = 60;
  zboyparams->colorize = 1;

  /* Here I am preloading a default configuration for input keys mapping */
  zboyparams->key_up = DRV_INPUT_KEY_UP;
  zboyparams->key_down = DRV_INPUT_KEY_DOWN;
  zboyparams->key_left = DRV_INPUT_KEY_LEFT;
  zboyparams->key_right = DRV_INPUT_KEY_RIGHT;
  zboyparams->key_start = DRV_INPUT_KEY_RET;
  zboyparams->key_select = DRV_INPUT_KEY_TAB;
  zboyparams->key_a = DRV_INPUT_KEY_LALT;
  zboyparams->key_b = DRV_INPUT_KEY_LCTRL;
  zboyparams->key_turboa = DRV_INPUT_NONE;
  zboyparams->key_turbob = DRV_INPUT_NONE;
  zboyparams->key_bckg = DRV_INPUT_KEY_F1;
  zboyparams->key_sprt = DRV_INPUT_KEY_F2;
  zboyparams->key_wind = DRV_INPUT_KEY_F3;
  zboyparams->key_save = DRV_INPUT_KEY_F5;
  zboyparams->key_load = DRV_INPUT_KEY_F7;
  zboyparams->key_asht = DRV_INPUT_KEY_F8;
  zboyparams->key_shot = DRV_INPUT_KEY_F9;
  zboyparams->key_rset = DRV_INPUT_KEY_F10;
  zboyparams->key_lanrset = DRV_INPUT_KEY_F12;
  zboyparams->key_quit = DRV_INPUT_KEY_ESC;
}


int zboy_loadconfig(struct zboyparamstype *zboyconf) {
  char configfile[1024];
  uint8_t conftable[128];
  int x, confsize;
  FILE *conf;
  drv_getsavedir(configfile, 1024);
  strcat(configfile, "zboy.cfg");
  conf = fopen(configfile, "rb");
  if (conf == NULL) return(-1);
  /* load values from file */
  confsize = fread(conftable, 1, 128, conf);
  fclose(conf);
  if (confsize != 47) return(-2);
  /* compute the checksum and validate it */
  for (x = 1; x < confsize; x++) conftable[0] += conftable[x];
  if (conftable[0] != 0) return(-2);
  /* use values */
  zboyconf->ShowFPS = (conftable[1] >> 7) & 1;
  /*zboyconf->NoScreenSaver = (conftable[1] >> 6) & 1;*/
  zboyconf->ShowRomInfos = (conftable[1] >> 5) & 1;
  zboyconf->NoSpeedLimit = (conftable[1] >> 4) & 1;
  zboyconf->HiScoresMem = (conftable[1] >> 3) & 1;
  zboyconf->NoCpuIdle = (conftable[1] >> 2) & 1;
  /*zboyconf->bootlogo = (conftable[1] >> 1) & 1;*/
  zboyconf->colorize = conftable[1] & 1;
  zboyconf->GraphicScaleFactor = conftable[2] & 15;
  zboyconf->scalingalgo = (conftable[2] >> 4) & 15;
  zboyconf->fpslimit = conftable[3];
  zboyconf->palette = conftable[4];
  /* joyid */
  zboyconf->joyid = conftable[5];
  zboyconf->nosound = conftable[6];
  /* now load key mappings */
  x = 7;
  zboyconf->key_up = conftable[x++];
  zboyconf->key_up <<= 8;
  zboyconf->key_up |= conftable[x++];
  zboyconf->key_down = conftable[x++];
  zboyconf->key_down <<= 8;
  zboyconf->key_down |= conftable[x++];
  zboyconf->key_left = conftable[x++];
  zboyconf->key_left <<= 8;
  zboyconf->key_left |= conftable[x++];
  zboyconf->key_right = conftable[x++];
  zboyconf->key_right <<= 8;
  zboyconf->key_right |= conftable[x++];
  zboyconf->key_start = conftable[x++];
  zboyconf->key_start <<= 8;
  zboyconf->key_start |= conftable[x++];
  zboyconf->key_select = conftable[x++];
  zboyconf->key_select <<= 8;
  zboyconf->key_select |= conftable[x++];
  zboyconf->key_a = conftable[x++];
  zboyconf->key_a <<= 8;
  zboyconf->key_a |= conftable[x++];
  zboyconf->key_b = conftable[x++];
  zboyconf->key_b <<= 8;
  zboyconf->key_b |= conftable[x++];
  zboyconf->key_turboa = conftable[x++];
  zboyconf->key_turboa <<= 8;
  zboyconf->key_turboa |= conftable[x++];
  zboyconf->key_turbob = conftable[x++];
  zboyconf->key_turbob <<= 8;
  zboyconf->key_turbob |= conftable[x++];
  zboyconf->key_bckg = conftable[x++];
  zboyconf->key_bckg <<= 8;
  zboyconf->key_bckg |= conftable[x++];
  zboyconf->key_sprt = conftable[x++];
  zboyconf->key_sprt <<= 8;
  zboyconf->key_sprt |= conftable[x++];
  zboyconf->key_wind = conftable[x++];
  zboyconf->key_wind <<= 8;
  zboyconf->key_wind |= conftable[x++];
  zboyconf->key_save = conftable[x++];
  zboyconf->key_save <<= 8;
  zboyconf->key_save |= conftable[x++];
  zboyconf->key_load = conftable[x++];
  zboyconf->key_load <<= 8;
  zboyconf->key_load |= conftable[x++];
  zboyconf->key_asht = conftable[x++];
  zboyconf->key_asht <<= 8;
  zboyconf->key_asht |= conftable[x++];
  zboyconf->key_shot = conftable[x++];
  zboyconf->key_shot <<= 8;
  zboyconf->key_shot |= conftable[x++];
  zboyconf->key_rset = conftable[x++];
  zboyconf->key_rset <<= 8;
  zboyconf->key_rset |= conftable[x++];
  zboyconf->key_lanrset = conftable[x++];
  zboyconf->key_lanrset <<= 8;
  zboyconf->key_lanrset |= conftable[x++];
  zboyconf->key_quit = conftable[x++];
  zboyconf->key_quit <<= 8;
  zboyconf->key_quit |= conftable[x++];

  /* all done */
  return(0);
}


int zboy_saveconfig(struct zboyparamstype *zboyconf) {
  char configfile[1024];
  uint8_t conftable[128];
  int x, i;
  FILE *conf;
  drv_getsavedir(configfile, 1024);
  strcat(configfile, "zboy.cfg");
  conf = fopen(configfile, "wb");
  if (conf == NULL) return(-1);

  /* prepare the buffer */
  conftable[0] = 0; /* starting key for the checksum */
  conftable[1] = 0;
  if (zboyconf->ShowFPS != 0) conftable[1] |= bx10000000;
  /*if (zboyconf->NoScreenSaver != 0) conftable[1] |= bx01000000;*/
  if (zboyconf->ShowRomInfos != 0) conftable[1] |= bx00100000;
  if (zboyconf->NoSpeedLimit != 0) conftable[1] |= bx00010000;
  if (zboyconf->HiScoresMem != 0) conftable[1] |= bx00001000;
  if (zboyconf->NoCpuIdle != 0) conftable[1] |= bx00000100;
  /*if (zboyconf->bootlogo != 0) conftable[1] |= bx00000010;*/
  if (zboyconf->colorize != 0) conftable[1] |= bx00000001;
  conftable[2] = (zboyconf->scalingalgo << 4);
  conftable[2] |= zboyconf->GraphicScaleFactor;
  conftable[3] = zboyconf->fpslimit;
  conftable[4] = zboyconf->palette;
  /* joyid */
  conftable[5] = zboyconf->joyid;
  conftable[6] = zboyconf->nosound;
  /* now save key mappings */
  i = 7;
  conftable[i++] = zboyconf->key_up >> 8;
  conftable[i++] = zboyconf->key_up & 0xff;
  conftable[i++] = zboyconf->key_down >> 8;
  conftable[i++] = zboyconf->key_down & 0xff;
  conftable[i++] = zboyconf->key_left >> 8;
  conftable[i++] = zboyconf->key_left & 0xff;
  conftable[i++] = zboyconf->key_right >> 8;
  conftable[i++] = zboyconf->key_right & 0xff;
  conftable[i++] = zboyconf->key_start >> 8;
  conftable[i++] = zboyconf->key_start & 0xff;
  conftable[i++] = zboyconf->key_select >> 8;
  conftable[i++] = zboyconf->key_select & 0xff;
  conftable[i++] = zboyconf->key_a >> 8;
  conftable[i++] = zboyconf->key_a & 0xff;
  conftable[i++] = zboyconf->key_b >> 8;
  conftable[i++] = zboyconf->key_b & 0xff;
  conftable[i++] = zboyconf->key_turboa >> 8;
  conftable[i++] = zboyconf->key_turboa & 0xff;
  conftable[i++] = zboyconf->key_turbob >> 8;
  conftable[i++] = zboyconf->key_turbob & 0xff;
  conftable[i++] = zboyconf->key_bckg >> 8;
  conftable[i++] = zboyconf->key_bckg & 0xff;
  conftable[i++] = zboyconf->key_sprt >> 8;
  conftable[i++] = zboyconf->key_sprt & 0xff;
  conftable[i++] = zboyconf->key_wind >> 8;
  conftable[i++] = zboyconf->key_wind & 0xff;
  conftable[i++] = zboyconf->key_save >> 8;
  conftable[i++] = zboyconf->key_save & 0xff;
  conftable[i++] = zboyconf->key_load >> 8;
  conftable[i++] = zboyconf->key_load & 0xff;
  conftable[i++] = zboyconf->key_asht >> 8;
  conftable[i++] = zboyconf->key_asht & 0xff;
  conftable[i++] = zboyconf->key_shot >> 8;
  conftable[i++] = zboyconf->key_shot & 0xff;
  conftable[i++] = zboyconf->key_rset >> 8;
  conftable[i++] = zboyconf->key_rset & 0xff;
  conftable[i++] = zboyconf->key_lanrset >> 8;
  conftable[i++] = zboyconf->key_lanrset & 0xff;
  conftable[i++] = zboyconf->key_quit >> 8;
  conftable[i++] = zboyconf->key_quit & 0xff;

  /* compute checksum, write to file and quit */
  for (x = 1; x < i; x++) conftable[0] += conftable[x]; /* compute the checksum */
  if (conftable[0] > 0) conftable[0] = 256 - conftable[0];
  /* write to file and close */
  fwrite(conftable, 1, i, conf); /* write to file */
  fclose(conf);
  return(0);
}
