/*
 * This file is part of the zBoy project.
 * computes filenames for snapshot files.
 */

#include <sys/time.h>

inline void getsnapshotfilename(char *stringtoset, int sz) {
  time_t t;
  time(&t);
  #ifdef LFNAVAIL
  snprintf(stringtoset, sz, "%s_%lX.pcx", RomInfos.ShortFilenameNoExt, (unsigned long) t);
  #else
  snprintf(stringtoset, sz, "%lX.pcx", (unsigned long) t);
  #endif
}
