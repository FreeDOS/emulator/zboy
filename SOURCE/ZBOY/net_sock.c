/*
 * A networking wrapper around common net operations for zBoy.
 * Copyright (C) Mateusz Viste 2010-2015
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset() */
#include <unistd.h> /* close() */

#ifdef __MINGW32__   /* if windows, include some non-standard microsoft headers */
  #include <winsock2.h>
  #include <ws2tcpip.h>
  #include "mingw_inet_aton.c" /* an inet_aton() substitute for windows */
  #ifndef MSG_DONTWAIT
    #define MSG_DONTWAIT 0x1000000
  #endif
#else
  #include <arpa/inet.h>
#endif

#include "net.h"


struct net_sock_t {
  struct sockaddr_in si_me;
  struct sockaddr_in si_other;
  struct sockaddr_in si_remotesender;
  int s;
  socklen_t slen;
};


struct net_sock_t *net_open(char *bindaddr, char *playpeer, unsigned short port) {
  int one = 1;
  struct net_sock_t *sock;
  sock = malloc(sizeof(struct net_sock_t));
  if (sock == NULL) return(NULL);
  sock->slen = sizeof(struct sockaddr_in);
  sock->s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock->s == -1) {
    printf("socket problem\n");
    free(sock);
    return(NULL);
  }
  memset((char *) &(sock->si_me), 0, sock->slen);
  sock->si_me.sin_family = AF_INET;
  sock->si_me.sin_port = htons(port);
  if (bindaddr == NULL) {
    sock->si_me.sin_addr.s_addr = htonl(INADDR_ANY); /* listen on all addresses if no bind specified */
  } else {
    /* if binding on broadcast, make sure to set SO_REUSEADDR */
    if (strcmp(bindaddr, "255.255.255.255") == 0) {
      sock->si_me.sin_addr.s_addr = INADDR_BROADCAST;
      setsockopt(sock->s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));
      setsockopt(sock->s, SOL_SOCKET, SO_BROADCAST, (char *)&one, sizeof(one));
    } else {
      if (inet_aton(bindaddr, &(sock->si_me.sin_addr)) == 0) {
        printf("inet_aton() failed - bindaddr is probably not a valid IP\n");
        free(sock);
        return(NULL);
      }
    }
  }
  memset((char *) &(sock->si_other), 0, sock->slen);
  sock->si_other.sin_family = AF_INET;
  sock->si_other.sin_port = htons(port);
  /* if playpeer is 255.255.255.255, then use broadcast with SO_REUSEADDR */
  if (strcmp(playpeer, "255.255.255.255") == 0) {
    sock->si_other.sin_addr.s_addr = INADDR_BROADCAST;
    setsockopt(sock->s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one));
    setsockopt(sock->s, SOL_SOCKET, SO_BROADCAST, (char *)&one, sizeof(one));
  } else {
    if (inet_aton(playpeer, &(sock->si_other.sin_addr)) == 0) {
      printf("inet_aton() failed - NetPlay peer is probably not a valid IP\n");
      free(sock);
      return(NULL);
    }
  }
  /* */
  if (bind(sock->s, (struct sockaddr *) &(sock->si_me), sock->slen) == -1) {
    printf("bind() problem: %s\n", strerror(errno));
    free(sock);
    return(NULL);
  }
  return(sock);
}


int net_send(struct net_sock_t *sock, void *databuff, int len) {
  return(sendto(sock->s, databuff, len, 0, (struct sockaddr *) &(sock->si_other), sock->slen));
}


int net_recvpeek(struct net_sock_t *sock, void *databuff, int len) {
  return(recvfrom(sock->s, databuff, len, MSG_PEEK | MSG_DONTWAIT, (struct sockaddr *) &(sock->si_remotesender), &(sock->slen)));
}


int net_recv(struct net_sock_t *sock, void *databuff, int len) {
  return(recvfrom(sock->s, databuff, len, 0, (struct sockaddr *) &(sock->si_remotesender), &(sock->slen)));
}


void net_getpeeraddr(struct net_sock_t *sock, char *databuff) {
  if (inet_ntop(AF_INET, &(sock->si_remotesender.sin_addr), databuff, sock->slen) == NULL) {
    databuff[0] = 0;
  }
}


void net_close(struct net_sock_t *sock) {
  if (sock != NULL) {
    close(sock->s);
    free(sock);
  }
}
