/*
 * Video emulation core of a GameBoy device
 * This file is part of the zBoy project
 * Copyright (C) 2010-2015 Mateusz Viste
 *
 * FF40 (LCDC register):
 * Bit7 0=Display Off  1=Display On
 * Bit6 0=TileMap at 9800-9BFF   1=TileMap at 9C00-9FFF
 * Bit5 0=WindowDisplay disabled  1=WindowDisplay enabled
 * Bit4 0=BG&Window tile data at 8800-97FF   1=BG&Window tile data at 8000-8FFF
 * Bit3 0=BgTile Map display at 9800-9BFF   1=BgTitle Map display at 9C00-9FFF
 * Bit2 0=SpriteSize=8x8    1=SpriteSize=8x16
 * Bit1 0=Do not display Sprites   1=Display sprites
 * Bit0 0=Do not display background   1=Display background
 *
 * Generating an interrupt means "enabling the corresponding bit of the IF register (FF0F)"
 */

#include "qsort.c"  /* provides the tableQsort() sub, used to sort sprites in DrawSprites */

unsigned int HideBackgroundDisplay = 0;
unsigned int HideSpritesDisplay = 0;
unsigned int HideWindowDisplay = 0;

uint8_t AutoScreenshot = 0;

unsigned int CountFPS, VideoClkCounterMode, VideoClkCounterVBlank;
unsigned int fpslimitGenFrameTrigger = 0;
uint8_t ScreenBuffer[160][144];
uint8_t ScreenOldBuffer[160][144];  /* used as a buffer to detect changes on screen */
uint8_t CurLY, LastLYdraw;   /* used by VideoSysUpdate */
uint32_t ScreenPalette32[256] = {0xFFFFFF,   /* Palette of shades to use onscreen */
                                 0xAAAAAA,
                                 0x555555,
                                 0x000000};

#include "videoout.c"          /* Video output (& scaling) routines */
/* void (*RefreshScreen)(); */ /* this is a pointer to a function, it needs to be set to the appropriate function later. */


#define pal_BGP 0xFF47
#define pal_OBP0 0xFF48
#define pal_OBP1 0xFF49
inline static uint8_t GetGbPalette(int PalAddr, uint8_t ColIdx) {
  /*  Returns the RGB value of a color index from one of the palettes */
  /*   ColIdx: 0..3 */
  /*   PalType: 0=BGP  1=OBP0  2=OBP1 */
  static const int colidxshift[4] = {0, 4, 2, 6};
  return((IoRegisters[PalAddr] >> colidxshift[ColIdx]) & 3);
}


static const int TilesDataAddresses[2] = {0x8800, 0x8000};
static const int BgTileMapAddresses[2] = {0x9800, 0x9C00};

inline static void DrawBackground(unsigned int CurScanline) {
  static unsigned int TilesDataAddr, BgTilesMapAddr, TileNum, TileToDisplay, TileTempAddress, LastDisplayedTile;
  static uint8_t x, y, z, t, u, UbyteBuff1, UbyteBuff2, PixelX;
  static uint8_t TileBuffer[64];

  /* if "BackgroundEnabled" bit is not set, or bg has been forced OFF by user, then do not draw background (fill with white instead) */
  if (((IoRegisters[0xFF40] & 1) == 0) || (HideBackgroundDisplay != 0)) {
    for (x = 0; x < 160; x++) ScreenBuffer[x][CurScanline] = 0;  /* white */
    return;
  }

  /* if the "BackgroundEnabled" bit is set then draw background */

  /* Get starting address of tiles data */
  TilesDataAddr = TilesDataAddresses[(IoRegisters[0xFF40] >> 4) & 1];

  /* Get address of BGP tiles *map* */
  BgTilesMapAddr = BgTileMapAddresses[(IoRegisters[0xFF40] >> 3) & 1];

  y = (CurScanline + IoRegisters[0xFF42]);
  z = (y & 7);   /* Same than z = y MOD 8 (but MUCH faster)    --> this is the tile's row that has to be computed */
  u = (z << 3);  /* << 3 = *8 */
  y >>= 3;  /* Same than y = y/8 (but >> is faster)            --> this is the number of the tile to display */

  /*FOR y = 0 TO 31 */
    LastDisplayedTile = 999; /* a valid tile number is 0..255 - I am forcing the 1st tile to get generated */
    TileNum = (y << 5);    /* << 5 means "*32" */
    for (x = 0; x < 32; x++) {
      if (TilesDataAddr == 0x8000) {
        TileToDisplay = VideoRAM[BgTilesMapAddr + TileNum];
      } else {
        TileToDisplay = UbyteToByte(VideoRAM[BgTilesMapAddr + TileNum]) + 128;
      }
      if (TileToDisplay != LastDisplayedTile) { /* Compute the tile only if it's different than the previous one (otherwise just */
        LastDisplayedTile = TileToDisplay;      /* reuse the same data) - I named this a "micro tile cache" in the changelog. */
        t = (z << 1);    /*   Get only the tile's y line related to current scanline */
        TileTempAddress = TilesDataAddr + (TileToDisplay << 4) + t;
        t <<= 2;
        /*FOR t = 0 TO 15 STEP 2 */
          UbyteBuff1 = VideoRAM[TileTempAddress];
          UbyteBuff2 = VideoRAM[TileTempAddress + 1];
          TileBuffer[t + 7] =  ((UbyteBuff2 & 1) | ((UbyteBuff1 & 1) << 1));
          TileBuffer[t + 6] = (((UbyteBuff2 & 2) >> 1) | (((UbyteBuff1 & 2) >> 1) << 1));
          TileBuffer[t + 5] = (((UbyteBuff2 & 4) >> 2) | (((UbyteBuff1 & 4) >> 2) << 1));
          TileBuffer[t + 4] = (((UbyteBuff2 & 8) >> 3) | (((UbyteBuff1 & 8) >> 3) << 1));
          TileBuffer[t + 3] = (((UbyteBuff2 & 16) >> 4) | (((UbyteBuff1 & 16) >> 4) << 1));
          TileBuffer[t + 2] = (((UbyteBuff2 & 32) >> 5) | (((UbyteBuff1 & 32) >> 5) << 1));
          TileBuffer[t + 1] = (((UbyteBuff2 & 64) >> 6) | (((UbyteBuff1 & 64) >> 6) << 1));
          TileBuffer[t + 0] = (((UbyteBuff2 & 128) >> 7) | (((UbyteBuff1 & 128) >> 7) << 1));
        /*NEXT t */
      }
      PixelX = ((x << 3) - IoRegisters[0xFF43]);  /* 0xFF43 is the SCX register */
      for (t = 0; t < 8; t++) {
        if (PixelX < 160) {
          ScreenBuffer[PixelX][CurScanline] = GetGbPalette(pal_BGP, TileBuffer[u + t]);
        }
        PixelX += 1;
      }
      TileNum += 1;
    } /* for(x) */
  /*NEXT y */
}


inline static void DrawWindow(unsigned int CurScanline) {
  /* Some infos about the window...

   WindowPosX = 7    [FF4Bh]  (should be >= 0 and <= 166)
   WindowPosY = 0    [FF4Ah]  (should be >= 0 and <= 143)

   Window tiles can be located at 8000h-8FFFh or 8800h-97FFh (just like for the background)
   At 8000h-8FFFh tiles are numbered from 0 to 255, while at 8800h-97FFh tiles are ranging from -128 to 127.

   The LCDC register is at FF40h.
   Bit 6 - Window tile map address (0: 9800h-9BFFh, 1: 9C00h-9FFFh)
   Bit 5 - Window Display (0: off, 1: on)
   Bit 4 - Window & background tile data address (0: 8800h-97FFh, 1: 8000h-8FFFh)
   Bit 0 - Window & background display (0: off, 1: on)

   The palette for both background and window is located at FF47h (BGP)
  */
  static int x, y, z, t, UbyteBuff1, UbyteBuff2, pixrow;
  static int TilesMapAddress, TilesDataAddress, TileNum, TileToDisplay, PixelX, TileTempAddress;
  static uint8_t TileBufferWin[64];

  /* Note that on the original DMG-01, bit 0 of LCDC (FF40h) controls only background, and bit 5 controls only the window - and that's how it is implemented here. On a GBC, things are slightly different: bit 5 still controls only window, but bit 0 controls BOTH the background AND window */
  if (((IoRegisters[0xFF40] & bx00100000) == 0) || (HideWindowDisplay != 0)) return;

  /* check that the window is visible */
  if ((IoRegisters[0xFF4B] > 166) || (IoRegisters[0xFF4A] > CurScanline) || (CurScanline > 143)) return;

  /* Get tile map address */
  TilesMapAddress = BgTileMapAddresses[(IoRegisters[0xFF40] >> 6) & 1];

  /* Get starting address of tiles data */
  TilesDataAddress = TilesDataAddresses[(IoRegisters[0xFF40] >> 4) & 1];

  /* draw the stuff */

  pixrow = (CurScanline - IoRegisters[0xFF4A]);
  y = (pixrow >> 3);  /* don't forget about adding WY! (SHR 3 is the same than / 8)  --> this is the y tile row to display */
  pixrow &= bx00000111;  /* MOD 8                --> this is the tile's row of pixels to display */
  TileNum = (y << 5); /* I'm doing "y << 5" instead of "y * 32" for higher speed */
  for (x = 0; x < 32; x++) {
    if (TilesDataAddress == 0x8000) {
      TileToDisplay = VideoRAM[TilesMapAddress + TileNum];
    } else {
      TileToDisplay = UbyteToByte(VideoRAM[TilesMapAddress + TileNum]) + 128;
    }
    z = (pixrow << 1);   /*      Get only the tile's y line related to current scanline */
    TileTempAddress = TilesDataAddress + (TileToDisplay << 4) + z;  /* << 4 is the same than *16 (just much faster) */
    z <<= 2;
    UbyteBuff1 = VideoRAM[TileTempAddress];     /* << 4 is the same than *16 (just much faster) */
    UbyteBuff2 = VideoRAM[TileTempAddress + 1]; /* << 4 is the same than *16 (just much faster) */

    TileBufferWin[z + 7] =  ((UbyteBuff2 & bx00000001) | ((UbyteBuff1 & bx00000001) << 1));      /* Note: I am using "SHL 2" here instead of "*4" (faster!) */
    TileBufferWin[z + 6] = (((UbyteBuff2 & bx00000010) >> 1) | (((UbyteBuff1 & bx00000010) >> 1) << 1));
    TileBufferWin[z + 5] = (((UbyteBuff2 & bx00000100) >> 2) | (((UbyteBuff1 & bx00000100) >> 2) << 1));
    TileBufferWin[z + 4] = (((UbyteBuff2 & bx00001000) >> 3) | (((UbyteBuff1 & bx00001000) >> 3) << 1));
    TileBufferWin[z + 3] = (((UbyteBuff2 & bx00010000) >> 4) | (((UbyteBuff1 & bx00010000) >> 4) << 1));
    TileBufferWin[z + 2] = (((UbyteBuff2 & bx00100000) >> 5) | (((UbyteBuff1 & bx00100000) >> 5) << 1));
    TileBufferWin[z + 1] = (((UbyteBuff2 & bx01000000) >> 6) | (((UbyteBuff1 & bx01000000) >> 6) << 1));
    TileBufferWin[z] =     (((UbyteBuff2 & bx10000000) >> 7) | (((UbyteBuff1 & bx10000000) >> 7) << 1));

    PixelX = ((x << 3) + IoRegisters[0xFF4B] - 7);  /* 0xFF4B is the WX register (starts at 7!)   / (x << 3) is the same than (x * 8), but faster */
    z = (((CurScanline - IoRegisters[0xFF4A]) & bx00000111) << 3);   /* "AND bx0111" is used instead of "MOD 8" and "SHL 3" instead of "* 8" */
    for (t = 0; t < 8; t++) {
      if ((PixelX >= 0) && (PixelX < 160)) {
        ScreenBuffer[PixelX][CurScanline] = GetGbPalette(pal_BGP, TileBufferWin[z]) | 32; /* | 32 is for marking it as 'window' (for possible colorization) */
      }
      PixelX++;  /* add 1 instead of relying on t value (faster) */
      z++;
    }
    TileNum++;   /* I'm adding 1 instead of relying on x value (faster) */
  }
}


inline static void DrawSprites(int CurScanline) {
  /* Sprites should be displayed in some specific order (aka "sprite priority"):
     When sprites with different x coordinate values overlap, the one with the smaller
     x coordinate (closer to the left) will have priority and appear above any others.
     When sprites with the same x coordinate values overlap, they have priority according
     to table ordering. (i.e. $FE00 - highest, $FE04 - next highest, etc.) */
  int PatternNum, SpriteFlags, UbyteBuff1, UbyteBuff2, SpritePalette, x, z;
  static int SpritePosX, SpritePosY, SpriteMaxY;
  int NumberOfSpritesToDisplay;
  uint8_t SpriteBuff[8];   /* big enough for a single 8-pixel sprite line */
  static int ListOfSpritesToDisplay[40];
  /* If bit 1 of LCDC is false or 'hide sprites' mode enabled -> don't display sprites */
  if (((IoRegisters[0xFF40] & bx00000010) == 0) || (HideSpritesDisplay != 0)) return;

  NumberOfSpritesToDisplay = 0;
  if ((IoRegisters[0xFF40] & bx00000100) == 0) {      /* If Bit 2 of LCDC [FF40] is reset -> the sprite is 8x8 */
    SpriteMaxY = 7;
  } else {  /* Else the sprite is 8x16 (the LSB is ignored in 8x16 sprite mode) */
    SpriteMaxY = 15;
  }
  for (x = 0; x < 40; x++) {   /* Read 40 blocks of 4 bytes starting from 0xFE00 */
    SpritePosY = SpriteOAM[0xFE00 + (x << 2)] - 16;     /* sprite's ypos on screen */
    SpritePosX = SpriteOAM[0xFE00 + (x << 2) + 1];      /* sprite's xpos on screen */
    if ((SpritePosX > 0) && (SpritePosY >= (CurScanline - SpriteMaxY)) && (SpritePosX < 168) && (SpritePosY <= CurScanline)) {
      /* Here I build the list of sprites that will have to be displayed */
      ListOfSpritesToDisplay[NumberOfSpritesToDisplay] = (SpritePosX << 6) | x; /* compute an ID that will allow to sort entries easily */
      NumberOfSpritesToDisplay += 1;
    }
  }
  /* sort the list of sprites to display (reverse order) */
  tableQrsort(ListOfSpritesToDisplay, NumberOfSpritesToDisplay - 1);

  /* and here we are going to display each sprite (in the right order, of course) */
  while (NumberOfSpritesToDisplay-- != 0) {
    x = (ListOfSpritesToDisplay[NumberOfSpritesToDisplay] & bx00111111);  /* Retrieve 6 least significant bits (these are the actual ID of the sprite) */
    x <<= 2; /* translate id into OAM byte displacement (*4) */

    SpritePosY = SpriteOAM[0xFE00 + x] - 16;     /* sprite's ypos on screen */
    SpritePosX = SpriteOAM[0xFE00 + x + 1] - 8;  /* sprite's xpos on screen */
    PatternNum = SpriteOAM[0xFE00 + x + 2];      /* pattern num */
    if (SpriteMaxY == 15) PatternNum &= bx11111110;   /* the LSB is ignored in 8x16 sprite mode */

    SpriteFlags = SpriteOAM[0xFE00 + x + 3];  /* Flags */

    /* select the sprite palette addr (bit 4 of SpriteFlags, 0=pal_OBP0, 1=pal_OBP1) */
    SpritePalette = pal_OBP0 + ((SpriteFlags >> 4) & 1);

    /* select only the one single line of the sprite that matches the current scanline */
    z = CurScanline - SpritePosY;

    /* SpritePosY defines the Y-pos of the sprite LINE now, not sprite's top edge */
    SpritePosY += z;

    /* adjust sprite's line position if sprite is V-mirrored */
    if ((SpriteFlags & bx01000000) != 0) {   /* Bit 6 = Y flip (vertical mirror) */
      UbyteBuff1 = VideoRAM[0x8000 + (PatternNum << 4) + ((SpriteMaxY - z) << 1)];
      UbyteBuff2 = VideoRAM[0x8000 + (PatternNum << 4) + ((SpriteMaxY - z) << 1) + 1];
    } else {
      UbyteBuff1 = VideoRAM[0x8000 + (PatternNum << 4) + (z << 1)];
      UbyteBuff2 = VideoRAM[0x8000 + (PatternNum << 4) + (z << 1) + 1];
    }

    /* load sprite line pixels in reverts order if X flip enabled */
    if ((SpriteFlags & bx00100000) != 0) {      /* Bit 5 = X flip (horizontal mirror) */
      SpriteBuff[0] =  ((UbyteBuff2 & bx00000001) | ((UbyteBuff1 & bx00000001) << 1));
      SpriteBuff[1] = (((UbyteBuff2 & bx00000010) >> 1) | (((UbyteBuff1 & bx00000010) >> 1) << 1));
      SpriteBuff[2] = (((UbyteBuff2 & bx00000100) >> 2) | (((UbyteBuff1 & bx00000100) >> 2) << 1));
      SpriteBuff[3] = (((UbyteBuff2 & bx00001000) >> 3) | (((UbyteBuff1 & bx00001000) >> 3) << 1));
      SpriteBuff[4] = (((UbyteBuff2 & bx00010000) >> 4) | (((UbyteBuff1 & bx00010000) >> 4) << 1));
      SpriteBuff[5] = (((UbyteBuff2 & bx00100000) >> 5) | (((UbyteBuff1 & bx00100000) >> 5) << 1));
      SpriteBuff[6] = (((UbyteBuff2 & bx01000000) >> 6) | (((UbyteBuff1 & bx01000000) >> 6) << 1));
      SpriteBuff[7] = (((UbyteBuff2 & bx10000000) >> 7) | (((UbyteBuff1 & bx10000000) >> 7) << 1));
    } else { /* no X flip */
      SpriteBuff[7] =  ((UbyteBuff2 & bx00000001) | ((UbyteBuff1 & bx00000001) << 1));
      SpriteBuff[6] = (((UbyteBuff2 & bx00000010) >> 1) | (((UbyteBuff1 & bx00000010) >> 1) << 1));
      SpriteBuff[5] = (((UbyteBuff2 & bx00000100) >> 2) | (((UbyteBuff1 & bx00000100) >> 2) << 1));
      SpriteBuff[4] = (((UbyteBuff2 & bx00001000) >> 3) | (((UbyteBuff1 & bx00001000) >> 3) << 1));
      SpriteBuff[3] = (((UbyteBuff2 & bx00010000) >> 4) | (((UbyteBuff1 & bx00010000) >> 4) << 1));
      SpriteBuff[2] = (((UbyteBuff2 & bx00100000) >> 5) | (((UbyteBuff1 & bx00100000) >> 5) << 1));
      SpriteBuff[1] = (((UbyteBuff2 & bx01000000) >> 6) | (((UbyteBuff1 & bx01000000) >> 6) << 1));
      SpriteBuff[0] = (((UbyteBuff2 & bx10000000) >> 7) | (((UbyteBuff1 & bx10000000) >> 7) << 1));
    }
    /* Now apply the sprite onscreen... */
    UbyteBuff1 = GetGbPalette(pal_BGP, 0);  /* Get background color into UbyteBuff1 */
    for (x = 0; x < 8; x++) { /*  Update all pixels, but not 0 (these are transparent for a sprite) */
      /* color 0 is transparent on sprites */
      if (SpriteBuff[x] == 0) continue;
      /* don't write outside screen */
      if (((SpritePosX + x) < 0) || ((SpritePosX + x) >= 160)) continue;
      /* If bit 7 of the sprite's flags is set, then the sprite is "hidden" (prevails only over color 0 of background) */
      if (((SpriteFlags & bx10000000) != 0) && (ScreenBuffer[SpritePosX + x][SpritePosY] != UbyteBuff1)) continue;
      /* blend pixels to screen buff and mark them as 'OBJ0' or 'OBJ1' (64/128) for later possible colorization */
      ScreenBuffer[SpritePosX + x][SpritePosY] = GetGbPalette(SpritePalette, SpriteBuff[x]) | (64 << (SpritePalette & 1));
    }
  }
}


void TurnLcdOff(void) {
  uint8_t x, y;
  for (y = 0; y < 144; y++) {
    for (x = 0; x < 160; x++) ScreenBuffer[x][y] = 0;
  }
}


void InitScreenOldBuffer(void) {    /* Call this one time, at the emulator */
  uint8_t x, y;                 /* start, to force the refresh of the  */
  for (y = 0; y < 144; y++) {   /* whole first LCD frame.              */
    for (x = 0; x < 160; x++) {
      ScreenOldBuffer[x][y] = 255; /* fill with an unlikely value */
    }
  }
}



#define GetLcdMode() (IoRegisters[0xFF41] & bx00000011)

static inline void SetLcdMode(uint8_t x) {
  IoRegisters[0xFF41] &= bx11111100;
  IoRegisters[0xFF41] |= x;   /* Here I set the LCD mode */
}


/* Parsing all the screen to detect changes that occured since last screen refresh */
#define DetectScreenChanges(); \
  x1 = 255;\
  y1 = 255;\
  x2 = 0;\
  for (y = 0; y < 144; y++) {\
    for (x = 0; x < 160; x++) {\
      if (ScreenBuffer[x][y] != ScreenOldBuffer[x][y]) {\
        ScreenOldBuffer[x][y] = ScreenBuffer[x][y];\
        if (y1 > y) y1 = y;\
        y2 = y;\
        if (x1 > x) x1 = x;\
        if (x2 < x) x2 = x;\
      }\
    }\
  }



static inline void VideoSysUpdate(int cycles, struct zboyparamstype *zboyparams) {
  static int x1, x2, y1, y2, x, y;
  static unsigned int LastFullframeRenderingTime = 0;
  static uint32_t OneSecondPollingTimer = 0;

  VideoClkCounterVBlank += cycles;
  if (VideoClkCounterVBlank >= 70224) {
    CurLY = 0;
    VideoClkCounterVBlank -= 70224;
    VideoClkCounterMode = VideoClkCounterVBlank; /* Sync VideoClkCounterMode with VideoClkCounterVBlank */
  }

  /* MemoryInternalHiRAM[0xFF44] is the LY register (current vertical scanline) */

  /* Not sure if I should increment LY when LCD is off... but if I don't, then Baseball doesn't start. */
  CurLY = (VideoClkCounterVBlank / 456);       /* LY should be between 0..153 */
  IoRegisters[0xFF44] = CurLY;               /* Save new LY */

  if ((IoRegisters[0xFF40] & bx10000000) != 0) {       /* If LCD is ON... */
    if (IoRegisters[0xFF44] == IoRegisters[0xFF45]) {  /* Compare LY with LYC, and if equal, and */
      if ((IoRegisters[0xFF41] & bx00000100) == 0) {   /* coincidence bit not set (yet), then    */
        IoRegisters[0xFF41] |= bx00000100;             /* set the coincidence (2) bit of the     */
        if ((IoRegisters[0xFF41] & bx01000000) != 0) { /* STAT register (FF41) and (if enabled   */
          INT(INT_LCDC);                               /* via bit 6 of STAT register) trigger    */
        }
      }
    } else {
      IoRegisters[0xFF41] &= bx11111011;   /* reset the coincidence flag */
    }
  }


  if (VideoClkCounterVBlank >= 65664) {    /* We are in a VBLANK period! */
    /* Here I trigger the vblank interrupt and set mode 1 */
    if (GetLcdMode() != bx00000001) {   /* Check if not already in mode 1 */
      SetLcdMode(1);   /* Here I set LCD mode 1 */
      INT(INT_VBLANK);   /* Ask for the VBLANK interrupt */
      if ((IoRegisters[0xFF41] & bx00010000) != 0) {
        INT(INT_LCDC);   /* Ask for the LCDC/VBLANK interrupt, too, if enabled */
      }
      if ((IoRegisters[0xFF40] & bx10000000) == 0) {   /* The LCD is off, make it all white */
        TurnLcdOff();
        /*UserMessage = "LCD IS OFF" */
      }
      /* There is some stuff to check once per second - let's see if the time has come */
      if ((drv_getticks() - OneSecondPollingTimer) >= 1000) {
        OneSecondPollingTimer = drv_getticks();
        FPSCOUNTER = CountFPS;
        CountFPS = 0;
        if (zboyparams->ShowFPS != 0) {
          char sfps[16];
          sprintf(sfps, "%u", FPSCOUNTER);
          SetUserMsg(sfps);
        }
        if (AutoScreenshot != 0) { /* Ask for a screenshot if autoscreenshot is ON, and last one was 10s ago */
          if ((OneSecondPollingTimer - LastScreenshotTime) >= 10000) AskForScreenshot = 1;
        }
      }
      /* Do a screenshot if the user wanted one */
      if (AskForScreenshot != 0) {
        char snapshotfile[1040];
        getsnapshotfilename(snapshotfile, sizeof(snapshotfile));
        SavePcxFile(snapshotfile);
        AskForScreenshot = 0;
        LastScreenshotTime = drv_getticks();
        SetUserMsg("SCREENSHOT DONE");
      }
      /* Print user messages onscreen, if any */
      if (UserMessageFramesLeft > 0) {
        UserMessageFramesLeft -= 1;
        PrintMsg(UserMessage, 1); /* Draw the user message, if any */
      }
      if (zboyparams->NoSpeedLimit == 0) AdjustTiming(zboyparams); /* Slow down CPU emulation before drawing graphic frame */
      fpslimitGenFrameTrigger += zboyparams->fpslimit;
      if (fpslimitGenFrameTrigger >= 60) { /* check out if SkipFrame allows to draw the frame */
        fpslimitGenFrameTrigger -= 60;
        DetectScreenChanges();
        if (LastFullframeRenderingTime > 90) { /* Redraw a full frame every 90 frames (my SDL looses screen content when minimized/maximized..) */
          RefreshScreen(0, 159, 0, 143, zboyparams);
          LastFullframeRenderingTime = 0;
        } else {
          if (x1 != 255) {
            RefreshScreen(x1, x2, y1, y2, zboyparams);  /* Refresh the (real) screen, if it has changed at all */
#ifdef DRVSDL_RECORDING
          } else {
            RefreshScreen(0, 0, 0, 0, zboyparams); /* recording requires all frames */
#endif
          }
          LastFullframeRenderingTime++;
        }
        CountFPS++;
      }
    }
  } else {   /* Outside the VBlank, perform the MODE 0-2-3 loop */
    VideoClkCounterMode += cycles;
    if (VideoClkCounterMode >= 456) {
      VideoClkCounterMode -= 456;
    }
    if (VideoClkCounterMode <= 80) { /* mode 2: the LCD controller reads from OAM memory */
      if (GetLcdMode() != 2) {    /*  Check if not already in mode 2 */
        SetLcdMode(2);   /* Here I set LCD mode 2 */
        if ((IoRegisters[0xFF41] & bx00100000) != 0) {   /* If OAM int is enabled, request LCDC int */
          INT(INT_LCDC);
        }
      }
    } else if (VideoClkCounterMode <= (172 + 80)) {  /* mode 3: transferring data from memory to the LCD */
      if (GetLcdMode() != 3) {    /* Check if not already in mode 3 */
        SetLcdMode(3);   /* Here I set LCD mode 3 (no int here) */
      }
    } else {     /* mode 0 (H-blank) */
      if (GetLcdMode() != 0) {   /* Check if not already in mode 0 */
        if ((IoRegisters[0xFF40] & bx10000000) > 0) {   /* If LCD is ON... */
          if (LastLYdraw != CurLY) {                    /* And curline hasn't been drawn yet... */
            LastLYdraw = CurLY;
            DrawBackground(CurLY);                      /* Generate current scanline */
            DrawWindow(CurLY);
            DrawSprites(CurLY);
          }
        }
        /* Trigger the hblank interrupt if enabled via bit 3 of the stat register (FF41) (via LCDC int?) */
        SetLcdMode(0);   /* Here I set LCD mode 0 */
        if ((IoRegisters[0xFF41] & bx00001000) != 0) {   /* If hblank int is enabled, request LCDC int */
          INT(INT_LCDC);
        }
      }
    }
  }
}
