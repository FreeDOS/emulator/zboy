/*
 * zBoy networking lib header
 * This file is part of the zBoy project
 * Copyright (C) 2010-2019 Mateusz Viste
 */

#ifndef NET_H_SENTINEL
#define NET_H_SENTINEL

struct net_sock_t; /* shadow type declaration */

struct net_sock_t *net_open(char *bindaddr, char *playpeer, unsigned short port);
int net_send(struct net_sock_t *sock, void *databuff, int len);
int net_recvpeek(struct net_sock_t *sock, void *databuff, int len);
int net_recv(struct net_sock_t *sock, void *databuff, int len);
void net_getpeeraddr(struct net_sock_t *sock, char *databuff);
void net_close(struct net_sock_t *sock);

#endif
