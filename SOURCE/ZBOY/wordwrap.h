/*
 *  Word-wrapping function
 *  Author: Mateusz Viste
 *  Last modified: 13 Aug 2011
 *
 *  Note, that the function will modify the char array you will pass to it!
 */

#ifndef WORDWRAP_H_SENTINEL
#define WORDWRAP_H_SENTINEL

void WordWrap(char *TextToWrap, int WrapValue, char *result);

#endif
