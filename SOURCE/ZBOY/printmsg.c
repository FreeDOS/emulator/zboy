/*
   PrintMsg()
*/

void SetUserMsg(char *msg) {
  strcpy(UserMessage, msg);
  UserMessageFramesLeft = FPSCOUNTER << 1; /* Make the message to be active for 120 frames (2s) */
}


void PrintMsg(char *txtorgmessage, unsigned char TransparentFlag) {
  /* TransparentFlag = 1  -->  Do not clear the screen, and do not print background on chars
     TransparentFlag = 0  --> Clear the screen before doing anything */
  static int bitfields[8] = {bx00000001, bx00000010, bx00000100, bx00001000, bx00010000, bx00100000, bx01000000, bx10000000};

  signed int pixrow, pixcol, linenum = 0;
  int x, textstring_len;
  char textstring[1024], txtmessage[1024];
  strncpy(txtmessage, txtorgmessage, 1023);
  if (TransparentFlag == 0) {
    for (pixcol = 0; pixcol < 160; pixcol++) {
      for (pixrow = 0; pixrow < 144; pixrow++) ScreenBuffer[pixcol][pixrow] = 254;
    }
  }
  do {
    WordWrap(txtmessage, 22, textstring);
    if (textstring[0] != 0) {
      for (textstring_len = 0; textstring[textstring_len] != 0; textstring_len++); /* Get the length of the textstring */
      for (x = 0; x < textstring_len; x++) {
        if ((textstring[x] >= 32) && (textstring[x] <= 94)) {
          for (pixrow = 0; pixrow < 8; pixrow++) {
            /* here I check if a shadow at the left of the glyph is needed */
            if ((FontData[(unsigned)textstring[x]][pixrow] & bitfields[0]) != 0) {
              ScreenBuffer[(x * 7) + 2][pixrow + (linenum << 3)] = 254;
            }

            for (pixcol = 0; pixcol < 8; pixcol++) { /* changed pixcol = -1 to pixcol = 0 */
              if ((FontData[(unsigned)textstring[x]][pixrow] & bitfields[pixcol]) != 0) {
                ScreenBuffer[pixcol + (x * 7) + 3][pixrow + (linenum << 3)] = 255;
              } else {  /* check if needed to display text shadow. */
                if (pixrow > 0) {
                  if ((FontData[(unsigned)textstring[x]][pixrow - 1] & bitfields[pixcol]) != 0) {
                    ScreenBuffer[pixcol + (x * 7) + 3][pixrow + (linenum << 3)] = 254;
                  }
                }
                if (pixcol > 0) {
                  if ((FontData[(unsigned)textstring[x]][pixrow] & bitfields[pixcol - 1]) != 0) {
                    ScreenBuffer[pixcol + (x * 7) + 3][pixrow + (linenum << 3)] = 254;
                  }
                }
                if (pixcol < 7) {
                  if ((FontData[(unsigned)textstring[x]][pixrow] & bitfields[pixcol + 1]) != 0) {
                    ScreenBuffer[pixcol + (x * 7) + 3][pixrow + (linenum << 3)] = 254;
                  }
                }
                if (pixrow < 7) {
                  if ((FontData[(unsigned)textstring[x]][pixrow + 1] & bitfields[pixcol]) != 0) {
                    ScreenBuffer[pixcol + (x * 7) + 3][pixrow + (linenum << 3)] = 254;
                  }
                }
              }
            }
          }
        }
      }
    }
    linenum++;
  } while ((txtmessage[0] != 0) && (linenum != 19));
}
